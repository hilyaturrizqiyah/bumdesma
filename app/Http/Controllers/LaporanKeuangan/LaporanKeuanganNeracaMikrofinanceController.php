<?php

namespace App\Http\Controllers\LaporanKeuangan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PDF;

class LaporanKeuanganNeracaMikrofinanceController extends Controller
{
    public function index(){
        $rand = rand(100000000000, 999999999);
        return view('\laporanKeuangan\neracaMikrofinance', ['random' => $rand])->with([
            'title' => 'Laporan Keuangan - Neraca Mikrofinance'
        ]);;
    }
    public function cetakPdf(){
        $rand = rand(100000000000, 999999999);
        $pdf = PDF::loadview('\laporanKeuangan\neracaMikrofinanceCetak', ['random' => $rand])->setPaper('a4', 'potrait');
        return $pdf->stream();
    }
}
