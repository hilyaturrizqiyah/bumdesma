<?php

namespace App\Http\Controllers\LaporanKeuangan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PDF;

class LaporanKeuanganRugiLabaMikrofinanceController extends Controller
{
    public function index(){
        $rand = rand(100000, 5000000);
        return view('\laporanKeuangan\rugiLabaMikrofinance', ['random' => $rand])->with([
            'title' => 'Laporan Keuangan - Rugi Laba Mikrofinance'
        ]);;
    }
    public function cetakPdf(){
        $rand = rand(100000000000, 999999999);
        $pdf = PDF::loadview('\laporanKeuangan\rugiLabaMikrofinanceCetak', ['random' => $rand])->setPaper('a4', 'potrait');
        return $pdf->stream();
    }
}
