<?php

namespace App\Http\Controllers\Laporan\Kelompok;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Notifications\Action;
use Illuminate\Support\Facades\DB;

class LaporanKelompokUEP extends Controller
{
    public function index()
    {
        $data = DB::table('tb_kelompok')->where('jenis_kelompok','2')->get();
        return view('Laporan.Kelompok.kelompok-uep', ['data' => $data])->with([
            'title' => 'Laporan Kelompok - Daftar Kelompok UEP'
        ]);
    }
}
