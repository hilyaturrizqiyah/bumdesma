<?php

namespace App\Http\Controllers\Laporan\Kelompok;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class LaporanKelompokSPKP extends Controller
{

    public function index()
    {
        $data = DB::table('tb_kelompok')->where('jenis_kelompok', '1')->get();
        return view('Laporan.Kelompok.kelompok-spkp', ['data' => $data])->with([
            'title' => 'Laporan Kelompok - Daftar Kelompok SPKP'
        ]);
    }
}
