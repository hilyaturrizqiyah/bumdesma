<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SppController extends Controller
{
    public function kasSpkp(){
        $rand = rand(100000, 5000000);
        return view('\spp\kasSpkp',['random' => $rand]);
    }

    public function bankSpkp(){
        $rand = rand(100000, 5000000);
        return view('\spp\bankSpkp',['random' => $rand]);
    }
}
