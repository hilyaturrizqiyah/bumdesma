<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BerkasController extends Controller
{
    public function cover(){
        return view('\berkas\cover');
    }
    public function daftarIsi(){
        return view('\berkas\daftarIsi');
    }
    public function suratPengantar(){
        return view('\berkas\suratPengantar');
    }
    public function kataPengantar(){
        return view('\berkas\kataPengantar');
    }
    public function profilPengurusUpk(){
        return view('\berkas\profilPengurusUpk');
    }
    public function pembatasLaporan(){
        return view('\berkas\pembatasLaporan');
    }
}
