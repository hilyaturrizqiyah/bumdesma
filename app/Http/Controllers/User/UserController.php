<?php

namespace App\Http\Controllers\User;

use \App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class UserController extends Controller
{
    public function index()
    {
        $auth = Auth::user();

        $users = User::all();

        $data['user'] = $auth;
        $data['users'] = $users;

        return view('users.userList', $data)->with([
            'title' => 'User Lists'
        ]);
    }

    public function create(){
        $auth = Auth::user();

        $roles = [
            [
                'id' => 1,
                'slug' => 'admin',
                'name' => 'Admin',
                'desc' => 'Punya akses ke semua menu'
            ],
            [
                'id' => 2,
                'slug' => 'member',
                'name' => 'Member',
                'desc' => 'Hanya bisa lihat pengguna saja'
            ]
        ];

        $data['roles'] = $roles;
        $data['auth'] = $auth;

        return view('users.userCreate', $data)
            ->with([
                'title' => 'Create User'
            ]);
    }

    public function submit(Request $request){
        dd($request->all());
    }

    public function userDetail($id){
        $auth = Auth::user();

        $userDetail = User::query()
            ->where('id', $id)
            ->with('roles')
            ->first();

        $explode = explode(' ', $userDetail->name);
        $firstName = $explode[0];

        $data['user'] = $auth;
        $data['first_name'] = $firstName;
        $data['user_detail'] = $userDetail;

        return view('users.userDetail', $data)->with([
            'title' => 'User Detail'
        ]);
    }

    public function userEditDetail($id){

        $auth = Auth::user();

        $userDetail = User::query()
            ->where('id', $id)
            ->with('roles')
            ->first();

        $roles = [];
        foreach ($auth->roles as $role){
            $roles[] = $role->slug;
        }

        if(in_array('admin', $roles) || $auth->id == $userDetail->id){
            // Do Nothing
        }
        else{
            return redirect()->back();
        }

        $dummyRole = [
            [
                'id' => 1,
                'slug' => 'admin',
                'name' => 'Admin',
                'desc' => 'Punya akses ke semua menu'
            ],
            [
                'id' => 2,
                'slug' => 'member',
                'name' => 'Member',
                'desc' => 'Hanya bisa lihat pengguna saja'
            ]
        ];

        $dummyUser = [
            'nik' => '3201012508010012',
            'name' => 'Super Admin',
            'email' => 'admin@admin.com',
            'telephone' => '89510102525',
            'jabatan' => 'Ketua',
            'divisi' => 'upk',
            'pbod' => 'Bogor',
            'bod' => '1996-05-12',
            'address' => 'this is an example of address',
            'gender' => 'laki-laki',
            'roles' => [
                '1', '3'
            ]
        ];

        $data['roles'] = $dummyRole;
        $data['user'] = $auth;
        $data['user_detail'] = $dummyUser;

        return view('users.userEditDetail', $data)->with([
            'title' => 'User Edit Detail'
        ]);
    }

    public function submitEdit(Request $request, $id){
        $auth = Auth::user();

        $userDetail = User::query()
            ->where('id', $id)
            ->with('roles')
            ->first();

        $userDetail->name = $request->name;

        return redirect()->back()->with([
            'message' => 'User berhasil diupdate'
        ]);
    }

    public function delete($id){
        $auth = Auth::user();

        $userDetail = User::query()->where('id', $id)->first();

        if ($userDetail->id == $auth->id){
            return redirect()->back()->with([
                'message' => 'Ga bisa menghapus user sendiri'
            ]);
        }

        $userDetail->delete();

        return redirect()->back()->with([
            'message' => 'User berhasil dihapus'
        ]);
    }

}
