<?php

namespace App\Http\Controllers\Anggota\Kelompok;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class KelompokListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('tb_kelompok')->get();
        return view('Anggota.listKelompok.listKelompokShow',['data' => $data])->with([
            'title' => 'Daftar Kelompok'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $desa = DB::table('tb_desa')->get();
        $jenisUsaha = DB::table('m_jenis_usaha')->get();
        $jenisKegiatan = DB::table('m_jenis_kegiatan')->get();
        return view('Anggota.listKelompok.listKelompokCreate',[
            'desa' => $desa,
            'jenisUsaha' => $jenisUsaha,
            'jenisKegiatan' => $jenisKegiatan,
        ])->with([
            'title' => 'Kelompok'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'namaKelompok' => 'unique:tb_kelompok,nama_kelompok'
        ]);

        DB::table('tb_kelompok')->insert([
            'jenis_kelompok' => $request->jenisKelompok,
            'kode_kelompok' => $request->kodeKelompok,
            'nama_kelompok' => $request->namaKelompok,
            'desa' => $request->desa,
            'alamat_kelompok' => $request->alamatKelompok,
            'telp_kelompok' => $request->telp,
            'nama_ketua' => $request->namaKetua,
            'alamat_ketua' => $request->alamatKetua,
            'anggota_awal' => $request->anggotaAwal,
            'anggota_sekarang' => $request->anggotaSekarang,
            'tanggal_berdiri' => $request->tanggalBerdiri,
            'tanggal_bergabung' => $request->tanggalBergabung,
            'awal_pinjaman' => $request->awalPinjaman,
            'jenis_usaha' => $request->jenisUsaha,
            'jenis_kegiatan' => $request->jenisKegiatan,
            'fungsi' => $request->fungsi,
            'tingkat' => $request->tingkat,
            'status' => $request->status,
        ]);

        return redirect('kelompok-list')->with([
            'title' => 'Kelompok',
            'success' => 'Data Berhasil Di Tambah'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = DB::table('tb_kelompok')->where('id',$id)->get();
        return view('Anggota.listKelompok.listKelompokDetail',['data' => $data])->with([
            'title' => 'Daftar Kelompok'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
