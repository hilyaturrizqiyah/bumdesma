<?php

namespace App\Http\Controllers\Master;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class MasterAgamaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('m_agama')->get();
        return view('Master.Agama.agamaShow', ['data' => $data])->with([
            'title' => 'Master Agama'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Master.Agama.agamaCreate')->with([
            'title' => 'Create Agama'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'agama' => 'required|unique:m_agama,agama'
        ]);

        DB::table('m_agama')->insert([
            'agama' => $request->agama,
            'keterangan' => $request->keterangan,
            'created_at' => Carbon::now()
        ]);

        return redirect('master-agama')->with([
            'title' => 'Master Agama',
            'success' => 'Data Berhasil Ditambah'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('m_agama')->where('id',$id)->get();
        return view('Master.Agama.agamaEdit',['data' => $data])->with([
            'title' => 'Master Agama'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('m_agama')->where('id',$id)->update([
            'agama' => $request->agama,
            'keterangan' => $request->keterangan,
            'updated_at' => Carbon::now()
        ]);

        return redirect('master-agama')->with([
            'title' => 'Master Agama',
            'success' => 'Data Berhasil Di Update'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('m_agama')->where('id', $id)->delete();
        return redirect('master-agama')->with([
            'title' => 'Master Agama',
            'success' => 'Data Berhasil Dihapus'
        ]);
    }
}
