<?php

namespace App\Http\Controllers\Master;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class MasterJenisKegiatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('m_jenis_kegiatan')->get();
        return view('Master.JenisKegiatan.jenisKegiatanShow', ['data' => $data])->with([
            'title' => 'Master Jenis Kegiatan'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Master.JenisKegiatan.jenisKegiatanCreate')->with([
            'title' => 'Create Jenis Kegiatan'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'jenisKegiatan' => 'required|unique:m_jenis_kegiatan,jenis_kegiatan'
        ]);

        DB::table('m_jenis_kegiatan')->insert([
            'jenis_kegiatan' => $request->jenisKegiatan,
            'keterangan' => $request->keterangan,
            'created_at' => Carbon::now()
        ]);

        return redirect('master-jenis-kegiatan')->with([
            'title' => 'Master Jenis Kegiatan',
            'success' => 'Data Berhasil Ditambah'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('m_jenis_kegiatan')->where('id',$id)->get();
        return view('Master.JenisKegiatan.jenisKegiatanEdit',['data' => $data])->with([
            'title' => 'Master Jenis Kegiatan'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('m_jenis_kegiatan')->where('id',$id)->update([
            'jenis_kegiatan' => $request->jenisKegiatan,
            'keterangan' => $request->keterangan,
            'updated_at' => Carbon::now()
        ]);

        return redirect('master-jenis-kegiatan')->with([
            'title' => 'Master Jenis Kegiatan',
            'success' => 'Data Berhasil Di Update'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('m_jenis_kegiatan')->where('id', $id)->delete();
        return redirect('master-jenis-kegiatan')->with([
            'title' => 'Master Jenis Kegiatan',
            'success' => 'Data Berhasil Dihapus'
        ]);
    }
}
