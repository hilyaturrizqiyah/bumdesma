<?php

namespace App\Http\Controllers\Master;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class MasterPekerjaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('m_pekerjaan')->get();
        return view('Master.Pekerjaan.pekerjaanShow', ['data' => $data])->with([
            'title' => 'Master Pekerjaan'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Master.Pekerjaan.pekerjaanCreate')->with([
            'title' => 'Create Pekerjaan'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'pekerjaan' => 'required|unique:m_pekerjaan,pekerjaan'
        ]);

        DB::table('m_pekerjaan')->insert([
            'pekerjaan' => $request->pekerjaan,
            'keterangan' => $request->keterangan,
            'created_at' => Carbon::now()
        ]);

        return redirect('master-pekerjaan')->with([
            'title' => 'Master Pekerjaan',
            'success' => 'Data Berhasil Ditambah'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('m_pekerjaan')->where('id',$id)->get();
        return view('Master.Pekerjaan.pekerjaanEdit',['data' => $data])->with([
            'title' => 'Master Pekerjaan'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('m_pekerjaan')->where('id',$id)->update([
            'pekerjaan' => $request->pekerjaan,
            'keterangan' => $request->keterangan,
            'updated_at' => Carbon::now()
        ]);

        return redirect('master-pekerjaan')->with([
            'title' => 'Master Pekerjaan',
            'success' => 'Data Berhasil Di Update'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('m_pekerjaan')->where('id', $id)->delete();
        return redirect('master-pekerjaan')->with([
            'title' => 'Master Pekerjaan',
            'success' => 'Data Berhasil Dihapus'
        ]);
    }
}
