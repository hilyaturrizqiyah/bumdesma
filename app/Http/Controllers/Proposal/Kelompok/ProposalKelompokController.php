<?php

namespace App\Http\Controllers\Proposal\Kelompok;

use App\Models\Kelompok;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class ProposalKelompokController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Proposal.Kelompok.proposalShow')->with([
            'title' => 'Proposal'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = DB::select('select id,kode_kelompok,jenis_kelompok,nama_kelompok,desa,nama_ketua,telp_kelompok,status from tb_kelompok');
        return view('Proposal.Kelompok.listKelompok',['data' => $data])->with([
            'title' => 'Daftar Kelompok'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function choose($id){
        return $id;
    }

    public function show($id)
    {
        $data['kelompok'] = Kelompok::with('anggota.usaha')->where('id', $id)->first();

        return view('Proposal.Kelompok.registrasi.showKelompok', $data)->with([
            'title' => 'Proposal'
        ]);
    }

    public function detail($id)
    {
        return view('Proposal.Kelompok.detailKelompok')->with([
            'title' => 'Detail'
        ]);
    }

    public function addMember(Request $request)
    {
        dd($request);
    }

    public function registering($id)
    {
        $data['kelompok'] = Kelompok::with('anggota.usaha')->where('id', $id)->first();

        return view('Proposal.Kelompok.registrasi.registrasiKelompok', $data)->with([
            'title' => 'Proposal'
        ]);
    }

    public function registerProcess($id, Request $request)
    {
        return redirect()->route('proposal-kelompok.register-document', $id);
    }

    public function document($id)
    {
        return view('Proposal.Kelompok.registrasi.showDocument')->with([
            'title' => 'Cetak Dokumen'
        ]);
    }


    public function verificationMember($id)
    {
        $data['kelompok'] = Kelompok::with(['anggota.usaha', 'anggota.proposal_detail'])->where('id', $id)->first();

        return view('Proposal.Kelompok.verifikasi.showKelompok', $data)->with([
            'title' => 'Verifikasi'
        ]);
    }

    public function verificationShow($id, Request $request)
    {
        return view('Proposal.Kelompok.verifikasi.verifikasiKelompok')->with([
            'title' => 'Verifikasi'
        ]);
    }

    public function verificationProcess($id, Request $request)
    {
        return redirect()->route('proposal-kelompok.verfication-summary', 2);
    }

    public function verificationSummary($id)
    {
        return view('Proposal.Kelompok.verifikasi.verificationSummary')->with([
            'title' => 'Verifikasi Selesai'
        ]);
    }

    public function penetapanShow($id)
    {
        return view('Proposal.Kelompok.penetapan.penetapanKelompok')->with([
            'title' => 'Penetapan'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
