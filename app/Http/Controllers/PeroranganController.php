<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PeroranganController extends Controller
{
    public function kasPerorangan(){
        $rand = rand(100000, 5000000);
        return view('\perorangan\kasPerorangan', ['random' => $rand]);
    }

    public function bankPerorangan(){
        $rand = rand(100000, 5000000);
        return view('\perorangan\bankPerorangan', ['random' => $rand]);
    }
}
