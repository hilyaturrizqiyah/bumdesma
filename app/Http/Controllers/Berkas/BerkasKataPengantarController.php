<?php

namespace App\Http\Controllers\Berkas;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PDF;

class BerkasKataPengantarController extends Controller
{
    public function index(){
        return view('\berkas\kataPengantar');
    }
}
