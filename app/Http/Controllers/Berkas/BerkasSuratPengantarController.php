<?php

namespace App\Http\Controllers\Berkas;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PDF;

class BerkasSuratPengantarController extends Controller
{
    public function index(){
        return view('\berkas\suratPengantar');
    }
    
    public function cetakPdf(){
        $pdf = PDF::loadview('\berkas\suratPengantarCetak')->setPaper('a4', 'potrait');
    	return $pdf->stream();
    }
}
