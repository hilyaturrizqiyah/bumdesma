<?php

namespace App\Http\Controllers\Berkas;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PDF;

class BerkasDaftarIsiController extends Controller
{
    public function index(){
        return view('\berkas\daftarIsi');
    }

    public function cetakPdf(){
        $pdf = PDF::loadview('\berkas\daftarIsiCetak')->setPaper('a4', 'potrait');
        return $pdf->stream();
    }
}
