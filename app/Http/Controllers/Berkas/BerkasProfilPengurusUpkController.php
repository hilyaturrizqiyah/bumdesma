<?php

namespace App\Http\Controllers\Berkas;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PDF;

class BerkasProfilPengurusUpkController extends Controller
{
    public function index(){
        return view('\berkas\profilPengurusUpk');
    }

    public function cetakPdf(){
        $rand = rand(100000000000, 999999999);
        $pdf = PDF::loadview('\berkas\profilPengurusUpkCetak', ['random' => $rand])->setPaper('a4', 'landscape');
        return $pdf->stream();
    }
}
