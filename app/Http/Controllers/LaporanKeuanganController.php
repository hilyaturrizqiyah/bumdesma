<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LaporanKeuanganController extends Controller
{
    public function neracaMikrofinance(){
        $rand = rand(100000, 5000000);
        return view('\laporanKeuangan\neracaMikrofinance', ['random' => $rand]);
    }

    public function rugiLabaMikrofinance(){
        $rand = rand(100000, 5000000);
        return view('\laporanKeuangan\rugiLabaMikrofinance', ['random' => $rand]);
    }
}
