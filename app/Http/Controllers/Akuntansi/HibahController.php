<?php

namespace App\Http\Controllers\Akuntansi;

use App\Http\Controllers\Controller;

class HibahController extends Controller
{
    public function index()
    {
        return view('Akuntansi.hibah')->with([
            'title' => 'Hibah'
        ]);
    }

    public function create()
    {
        return view('Akuntansi.hibahCreate')->with([
            'title' => 'Create Hibah'
        ]);
    }
}
