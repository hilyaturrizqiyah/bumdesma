<?php

namespace App\Http\Controllers\Akuntansi;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TutupBukuController extends Controller
{
    public function validator(){
        $auth = Auth::user();

        return view('Akuntansi.TutupBuku.validator')
            ->with([
                'title' => 'Validasi'
            ]);
    }

    public function pembagianSurplus(){
        $auth = Auth::user();

        return view('Akuntansi.TutupBuku.pembagianSurplus')
            ->with([
                'title' => 'Pembagian Surplus'
            ]);
    }
    public function perubahanSurplus(){
        $auth = Auth::user();

        return view('Akuntansi.TutupBuku.perubahanSurplus')
            ->with([
                'title' => 'Perubahan Surplus'
            ]);
    }
    public function saldoAwal(){
        $auth = Auth::user();

        return view('Akuntansi.TutupBuku.saldoAwal')
            ->with([
                'title' => 'Saldo Awal'
            ]);
    }
    public function tutupBuku(){
        $auth = Auth::user();

        return view('Akuntansi.TutupBuku.tutupBuku')
            ->with([
                'title' => 'Proses Tutup Buku'
            ]);
    }
    public function neracaAwal(){
        $auth = Auth::user();

        return view('Akuntansi.TutupBuku.neracaAwal')
            ->with([
                'title' => 'Neraca Awal'
            ]);
    }

    public function laporanNeracaAwal(){
        return null;
    }
}
