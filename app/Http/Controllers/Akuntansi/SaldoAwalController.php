<?php

namespace App\Http\Controllers\Akuntansi;

use App\Http\Controllers\Controller;

class SaldoAwalController extends Controller
{
    public function index()
    {
        return view('Akuntansi.saldoAwal')->with([
            'title' => 'Saldo Awal'
        ]);
    }

    public function create(){

        return view('Akuntansi.saldoAwalCreate')
            ->with([
                'title' => 'Create Saldo Awal'
            ]);
    }
}
