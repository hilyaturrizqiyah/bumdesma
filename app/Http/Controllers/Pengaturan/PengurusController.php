<?php

namespace App\Http\Controllers\Pengaturan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PengurusController extends Controller
{
    public function index()
    {
        $auth = Auth::user();

        $userLists = [
            [
                'id' => 1,
                'nik' => '3201012605020014',
                'name' => 'Adyatma Mahavir Alister Bagaskara',
                'address' => 'How about this a very very long text that cannot be overflowed thus this text should be overflow, lets see the result',
                'jabatan' => 'Ketua',
                'kepengurusan' => 'UPP'
            ],
            [
                'id' => 2,
                'nik' => '3201012605020011',
                'name' => 'Muhammad Syaifullah Nurrohman',
                'address' => 'How about this a very very long text that cannot be overflowed thus this text should be overflow, lets see the result',
                'jabatan' => 'Staff',
                'kepengurusan' => 'UPP'
            ],
        ];

        $woow =  [];
        foreach ($userLists as $item){
            $item['name'] = strlen($item['name']) > 30 ? substr($item['name'],0,30)."..." : $item['name'];

            $item['address'] = strlen($item['address']) > 40 ? substr($item['address'],0,40)."..." : $item['address'];

            $woow[] = $item;
        }

        $data['pengurus'] = $woow;

        return view('Pengaturan.pengurus.pengurusLists', $data)
            ->with([
                'title' => 'Pengurus'
            ]);
    }

    public function create(){
        $auth = Auth::user();

        return view('Pengaturan.pengurus.pengurusCreate')
            ->with([
                'title' => 'Create Pengurus'
            ]);
    }

    public function edit($id){
        $auth = Auth::user();

        $data = [
            "nik" => "3201012605020014",
            "email" => "dyatma@gmail.com",
            "jabatan" => "staff",
            "pbod" => "Bogor",
            "bod" => "1996-03-24",
            "address" => "Perumahan Griya Cibinonng Indah Blok K-6",
            "fullname" => "Adyatma Mahavir Alister Bagaskara",
            "telephone" => "0895404035511",
            "divisi" => "upk",
            "gender" => 'laki-laki',
            'image' => '/example/path/image'
        ];

        $data['pengurus'] = $data;

        return view('Pengaturan.pengurus.pengurusEdit', $data)
            ->with([
                'title' => 'Edit Pengurus'
            ]);

    }

    public function submit(Request $request){

        dd($request->all());
    }

    public function delete($id){

        return redirect()->back()->with([
            'message' => 'Pengurus berhasil dihapus'
        ]);
    }
}
