<?php

namespace App\Http\Controllers\Pengaturan;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class PengaturanKecamatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::select('SELECT a.id,a.kode,a.nama_upk,a.alamat_upk,b.name as propinsi, c.name as kota, a.telp_upk,a.email_upk,a.website_upk,a.ketua_upk,a.sekretaris_upk,a.bendahara_upk,a.ketua_bkad,a.ketua_bpupk,a.logo_upk from tb_profil a, indonesia_provinces b, indonesia_cities c where a.propinsi_upk = b.id and a.kota_kab_upk = c.id');

        $count = DB::table('tb_profil')->count();
        return view('Pengaturan.Kecamatan.kecamatanShow',['data' => $data])->with([
            'title'=> 'Profile',
            'count' => $count
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
