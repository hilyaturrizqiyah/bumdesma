<?php

namespace App\Http\Controllers\SPP;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PDF;

class SppBankSpkpController extends Controller
{
    public function index(){
        $rand = rand(100000000000, 999999999);
        return view('\spp\bankSpkp',['random' => $rand]);
    }

    public function cetakPdf(){
        $rand = rand(100000000000, 999999999);
        $pdf = PDF::loadview('\spp\BankSpkpCetak', ['random' => $rand])->setPaper('a4', 'landscape');
        return $pdf->stream();
    }
}