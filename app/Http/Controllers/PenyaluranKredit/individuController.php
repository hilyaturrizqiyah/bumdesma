<?php

namespace App\Http\Controllers\PenyaluranKredit;

use App\Http\Controllers\Controller;

class individuController extends Controller
{
    public function uepIndex(){

        return view('penyaluranKredit.individual.individualIndex')
            ->with([
                'title' => 'SPP'
            ]);
    }

    public function sppIndex(){

        return view('penyaluranKredit.individual.individualIndex')
            ->with([
                'title' => 'SPP'
            ]);
    }

    public function individualIndex(){
        return view('penyaluranKredit.individual.individualIndex')
            ->with([
                'title' => 'SPP'
            ]);
    }
}
