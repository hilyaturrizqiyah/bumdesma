<?php

namespace App\Http\Controllers\PenyaluranKredit;

use App\Http\Controllers\Controller;

class kelompokController extends Controller
{

    public function uepIndex(){

        return view('penyaluranKredit.kelompok.kelompokIndex')
            ->with([
                'title' => 'SPP'
            ]);
    }

    public function sppIndex(){

        return view('penyaluranKredit.kelompok.kelompokIndex')
            ->with([
                'title' => 'SPP'
            ]);
    }
}
