<?php

namespace App\Http\Controllers\Perorangan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PDF;
class PeroranganKasPeroranganController extends Controller
{
    public function index(){
        $rand = rand(100000000000, 999999999);
        return view('\perorangan\kasPerorangan', ['random' => $rand])->with([
            'title' => 'Perorangan - Kas Perorangan'
        ]);;
    }
    public function cetakPdf(){
        $rand = rand(100000000000, 999999999);
        $pdf = PDF::loadview('\perorangan\kasPeroranganCetak', ['random' => $rand])->setPaper('a4', 'landscape');
        return $pdf->stream();
    }
}
