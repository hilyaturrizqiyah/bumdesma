<?php

namespace App\Http\Controllers\Perorangan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PDF;
class PeroranganBankPeroranganController extends Controller
{
    public function index(){
        $rand = rand(100000000000, 999999999);
        return view('\perorangan\bankPerorangan', ['random' => $rand])->with([
            'title' => 'Perorangan - Bank Perorangan'
        ]);
    }
    public function cetakPdf(){
        $rand = rand(100000000000, 999999999);
        $pdf = PDF::loadview('\perorangan\bankPeroranganCetak', ['random' => $rand])->setPaper('a4', 'landscape');
        return $pdf->stream();
    }
}
