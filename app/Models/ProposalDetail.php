<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProposalDetail extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $table = 'tb_proposal_detail';

    public function anggota(){
        return $this->belongsTo(AnggotaKelompok::class, 'id_anggota', 'id');
    }
}
