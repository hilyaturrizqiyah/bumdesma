<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AnggotaKelompok extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $table = 'tb_anggota_kelompok';

    public function kelompok(){
        return $this->belongsTo(Kelompok::class, 'id_kelompok', 'id');
    }

    public function proposal_detail(){
        return $this->hasOne(ProposalDetail::class, 'id_anggota', 'id');
    }

    public function usaha(){
        return $this->belongsTo(JenisUsaha::class, 'jenis_usaha', 'id');
    }
}
