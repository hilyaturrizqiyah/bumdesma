<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kelompok extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $table = 'tb_kelompok';

    public function anggota(){
        return $this->hasMany(AnggotaKelompok::class, 'id_kelompok', 'id');
    }
}
