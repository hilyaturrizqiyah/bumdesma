<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/config-clear', function () {
    Artisan::call('config:clear');
    return 'Configuration cache cleared!';
});

Route::get('/config-cache', function () {
    Artisan::call('config:cache');
    return 'Configuration cache cleared! <br> Configuration cached successfully!';
});

Route::get('/cache-clear', function () {
    Artisan::call('cache:clear');
    return 'Application cache cleared!';
});

Route::get('/view-cache', function () {
    Artisan::call('view:cache');
    return 'Compiled views cleared! <br> Blade templates cached successfully!';
});

Route::get('/view-clear', function () {
    Artisan::call('view:clear');
    return 'Compiled views cleared!';
});

Route::get('/route-cache', function () {
    Artisan::call('route:cache');
    return 'Route cache cleared! <br> Routes cached successfully!';
});

Route::get('/route-clear', function () {
    Artisan::call('route:clear');
    return 'Route cache cleared!';
});

Route::get('/storage-link', function () {
    Artisan::call('storage:link');
    return 'The links have been created.';
});

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'root'])->name('root');
// Route::get('/testing', [App\Http\Controllers\Pengaturan\PengaturanProfileController::class, 'index'])->name('index');

// Master Data
Route::resource('master-jenis-kegiatan', App\Http\Controllers\Master\MasterJenisKegiatanController::class);
Route::resource('master-jenis-usaha', App\Http\Controllers\Master\MasterJenisUsahaController::class);
Route::resource('master-pekerjaan', App\Http\Controllers\Master\MasterPekerjaanController::class);
Route::resource('master-agama', App\Http\Controllers\Master\MasterAgamaController::class);
Route::resource('master-kekeluargaan', App\Http\Controllers\Master\MasterKekeluargaanController::class);
Route::resource('master-status-perkawinan', App\Http\Controllers\Master\MasterStatusPerkawinanController::class);

//Update User Details
Route::post('/update-profile/{id}', [App\Http\Controllers\HomeController::class, 'updateProfile'])->name('updateProfile');
Route::post('/update-password/{id}', [App\Http\Controllers\HomeController::class, 'updatePassword'])->name('updatePassword');

// Route::get('{any}', [App\Http\Controllers\HomeController::class, 'index1'])->name('index1');

//Language Translation
Route::get('index/{locale}', [App\Http\Controllers\HomeController::class, 'lang']);

// Pengaturan
Route::resource('pengaturan-profile', App\Http\Controllers\Pengaturan\PengaturanProfileController::class);
Route::resource('pengaturan-kecamatan', App\Http\Controllers\Pengaturan\PengaturanKecamatanController::class);
Route::resource('pengaturan-desa', App\Http\Controllers\Pengaturan\PengaturanDesaController::class);
Route::prefix('pengaturan')->group(function () {
    Route::prefix('pengurus')->group(function () {
        Route::get('/', 'App\Http\Controllers\Pengaturan\PengurusController@index')->name('listPengurus');
        Route::get('/create', 'App\Http\Controllers\Pengaturan\PengurusController@create')->name('createPengurus');
        Route::get('/edit/{id}', 'App\Http\Controllers\Pengaturan\PengurusController@edit')->name('editPengurus');
        Route::post('/submit', 'App\Http\Controllers\Pengaturan\PengurusController@submit')->name('submitPengurus');
        Route::delete('/delete/{id}', 'App\Http\Controllers\Pengaturan\PengurusController@index')->name('deletePengurus');
    });
});

// Akuntansi
Route::prefix('akuntansi')->group(function (){
    Route::get('periode-akuntansi', 'App\Http\Controllers\Akuntansi\PeriodeAkuntansiController@index');
    Route::prefix('saldo-awal')->group(function (){
        Route::get('/', 'App\Http\Controllers\Akuntansi\SaldoAwalController@index');
        Route::get('/create', 'App\Http\Controllers\Akuntansi\SaldoAwalController@create');
        Route::post('/submit', 'App\Http\Controllers\Akuntansi\SaldoAwalController@submit')->name('saldoAwalSubmit');
    });
    Route::prefix('hibah')->group(function (){
        Route::get('/', 'App\Http\Controllers\Akuntansi\HibahController@index');
        Route::get('/create', 'App\Http\Controllers\Akuntansi\HibahController@create');
    });
    Route::prefix('tutup-buku')->group(function(){
        Route::get('/validator', 'App\Http\Controllers\Akuntansi\TutupBukuController@validator');
        Route::get('/pembagian-surplus', 'App\Http\Controllers\Akuntansi\TutupBukuController@pembagianSurplus');
        Route::get('/perubahan-surplus', 'App\Http\Controllers\Akuntansi\TutupBukuController@perubahanSurplus');
        Route::get('/saldo-awal', 'App\Http\Controllers\Akuntansi\TutupBukuController@saldoAwal');
        Route::get('/proses-tutup-buku', 'App\Http\Controllers\Akuntansi\TutupBukuController@tutupBuku');
        Route::get('/neraca-awal', 'App\Http\Controllers\Akuntansi\TutupBukuController@neracaAwal');
        Route::post('/neraca-awal', 'App\Http\Controllers\Akuntansi\TutupBukuController@laporanNeracaAwal')->name('laporanNeracaAwal');
    });
});

// User
Route::prefix('user')->group(function (){
    Route::get('/', 'App\Http\Controllers\User\UserController@index')->name('user.show');
    Route::get('/create', 'App\Http\Controllers\User\UserController@create')->name('userCreate');
    Route::post('/submit', 'App\Http\Controllers\User\UserController@submit')->name('userSubmit');
    Route::get('user-detail/{id}', 'App\Http\Controllers\User\UserController@userDetail')->name('user.detail');
    Route::get('user-edit/{id}', 'App\Http\Controllers\User\UserController@userEditDetail')->name('user.edit.detail');
    Route::post('edit', 'App\Http\Controllers\User\UserController@submitEdit')->name('user.update');
    Route::delete('delete/{id}', 'App\Http\Controllers\User\UserController@delete')->name('user.delete');
});


// Keanggotaan 
Route::resource('kelompok-list', App\Http\Controllers\Anggota\Kelompok\KelompokListController::class);
Route::resource('kelompok-anggota', App\Http\Controllers\Anggota\Kelompok\KelompokAnggotaController::class);
Route::resource('individu-anggota', App\Http\Controllers\Anggota\Individu\IndividuAnggotaController::class);

// Proposal
Route::resource('proposal-kelompok', App\Http\Controllers\Proposal\Kelompok\ProposalKelompokController::class);
Route::post('/tambah-anggota', [App\Http\Controllers\Proposal\Kelompok\ProposalKelompokController::class, 'addMember'])->name('proposal-kelompok.add-member');
// Detail kelompok
Route::get('/proposal-kelompok/{id}/detail', [App\Http\Controllers\Proposal\Kelompok\ProposalKelompokController::class, 'detail'])->name('proposal-kelompok.detail');
// Registrasi Proposal
Route::post('/tambah-anggota', [App\Http\Controllers\Proposal\Kelompok\ProposalKelompokController::class, 'addMember'])->name('proposal-kelompok.add-member');
Route::get('/proposal-kelompok/{id}/register', [App\Http\Controllers\Proposal\Kelompok\ProposalKelompokController::class, 'registering'])->name('proposal-kelompok.registering');
Route::post('/proposal-kelompok/{id}/register', [App\Http\Controllers\Proposal\Kelompok\ProposalKelompokController::class, 'registerProcess'])->name('proposal-kelompok.register-process');
Route::get('/proposal-kelompok/{id}/dokumen', [App\Http\Controllers\Proposal\Kelompok\ProposalKelompokController::class, 'document'])->name('proposal-kelompok.register-document');
// Verifikasi Proposal
Route::get('/proposal-kelompok/{id}/verifikasi-anggota', [App\Http\Controllers\Proposal\Kelompok\ProposalKelompokController::class, 'verificationMember'])->name('proposal-kelompok.verification-member');
Route::get('/proposal-kelompok/{id}/verifikasi', [App\Http\Controllers\Proposal\Kelompok\ProposalKelompokController::class, 'verificationShow'])->name('proposal-kelompok.verification-show');
Route::post('/proposal-kelompok/{id}/verifikasi', [App\Http\Controllers\Proposal\Kelompok\ProposalKelompokController::class, 'verificationProcess'])->name('proposal-kelompok.verification-process');
Route::get('/proposal-kelompok/{id}/rangkuman-verifikasi', [App\Http\Controllers\Proposal\Kelompok\ProposalKelompokController::class, 'verificationSummary'])->name('proposal-kelompok.verfication-summary');
// Penetapan Proposal
Route::get('/proposal-kelompok/{id}/penetapan', [App\Http\Controllers\Proposal\Kelompok\ProposalKelompokController::class, 'penetapanShow'])->name('proposal-kelompok.penetapan-show');
Route::post('/proposal-kelompok/{id}/penetapan', [App\Http\Controllers\Proposal\Kelompok\ProposalKelompokController::class, 'penetapanProcess'])->name('proposal-kelompok.penetapan-process');

// Pinjaman
Route::resource('pinjaman-kelompok', App\Http\Controllers\Pinjaman\Kelompok\PinjamanKelompokController::class);
// Detail pinjaman kelompok
Route::get('/pinjaman-kelompok-uep/{id}/detail', [App\Http\Controllers\Pinjaman\Kelompok\PinjamanKelompokController::class, 'detailUep'])->name('pinjaman-kelompok-uep.detail');
Route::get('/pinjaman-kelompok-spp/{id}/detail', [App\Http\Controllers\Pinjaman\Kelompok\PinjamanKelompokController::class, 'detailSpp'])->name('pinjaman-kelompok-spp.detail');
Route::resource('pinjaman-individu', App\Http\Controllers\Pinjaman\Individu\PinjamanIndividuController::class);


// Transaksi
Route::resource('angsuran-kelompok', App\Http\Controllers\Angsuran\Kelompok\PenerimaanAngsuranController::class);
Route::resource('angsuran-kelompok-uep', App\Http\Controllers\Angsuran\Kelompok\UEP\PenerimaanAngsuranUepController::class);
Route::resource('angsuran-kelompok-spp', App\Http\Controllers\Angsuran\Kelompok\SPP\PenerimaanAngsuranSppController::class);
Route::resource('angsuran-kelompok-lain', App\Http\Controllers\Angsuran\Kelompok\Lain\PenerimaanAngsuranLainController::class);

Route::resource('angsuran-individu', App\Http\Controllers\Angsuran\Individu\PenerimaanAngsuranController::class);
Route::resource('angsuran-individu-uep', App\Http\Controllers\Angsuran\Individu\UEP\PenerimaanAngsuranUepController::class);
Route::resource('angsuran-individu-spp', App\Http\Controllers\Angsuran\Individu\SPP\PenerimaanAngsuranSppController::class);
Route::resource('angsuran-individu-lain', App\Http\Controllers\Angsuran\Individu\Lain\PenerimaanAngsuranLainController::class);

// Penyaluran Kredit
Route::prefix('penyaluran-kredit')->group(function (){
    Route::prefix('kelompok')->group(function (){
        Route::get('/uep', 'App\Http\Controllers\PenyaluranKredit\kelompokController@uepIndex');
        Route::get('/spp', 'App\Http\Controllers\PenyaluranKredit\kelompokController@sppIndex');
    });

    Route::prefix('individu')->group(function (){
        Route::get('/uep', 'App\Http\Controllers\PenyaluranKredit\individuController@uepIndex');
        Route::get('/spp', 'App\Http\Controllers\PenyaluranKredit\individuController@sppIndex');
        Route::get('/perorangan', 'App\Http\Controllers\PenyaluranKredit\individuController@individualIndex');
    });
});


// Route::get('provinces', 'DependentDropdownController@provinces')->name('provinces');
Route::get('/provinces', [\App\Http\Controllers\DependantDropdownController::class, 'provinces'])->name('provinces');
Route::get('/cities', [\App\Http\Controllers\DependantDropdownController::class, 'cities'])->name('cities');
Route::get('/districts', [\App\Http\Controllers\DependantDropdownController::class, 'districts'])->name('districts');
Route::get('/villages', [\App\Http\Controllers\DependantDropdownController::class, 'villages'])->name('villages');

/* Route Laporan */
// Laporan Keuangan
Route::get('neraca-mikrofinance', [\App\Http\Controllers\LaporanKeuangan\LaporanKeuanganNeracaMikrofinanceController::class, 'index']);
Route::get('neraca-mikrofinance/cetak-pdf', [\App\Http\Controllers\LaporanKeuangan\LaporanKeuanganNeracaMikrofinanceController::class, 'cetakPdf']);
Route::get('rugi-laba-mikrofinance', [\App\Http\Controllers\LaporanKeuangan\LaporanKeuanganRugiLabaMikrofinanceController::class, 'index']);
Route::get('rugi-laba-mikrofinance/cetak-pdf', [\App\Http\Controllers\LaporanKeuangan\LaporanKeuanganRugiLabaMikrofinanceController::class, 'cetakPdf']);

//Kelompok
Route::prefix('laporan-kelompok')->namespace('Laporan\Kelompok')->group(function () {
    Route::get('laporan-kelompok-spkp', [\App\Http\Controllers\Laporan\Kelompok\LaporanKelompokSPKP::class, 'index'])->name('lap-kel-spkp');
    Route::get('laporan-kelompok-uep', [\App\Http\Controllers\Laporan\Kelompok\LaporanKelompokUEP::class, 'index'])->name('lap-kel-uep');
});

//Buku Pembantu
Route::prefix('laporan-buku-pembantu')->namespace('Laporan\BukuPembantu')->group(function () {
    Route::get('daftar-BDD', [\App\Http\Controllers\Laporan\BukuPembantu\BukuDaftarBDD::class, 'index'])->name('daftar-bdd');
    Route::get('daftar-inventaris', [\App\Http\Controllers\Laporan\BukuPembantu\BukuDaftarInventaris::class, 'index'])->name('daftar-inven');
    Route::get('daftar-aktiva-tetap', [\App\Http\Controllers\Laporan\BukuPembantu\BukuDaftarAktivaTetap::class, 'index'])->name('daftar-aktiv');
    Route::get('daftar-biaya-lain', [\App\Http\Controllers\Laporan\BukuPembantu\BukuDaftarBiayaLain::class, 'index'])->name('daftar-biaya');
    Route::get('daftar-perguliran-SPP', [\App\Http\Controllers\Laporan\BukuPembantu\BukuDaftarPerguliranSPP::class, 'index'])->name('daftar-gulir-spp');
    Route::get('daftar-perguliran-UEP', [\App\Http\Controllers\Laporan\BukuPembantu\BukuDaftarPerguliranUEP::class, 'index'])->name('daftar-gulir-uep');
    Route::get('daftar-pemb_ITW', [\App\Http\Controllers\Laporan\BukuPembantu\BukuDaftarITW::class, 'index'])->name('daftar-pemb_itw');
});

//Rekap Pinjaman UEP
Route::prefix('laporan-pinjaman-UEP')->group(function () {

    #Kelompok
    Route::prefix('/')->namespace('Laporan\PinjamanUEP\Kelompok')->group(function () {
        Route::get('daftar-LPP-UEP-kelompok', [\App\Http\Controllers\Laporan\PinjamanUEP\Kelompok\LPP_UEP::class, 'index'])->name('lpp-uep-kelompok');
        Route::get('daftar-kolektibilitas-UEP-kelompok', [\App\Http\Controllers\Laporan\PinjamanUEP\Kelompok\KolektibilitasUEP::class, 'index'])->name('kolek-uep-kelompok');
        Route::get('daftar-penghapusan-UEP-kelompok', [\App\Http\Controllers\Laporan\PinjamanUEP\Kelompok\PenghapusanUEP::class, 'index'])->name('hapus-uep-kelompok');
        Route::get('daftar-UEP-lain-kelompok', [\App\Http\Controllers\Laporan\PinjamanUEP\Kelompok\LPP_Lain::class, 'index'])->name('uep-lain-kelompok');
    });

    #Individu
    Route::prefix('/')->namespace('Laporan\PinjamanUEP\Individu')->group(function () {
        Route::get('daftar-LPP-UEP-individu', [\App\Http\Controllers\Laporan\PinjamanUEP\Individu\LPP_UEP::class, 'index'])->name('lpp-uep-individu');
        Route::get('daftar-kolektibilitas-UEP-individu', [\App\Http\Controllers\Laporan\PinjamanUEP\Individu\KolektibilitasUEP::class, 'index'])->name('kolek-uep-individu');
    });
});

//Rekap Pinjaman SPP
Route::prefix('laporan-pinjaman-SPP')->group(function () {

    #Kelompok
    Route::prefix('/')->namespace('Laporan\PinjamanSPP\Kelompok')->group(function () {
        Route::get('LPP-SPP-kelompok', [\App\Http\Controllers\Laporan\PinjamanSPP\Kelompok\LPP_SPP::class, 'index'])->name('lpp-spp-kelompok');
        Route::get('kolektibilitas-SPKP-kelompok', [\App\Http\Controllers\Laporan\PinjamanSPP\Kelompok\KolektibilitasSPKP::class, 'index'])->name('kolek-spp-kelompok');
        Route::get('penghapusan-SPKP-kelompok', [\App\Http\Controllers\Laporan\PinjamanSPP\Kelompok\PenghapusanSPKP::class, 'index'])->name('hapus-spp-kelompok');
        Route::get('SPKP-lain-kelompok', [\App\Http\Controllers\Laporan\PinjamanSPP\Kelompok\LPP_SPKPLain::class, 'index'])->name('spp-lain-kelompok');
    });

    #Individu
    Route::prefix('/')->namespace('Laporan\PinjamanSPP\Individu')->group(function () {
        Route::get('LPP-SPP-individu', [\App\Http\Controllers\Laporan\PinjamanSPP\Individu\LPP_SPP::class, 'index'])->name('lpp-spp-individu');
        Route::get('kolektibilitas-SPP-individu', [\App\Http\Controllers\Laporan\PinjamanSPP\Individu\KolektibilitasSPP::class, 'index'])->name('kolek-spp-individu');
    });
});

//Buku Bank
Route::resource('buku-bank-spp', App\Http\Controllers\Transaksi\BukuBank\BukuBankSppController::class);
Route::resource('buku-bank-uep', App\Http\Controllers\Transaksi\BukuBank\BukuBankUepController::class);
Route::resource('buku-bank-ops', App\Http\Controllers\Transaksi\BukuBank\BukuBankOpsController::class);
Route::resource('buku-bank-perorangan', App\Http\Controllers\Transaksi\BukuBank\BukuBankPeroranganController::class);

//Laporan UEP
Route::resource('laporan-uep-kas', App\Http\Controllers\Laporan\UEP\LaporanUepKasController::class);
Route::post('laporan-uep-kas-cetak', [\App\Http\Controllers\Laporan\UEP\LaporanUepKasController::class, 'cetakLaporan']);
Route::resource('laporan-uep-bank', App\Http\Controllers\Laporan\UEP\LaporanUepBankController::class);
Route::post('laporan-uep-bank-cetak', [\App\Http\Controllers\Laporan\UEP\LaporanUepBankController::class, 'cetakLaporan']);

// SPP
Route::get('kas-spkp',[\App\Http\Controllers\SppController::class, 'kasSpkp'])->name('kasSpkp');
Route::get('bank-spkp',[\App\Http\Controllers\SppController::class, 'bankSpkp'])->name('bankSpkp');

// Perorangan
Route::get('kas-perorangan',[\App\Http\Controllers\PeroranganController::class, 'kasPerorangan'])->name('kasPerorangan');
Route::get('bank-perorangan',[\App\Http\Controllers\PeroranganController::class, 'bankPerorangan'])->name('bankPerorangan');

// Berkas
Route::get('cover',[\App\Http\Controllers\BerkasController::class, 'cover'])->name('cover');
Route::get('daftar-isi',[\App\Http\Controllers\BerkasController::class, 'daftarIsi'])->name('daftarIsi');
Route::get('surat-pengantar',[\App\Http\Controllers\BerkasController::class, 'suratPengantar'])->name('suratPengantar');
Route::get('kata-pengantar',[\App\Http\Controllers\BerkasController::class, 'kataPengantar'])->name('kataPengantar');
Route::get('profil-pengurus-upk',[\App\Http\Controllers\BerkasController::class, 'profilPengurusUpk'])->name('profilPengurusUpk');
Route::get('pembatas-laporan',[\App\Http\Controllers\BerkasController::class, 'pembatasLaporan'])->name('pembatasLaporan');


//Laporan Rekap Pijaman UEP
Route::resource('laporan-pinjaman-uep-lpp', App\Http\Controllers\Laporan\RekapPinjamanUEP\LaporanPinjamanUepLppController::class);
Route::post('laporan-pinjaman-uep-lpp-cetak', [\App\Http\Controllers\Laporan\RekapPinjamanUEP\LaporanPinjamanUepLppController::class, 'cetakLaporan']);

Route::resource('laporan-pinjaman-uep-kolekt', App\Http\Controllers\Laporan\RekapPinjamanUEP\LaporanPinjamanUepKolektController::class);
Route::post('laporan-pinjaman-uep-kolekt-cetak', [\App\Http\Controllers\Laporan\RekapPinjamanUEP\LaporanPinjamanUepKolektController::class, 'cetakLaporan']);

Route::resource('laporan-pinjaman-uep-lain', App\Http\Controllers\Laporan\RekapPinjamanUEP\LaporanPinjamanUepLainController::class);
Route::post('laporan-pinjaman-uep-lain-cetak', [\App\Http\Controllers\Laporan\RekapPinjamanUEP\LaporanPinjamanUepLainController::class, 'cetakLaporan']);

//Laporan Rekap Pijaman SPP
Route::resource('laporan-pinjaman-spp-lpp', App\Http\Controllers\Laporan\RekapPinjamanSPP\LaporanPinjamanSppLppController::class);
Route::post('laporan-pinjaman-spp-lpp-cetak', [\App\Http\Controllers\Laporan\RekapPinjamanSPP\LaporanPinjamanSppLppController::class, 'cetakLaporan']);

Route::resource('laporan-pinjaman-spp-kolekt', App\Http\Controllers\Laporan\RekapPinjamanSPP\LaporanPinjamanSppKolektController::class);
Route::post('laporan-pinjaman-spp-kolekt-cetak', [\App\Http\Controllers\Laporan\RekapPinjamanSPP\LaporanPinjamanSppKolektController::class, 'cetakLaporan']);

Route::resource('laporan-pinjaman-spp-lain', App\Http\Controllers\Laporan\RekapPinjamanSPP\LaporanPinjamanSppLainController::class);
Route::post('laporan-pinjaman-spp-lain-cetak', [\App\Http\Controllers\Laporan\RekapPinjamanSPP\LaporanPinjamanSppLainController::class, 'cetakLaporan']);
