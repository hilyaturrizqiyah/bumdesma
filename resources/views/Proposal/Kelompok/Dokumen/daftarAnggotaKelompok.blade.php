<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="<?php echo base_url('assets/images/SMT.png'); ?>">
<title>Cetak Surat keterangan Usaha</title>
</head>
<body>
	<table>
		<tr>
			<td style="width: 700px;">
				<table style="font-family: arial;">
					<tr>
						<td>
							<table>
								<tr>
									<td width="450" align="left">KELOMPOK ANGGREK</td>
									<td width="450" align="right">Lampiran1</td>
								</tr>
								<tr>
									<td width="450" align="left" style="font-size: 13px;">Alamat : DESA GARAWASTU</td>
									<td width="450" align="right" style="font-size: 13px;">Dokumen Proposal Kredit</td>
								</tr>
							</Table><Table>
								<tr>
									<td style="font-size: 2px;">&nbsp;</td>
								</tr>
								<tr>
									<td style="border-top: 2px solid #000; width: 700px;" align="center"></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td align="center" style="font-size: 25px;"><b>DAFTAR ANGGOTA KELOMPOK</b></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td style="border-top: 2px solid #000; width: 700px;" align="center"></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
							</table>
							<table>
								<tr>
									<td style="width: 200px;">Kode Registrasi</td>
									<td style="width: 350px;">: &nbsp;&nbsp;201272312873872</td>
									<td style="width: 200px;">Jenis Kelompok</td>
									<td style="width: 300px;">: &nbsp;&nbsp;SPKP</td>
								</tr>
								<tr>
									<td style="width: 200px;">Nama Kelompok</td>
									<td style="width: 350px;">: &nbsp;&nbsp;ANGGREK</td>
									<td style="width: 200px;">Jenis Usaha</td>
									<td style="width: 300px;">: &nbsp;&nbsp;SIMPAN PINJAM</td>
								</tr>
								<tr>
									<td style="width: 200px;">Desa</td>
									<td style="width: 350px;">: &nbsp;&nbsp;GARAWASTU</td>
									<td style="width: 200px;">Jenis Kegiatan</td>
									<td style="width: 300px;">: &nbsp;&nbsp;ANEKA JASA</td>
								</tr>
								<tr>
									<td style="width: 200px;">Nama Ketua</td>
									<td style="width: 350px;">: &nbsp;&nbsp;ROENAH</td>
									<td style="width: 200px;">Tingkat</td>
									<td style="width: 300px;">: &nbsp;&nbsp;Berkembang</td>
								</tr>
			
							</table>
							<br>
							<table>
								<tr>
									<td style="border-top: 2px solid #000; width: 700px;" align="center"></td>
								</tr>
							</table>
							<table border="2" cellpadding="0" cellspacing="0" align="center" style="font-size: 12px;">
								<tr>
									<td style="width: 20px;" align="center"><b>No.</b></td>
									<td style="width: 200px;" align="center"><b>Nama Lengkap</b></td>
									<td style="width: 115px;" align="center"><b>Jabatan</b></td>
									<td style="width: 50px;" align="center"><b>L/P</b></td>
									<td style="width: 70px;" align="center"><b>Umur</b></td>
									<td style="width: 130px;" align="center"><b>Jenis Usaha</b></td>
									<td style="width: 85px;" align="center"><b>Status</b></td>
								</tr>
								<tr>
									<td style="width: 20px;" align="center">1. </td>
									<td style="width: 200px;" align="LEFT"><b>ROENAH</b> <br> ID:32102503105001</td>
									<td style="width: 115px;" align="center">SEKRETARIS</td>
									<td style="width: 50px;" align="center">P</td>
									<td style="width: 70px;" align="center">51</td>
									<td style="width: 130px;" align="center">Perdagangan</td>
									<td style="width: 85px;" align="center">PEMANFAAT</td>
								</tr>
							</table>
							<table>
							<tr>
								<td width="400">&nbsp;</td>
								<td align="center">&nbsp;</td>
							</tr>
							<tr>
								<td width="400">&nbsp;</td>
								<td align="center">&nbsp;</td>
							</tr>
							<tr>
								<td width="400">&nbsp;</td>
								<td align="center">Garawastu, 11 Desember 2022</td>
							</tr>
							<tr>
								<td width="400">&nbsp;</td>
								<td align="center">Ketua Kelompok</td>
							</tr>
							<tr>
								<td width="400">&nbsp;</td>
								<td align="center">&nbsp;</td>
							</tr>
							<tr>
								<td width="400">&nbsp;</td>
								<td align="center">&nbsp;</td>
							</tr>
							<tr>
								<td width="400">&nbsp;</td>
								<td align="center">&nbsp;</td>
							</tr>
							<tr>
								<td width="400">&nbsp;</td>
								<td align="center"><b><u>ROENAH</u></b></td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>