<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="<?php echo base_url('assets/images/SMT.png'); ?>">
<title>Cetak Surat keterangan Usaha</title>
</head>
<body>
	<table>
		<tr>
			<td style="width: 700px;">
				<table style="font-family: arial;">
					<tr>
						<td>
							<table>
								<tr>
									<td>TIM VERIFIKASI KECAMATAN SINDANG</td>
								</tr>
								<tr>
									<td style="font-size: 13px;">Alamat : Jalan Jogja Kecil No. 16 Sindang Majalengka 45471</td>
								</tr>
								<tr>
									<td style="font-size: 2px;">&nbsp;</td>
								</tr>
								<tr>
									<td style="border-top: 2px solid #000; width: 700px;" align="center"></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td align="center" style="font-size: 25px;"><b>REKOMENDASI TIM VERIFIKASI</b></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td style="border-top: 2px solid #000; width: 700px;" align="center"></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
							</table>
							<table>
								<tr>
									<td style="width: 200px;">Kode Registrasi</td>
									<td style="width: 350px;">: &nbsp;&nbsp;201272312873872</td>
									<td style="width: 200px;">Jenis Kelompok</td>
									<td style="width: 300px;">: &nbsp;&nbsp;SPKP</td>
								</tr>
								<tr>
									<td style="width: 200px;">Nama Kelompok</td>
									<td style="width: 350px;">: &nbsp;&nbsp;ANGGREK</td>
									<td style="width: 200px;">Jenis Usaha</td>
									<td style="width: 300px;">: &nbsp;&nbsp;SIMPAN PINJAM</td>
								</tr>
								<tr>
									<td style="width: 200px;">Desa</td>
									<td style="width: 350px;">: &nbsp;&nbsp;GARAWASTU</td>
									<td style="width: 200px;">Jenis Kegiatan</td>
									<td style="width: 300px;">: &nbsp;&nbsp;ANEKA JASA</td>
								</tr>
								<tr>
									<td style="width: 200px;">Nama Ketua</td>
									<td style="width: 350px;">: &nbsp;&nbsp;ROENAH</td>
									<td style="width: 200px;">Tingkat</td>
									<td style="width: 300px;">: &nbsp;&nbsp;Berkembang</td>
								</tr>
								<tr>
									<td style="width: 200px;">Alamat</td>
									<td style="width: 350px;">: &nbsp;&nbsp;DESA GARAWASTU</td>
									<td style="width: 200px;">Fungsi</td>
									<td style="width: 300px;">: &nbsp;&nbsp;PENYALUR</td>
								</tr>
								<tr>
									<td style="width: 200px;">No. Telp</td>
									<td style="width: 350px;">: &nbsp;&nbsp;082138729387</td>
								</tr>
							</table>
							<br>
							<table>
								<tr>
									<td style="border-top: 2px solid #000; width: 700px;" align="center"></td>
								</tr>
							</table>
							<table border="2" cellpadding="0" cellspacing="0" align="center">
								<tr>
									<td style="width: 20px;" align="center"><b>No.</b></td>
									<td style="width: 200px;" align="center"><b>Nama Anggota</b></td>
									<td style="width: 150px;" align="center"><b>Proposal</b></td>
									<td style="width: 150px;" align="center"><b>Rekomendasi</b></td>
									<td style="width: 150px;" align="center"><b>Catatan</b></td>
								</tr>
								<tr>
									<td style="width: 20px;" align="center">1.</td>
									<td style="width: 200px;" align="left"> &nbsp;ROENAH</td>
									<td style="width: 150px;" align="right">5,000,000&nbsp; </td>
									<td style="width: 150px;" align="center"></td>
									<td style="width: 150px;" align="center"></td>
								</tr>
							</table>
							<table border="2" cellpadding="0" cellspacing="0" align="center" style="border-top: 0px;">
								<tr>
									<td style="border-right: 0px; width: 28px;" align="left">&nbsp;</td>
									<td style="border-left: 0px; width: 200px;" align="left"> &nbsp;JUMLAH</td>
									<td style="width: 150px;" align="right">33,000,000&nbsp; </td>
									<td style="width: 150px;" align="center"></td>
									<td style="width: 150px;" align="center"></td>
								</tr>
							</table>
							<table>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td><b>Kesimpulan :</b></td>
								</tr>
								<tr>
									<td style="font-size: 13px;">Berdasarkan hasil kunjungan lapangan dan wawancara, Tim Verifikasi Kecamatan Sindang menyimpulkan bahwa kelompok dengan identitas tersebut diatas dinyatakan :</td>
								</tr>
								<tr>
									<td style="font-size: 2px;">&nbsp;</td>
								</tr>
							</table>
							<table style="font-size: 13px; ">
								<tr>
									<td style="width: 30; border-style: none;">&nbsp;</td>
									<td style="width: 20; border-style: solid;">&nbsp;</td>
									<td style="width: 50;"> <b>&nbsp;&nbsp;&nbsp;LAYAK</b></td>
									<td style="width: 30; border-style: none;">&nbsp;</td>
									<td style="width: 20; border-style: solid;">&nbsp;</td>
									<td style="width: 100;"> <b>&nbsp;&nbsp;&nbsp;TIDAK LAYAK</b></td>
								</tr>
							</table>
							<table>
							<tr>
									<td style="font-size: 2px;">&nbsp;</td>
								</tr>
								<tr>
									<td style="font-size: 13px;">Untuk memperoleh pinjaman berdasarkan rekomendasi dengan uraian sebagai berikut:</td>
								</tr>
								<tr>
									<td style="font-size: 2px;">&nbsp;</td>
								</tr>
							</table>
							<table border="2" cellpadding="0" cellspacing="0">
								<tr>
									<td style="width: 150px;" align="center"><b>DESKRIPSI</b></td>
									<td style="width: 150px;" align="center"><b>TANGGAL</b></td>
									<td style="width: 200px;" align="center"><b>ALOKASI	</b></td>
									<td style="width: 100px;" align="center"><b>JASA</b></td>
									<td style="width: 100px;" align="center"><b>JANGKA</b></td>
									<td style="width: 150px;" align="center"><b>SISTEM</b></td>
								</tr>
								<tr>
									<td style="width: 150px;" align="left"><b>PROPOSAL</b></td>
									<td style="width: 150px;" align="center">19-11-2021</td>
									<td style="width: 200px;" align="right">33,000,000&nbsp; </td>
									<td style="width: 100px;" align="center">20</td>
									<td style="width: 100px;" align="center">12</td>
									<td style="width: 150px;" align="left"> &nbsp;Bulanan</td>
								</tr>
								<tr>
									<td style="width: 150px;" align="left"><b>REKOMENDASI</b></td>
									<td style="width: 150px;" align="center"></td>
									<td style="width: 200px;" align="right"></td>
									<td style="width: 100px;" align="center"></td>
									<td style="width: 100px;" align="center"></td>
									<td style="width: 150px;" align="left"></td>
								</tr>
							</table>
							<table>
							<tr>
									<td style="font-size: 2px;">&nbsp;</td>
								</tr>
								<tr>
									<td style="font-size: 13px;">Atas hasil rekomendasi tersebut di atas, selanjutnya dibahas pada Rapat Tim Pendanaan atau Musyawarah Pendanaan Perguliran di Kecamatan Sindang sesuai dengan ketentuan dan aturan yang berlaku.</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
							</table>
							<table border="0">
								<tr>
									<td style="width: 20;" align="center">&nbsp;</td>
									<td style="width: 200;" align="center">&nbsp;</td>
									<td style="width: 280;" align="center">Tim Verifikasi Kecamatan Sindang</td>
									<td style="width: 200;" align="center">&nbsp;</td>
								</tr>
								<tr>
									<td style="width: 50;" align="center">&nbsp;</td>
									<td style="width: 200;" align="center">Ketua</td>
									<td style="width: 250;" align="center">Anggota</td>
									<td style="width: 200;" align="center">Anggota</td>
								</tr>
								<tr>
									<td style="width: 50;" align="center">&nbsp;</td>
									<td style="width: 200;" align="center">&nbsp;</td>
									<td style="width: 250;" align="center">&nbsp;</td>
									<td style="width: 200;" align="center">&nbsp;</td>
								</tr>
								<tr>
									<td style="width: 50;" align="center">&nbsp;</td>
									<td style="width: 200;" align="center">&nbsp;</td>
									<td style="width: 250;" align="center">&nbsp;</td>
									<td style="width: 200;" align="center">&nbsp;</td>
								</tr>
								<tr>
									<td style="width: 50;" align="center">&nbsp;</td>
									<td style="width: 200;" align="center">&nbsp;</td>
									<td style="width: 250;" align="center">&nbsp;</td>
									<td style="width: 200;" align="center">&nbsp;</td>
								</tr>
								<tr>
									<td style="width: 50;" align="center">&nbsp;</td>
									<td style="width: 200;" align="center"><b><u>NAMA KETUA</u></b></td>
									<td style="width: 250;" align="center"><b><u>NAMA ANGGOTA</u></b></td>
									<td style="width: 200;" align="center"><b><u>NAMA ANGGOTA</u></b></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>