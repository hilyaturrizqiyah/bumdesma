<html>
<head>
<title>Cetak Surat keterangan Usaha</title>
</head>
<body>
<table style="font-family: arial;">
	<tr>
		<td>
			<table border="0" style="">
				<tr>
					<td width="700" style="border-top: 70px solid green;"></td>
				</tr>
				<tr>
					<td width="10" style="line-height: 5px;">&nbsp;</td>
				</tr>
				<tr>
					<td width="700" style="border-top: 15px solid orange;"></td>
				</tr>
			</table>
<br>	
			<table border="0" style="">
				<tr>
					<td width="700" align="center" style="font-weight: bold; font-size: 35px; ">PROPOSAL</td>
				</tr>
				<tr>
					<td align="center" style="font-size: 25px;">KELOMPOK SIMPAN PINJAM KHUSUS PEREMPUAN</td>
				</tr>
				<tr>
					<td align="center" style="font-weight: bold; font-size: 35px; ">ANGGREK</td>
				</tr>
				<tr>
					<td align="center" style="font-weight: bold; font-size: 35px; ">&nbsp;</td>
				</tr>
				<tr>
					<td align="center" style="font-weight: bold; font-size: 35px; ">&nbsp;</td>
				</tr>
				<tr>
					<td align="center" style="font-weight: bold; font-size: 35px; ">&nbsp;</td>
				</tr>
				<tr>
					<td align="center" style="font-weight: bold; font-size: 35px; ">&nbsp;</td>
				</tr>
			</table>
			<table style="font-size: 13px;">
				<tr>
					<td width="50"></td>
					<td width="300" style="border-style: solid;">
						<table border="0">
							<tr>
								<td width="140">No. Registrasi</td>
								<td width="10">:</td>
								<td style="border-bottom: 2px solid black; font-weight: bold; width: 200;" >20181203105007</td>
							</tr>
							<tr>
								<td width="140">Tgl. Registrasi</td>
								<td width="10">:</td>
								<td style="border-bottom: 2px solid black; font-weight: bold; width: 200;" >19-11-2018</td>
							</tr>
							<tr>
								<td width="140">Kode Pinjaman</td>
								<td width="10">:</td>
								<td style="border-bottom: 2px solid black; font-weight: bold; width: 200;" ></td>
							</tr>
							<tr>
								<td width="140">No. SPK</td>
								<td width="10">:</td>
								<td style="border-bottom: 2px solid black; font-weight: bold; width: 200;" ></td>
							</tr>
						</table>
					</td>
					<td width="270" style="border-style: solid;">
						<table style="font-size: 13;">
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td>VALIDASI PROPOSAL</td>
							</tr>
						</table>
						<table style="font-size: 12;">
							<tr>
								<td style="width: 10; border-style: solid;"></td>
								<td>&nbsp; MS/TMS</td>
							</tr>
							<tr>
								<td style="width: 10; border-style: solid;"></td>
								<td>&nbsp; Rekomendasi Layak/Tidak Layak</td>
							</tr>
							<tr>
								<td style="width: 10;"></td>
								<td>&nbsp; Tgl. <b>______________________________</b></td>
							</tr>
							<tr>
								<td style="width: 10; border-style: solid;"></td>
								<td>&nbsp; Penetapan/Pembatalan</td>
							</tr>
							<tr>
								<td style="width: 10;"></td>
								<td>&nbsp; Tgl. <b>______________________________</b></td>
							</tr>
							<tr>
								<td style="width: 10;"></td>
								<td>&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<table>
				<tr>
					<td style="font-size: 35;">&nbsp;</td>
				</tr>
				<tr>
					<td style="font-size: 35;">&nbsp;</td>
				</tr>
				<tr>
					<td style="font-size: 35;">&nbsp;</td>
				</tr>
				<tr>
					<td style="font-size: 25;" align="center" width="700"><b>DESA GARAWASTU</b></td>
				</tr>
				<tr>
					<td style="font-size: 25;" align="center" width="700"><b>KECAMATAN SINDANG</b></td>
				</tr>
				<tr>
					<td style="font-size: 25;" align="center" width="700"><b>KABUPATEN MAJALENGKA</b></td>
				</tr>
				<tr>
					<td style="font-size: 25;" align="center" width="700"><b>PROVINSI JAWA BARAT</b></td>
				</tr>
				<tr>
					<td style="font-size: 25;" align="center" width="700"><b>TAHUN 2018</b></td>
				</tr>
			</table>
			<br>
			<table border="0" style="">
				<tr>
					<td width="700" style="border-top: 15px solid orange;"></td>
				</tr>
				<tr>
					<td width="10" style="line-height: 5px;">&nbsp;</td>
				</tr>
				<tr>
					<td width="700" style="border-top: 70px solid green;"></td>
				</tr>				
			</table>
		</td>
	</tr>
</table>
</body>
</html>