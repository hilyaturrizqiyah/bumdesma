@extends('layouts.master')

@section('title') {{ $title }} @endsection

@section('css')
<!-- DataTables -->
<link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

@component('components.breadcrumb')
@slot('li_1') Keangotaan @endslot
@slot('title') Daftar Proposal Kelompok @endslot
@endcomponent


<div class="col-xl-12">
    <div class="card">
        <div class="card-body">
            @if ($message = Session::get('success'))
            <div class="alert border-0 border-start border-5 border-primary alert-dismissible fade show">
                <div>{{ $message }}</div>
            </div>
            @endif

            <p>
                <a class="btn btn-primary btn-sm" href="{{ route('proposal-kelompok.create') }}">Registrasi Proposal</a>
            </p>

            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-bs-toggle="tab" href="#home" role="tab">
                        <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                        <span class="d-none d-sm-block">Registrasi</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-bs-toggle="tab" href="#profile" role="tab">
                        <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                        <span class="d-none d-sm-block">Verifikasi</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-bs-toggle="tab" href="#messages" role="tab">
                        <span class="d-block d-sm-none"><i class="far fa-envelope"></i></span>
                        <span class="d-none d-sm-block">Penetapan</span>
                    </a>
                </li>

            </ul>

            <!-- Tab panes -->
            <div class="tab-content p-3 text-muted">
                <div class="tab-pane active" id="home" role="tabpanel">
                    <p class="mb-0">
                        <table id="registrasi" class="table table-bordered dt-responsive  nowrap w-100">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Kode Registrasi</th>
                                    <th>Kelompok</th>
                                    <th>Desa</th>
                                    <th>Proposal</th>
                                    <th>Orang</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>

                            @php
                            $i = 1;
                            @endphp
                            <tbody>
                                {{-- @foreach ($data as $data ) --}}
                                <tr>
                                    <td>1</td>
                                    <td>2022-03-19</td>
                                    <td>3243454545</td>
                                    <td>Bunga Mawar</td>
                                    <td>Citarum</td>
                                    <td>50.000.000</td>
                                    <td>10</td>
                                    <td>
                                        <a class="btn btn-outline-primary btn-sm" href="{{ route('proposal-kelompok.verification-member', 2) }}">Verifikasi</a>
                                        <a class="btn btn-outline-info btn-sm" href="{{ route('proposal-kelompok.detail', 2) }}">Detail</a>
                                        <button type="submit" class="btn btn-outline-danger btn-sm">Hapus</button>
                                    </td>
                                </tr>
                                {{-- @endforeach --}}
                            </tbody>
                        </table>
                    </p>
                </div>

                <div class="tab-pane" id="profile" role="tabpanel">
                    <p class="mb-0">
                        <table id="verifikasi" class="table table-bordered dt-responsive  nowrap w-100">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>Kode Registrasi</th>
                                <th>Kelompok</th>
                                <th>Desa</th>
                                <th>Proposal</th>
                                <th>Orang</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>2022-03-19</td>
                                <td>3243454545</td>
                                <td>Bunga Mawar</td>
                                <td>Citarum</td>
                                <td>50.000.000</td>
                                <td>10</td>
                                <td>
                                    <a class="btn btn-outline-success btn-sm" href="{{ route('proposal-kelompok.penetapan-show', 2) }}">Penetapan</a>
                                    <a class="btn btn-outline-info btn-sm" href="{{ route('proposal-kelompok.detail', 2) }}">Detail</a>
                                    <button type="submit" class="btn btn-outline-danger btn-sm">Hapus</button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </p>
                </div>
                <div class="tab-pane" id="messages" role="tabpanel">
                    <table id="penetapan" class="table table-bordered dt-responsive  nowrap w-100">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal</th>
                            <th>Kode Registrasi</th>
                            <th>Kelompok</th>
                            <th>Desa</th>
                            <th>Alokasi</th>
                            <th>Orang</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>2022-03-19</td>
                            <td>3243454545</td>
                            <td>Bunga Mawar</td>
                            <td>Citarum</td>
                            <td>50.000.000</td>
                            <td>10</td>
                            <td>
                                <a class="btn btn-outline-info btn-sm" href="{{ route('proposal-kelompok.detail', 2) }}">Detail</a>
                                <button type="submit" class="btn btn-outline-danger btn-sm">Hapus</button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>

@endsection
@section('script')
<!-- Required datatable js -->
<script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>
<script src="{{ URL::asset('/assets/libs/jszip/jszip.min.js') }}"></script>
<script src="{{ URL::asset('/assets/libs/pdfmake/pdfmake.min.js') }}"></script>
<!-- Datatable init js -->
<script src="{{ URL::asset('/assets/js/pages/datatables.init.js') }}"></script>
<script>
    function initDatatable(id) {
        $(`#${id}`).DataTable();
    }

    initDatatable('registrasi')
    initDatatable('verifikasi')
    initDatatable('penetapan')

</script>
@endsection
