@extends('layouts.master')

@section('title') {{ $title }} @endsection

@section('css')
    <!-- DataTables -->
    <link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Penetapan @endslot
        @slot('title') Konfirmasi dan Validasi @endslot
    @endcomponent

    <div class="row">
        <div class="col-12">
            <div class="card">
                <form method="post" action="{{route('proposal-kelompok.verification-process', 2)}}">
                    @csrf
                    <div class="row">
                        <div class="col-xl-12" {{-- style="border-right: 1px solid #dddfe1 !important;" --}}>
                            <div class="p-4 text-center text-xl-start pb-0">
                                <div class="row mb-0">
                                    <div class="col-12">
                                        <div>
                                            <p class="font-size-18 mb-0 text-truncate">#321239399234 | Teratai</p>
                                            <p class="font-size-14 text-muted">Alamat : Desa Gunung kidung</p>
                                        </div>
                                    </div>
                                </div>
                                <hr class="mt-0">

                            </div>
                        </div>

                    </div>

                    <div class="card-body pt-0">
                        @if ($message = Session::get('success'))
                            <div class="alert border-0 border-start border-5 border-primary alert-dismissible fade show">
                                <div>{{ $message }}</div>
                            </div>
                        @endif

                        <div>
                            <div class="row">
                                <div class="col-4">
                                    <h4 class="pb-2">Proposal</h4>
                                    <div class="row mb-4">
                                        <label class="col-form-label col-lg-3">Tanggal</label>
                                        <div class="col-lg-9">
                                            <div class="input-group">
                                                <input type="text" name="tanggal" value="{{date('d M, Y')}}" class="form-control" disabled>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-4">
                                        <label class="col-form-label col-lg-3">Kode Registrasi</label>
                                        <div class="col-lg-9">
                                            <input id="kode_registrasi" name="kode_registrasi" value="REG-01928" type="text" class="form-control" placeholder="Kode Registrasi" disabled>
                                        </div>
                                    </div>

                                    <div class="row mb-4">
                                        <label class="col-form-label col-lg-3">Alokasi Pengajuan</label>
                                        <div class="col-lg-9">
                                            <input id="jumlah_pengajuan" name="jumlah_pengajuan" value="10000000" type="text" class="form-control" placeholder="Jumlah Pengajuan" disabled>
                                        </div>
                                    </div>

                                    <div class="row mb-4">
                                        <label class="col-form-label col-lg-3">Pemanfaat</label>
                                        <div class="col-lg-9">
                                            <div class="input-group" id="datepicker2">
                                                <input type="text" name="jumlah_pemanfaat" value="1" class="form-control" placeholder="Jumlah Pemanfaat" disabled>
                                                <span class="input-group-text">orang</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mb-4">
                                        <label class="col-form-label col-lg-3">Jasa</label>
                                        <div class="col-lg-9">
                                            <div class="input-group" id="datepicker2">
                                                <input type="text" class="form-control" placeholder="Jasa" disabled>
                                                <span class="input-group-text">% per Tahun</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mb-4">
                                        <label class="col-form-label col-lg-3">Jangka</label>
                                        <div class="col-lg-9">
                                            <div class="input-group" id="datepicker2">
                                                <input type="text" class="form-control" placeholder="Jangka" disabled>
                                                <span class="input-group-text">Bulan</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mb-4">
                                        <label class="col-form-label col-lg-3">Angsuran</label>
                                        <div class="col-lg-9">
                                            <select name="sitem_angsuran" id="sitem_angsuran" class="form-select" disabled>
                                                <option selected value="1">----</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-4">
                                    <h4 class="pb-2">Verifikasi</h4>

                                    <div class="row mb-4">
                                        <label class="col-form-label col-lg-3">Tanggal Proposal</label>
                                        <div class="col-lg-9">
                                            <div class="input-group" id="datepicker2">
                                                <input type="text" name="tanggal" value="{{date('d M, Y')}}" class="form-control" placeholder="dd M, yyyy"
                                                       data-date-format="dd M, yyyy" data-date-container='#datepicker2' data-provide="datepicker"
                                                       data-date-autoclose="true" disabled>

                                                <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mb-4">
                                        <label class="col-form-label col-lg-3">Alokasi Pinjaman</label>
                                        <div class="col-lg-9">
                                            <input id="jumlah_pinjaman" name="jumlah_pinjaman" value="10000000" type="text" class="form-control" placeholder="Jumlah Pinjaman" disabled>
                                        </div>
                                    </div>

                                    <div class="row mb-4">
                                        <label class="col-form-label col-lg-3">Pemanfaat</label>
                                        <div class="col-lg-9">
                                            <div class="input-group" id="datepicker2">
                                                <input type="text" name="jumlah_pemanfaat" value="1" class="form-control" placeholder="Jumlah Pemanfaat" disabled>
                                                <span class="input-group-text">orang</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-4">
                                        <label class="col-form-label col-lg-3">Jasa</label>
                                        <div class="col-lg-9">
                                            <div class="input-group" id="datepicker2">
                                                <input type="text" name="jumlah_pemanfaat" class="form-control" placeholder="Jasa" disabled>
                                                <span class="input-group-text">% per Tahun</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mb-4">
                                        <label class="col-form-label col-lg-3">Jangka</label>
                                        <div class="col-lg-9">
                                            <div class="input-group" id="datepicker2">
                                                <input type="text" name="jumlah_pemanfaat" class="form-control" placeholder="Jangka" disabled>
                                                <span class="input-group-text">Bulan</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mb-4">
                                        <label class="col-form-label col-lg-3">Sistem Angsuran</label>
                                        <div class="col-lg-9">
                                            <select name="sitem_angsuran" id="sitem_angsuran" class="form-select" disabled>
                                                <option selected disabled>-- Pilih Sistem Angsuran --</option>
                                                <option value="1">Bulanan</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row mb-4">
                                        <label class="col-form-label col-lg-3">Keterangan</label>
                                        <div class="col-lg-9">
                                            <select name="sitem_angsuran" id="sitem_angsuran" class="form-select" disabled>
                                                <option selected disabled>-- Pilih Sistem Angsuran --</option>
                                                <option value="1">Layak</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-4">
                                    <h4 class="pb-2">Penetapan</h4>

                                    <div class="row mb-4">
                                        <label class="col-form-label col-lg-3">Tanggal Proposal</label>
                                        <div class="col-lg-9">
                                            <div class="input-group" id="datepicker2">
                                                <input type="text" name="tanggal" value="{{date('d M, Y')}}" class="form-control" placeholder="dd M, yyyy"
                                                       data-date-format="dd M, yyyy" data-date-container='#datepicker2' data-provide="datepicker"
                                                       data-date-autoclose="true">

                                                <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mb-4">
                                        <label class="col-form-label col-lg-3">Alokasi Pinjaman</label>
                                        <div class="col-lg-9">
                                            <input id="jumlah_pinjaman" name="jumlah_pinjaman" value="10000000" type="text" class="form-control" placeholder="Jumlah Pinjaman">
                                        </div>
                                    </div>

                                    <div class="row mb-4">
                                        <label class="col-form-label col-lg-3">Pemanfaat</label>
                                        <div class="col-lg-9">
                                            <div class="input-group" id="datepicker2">
                                                <input type="text" name="jumlah_pemanfaat" value="1" class="form-control" placeholder="Jumlah Pemanfaat" disabled>
                                                <span class="input-group-text">orang</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-4">
                                        <label class="col-form-label col-lg-3">Jasa</label>
                                        <div class="col-lg-9">
                                            <div class="input-group" id="datepicker2">
                                                <input type="text" name="jumlah_pemanfaat" class="form-control" placeholder="Jasa">
                                                <span class="input-group-text">% per Tahun</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mb-4">
                                        <label class="col-form-label col-lg-3">Jangka</label>
                                        <div class="col-lg-9">
                                            <div class="input-group" id="datepicker2">
                                                <input type="text" name="jumlah_pemanfaat" class="form-control" placeholder="Jangka">
                                                <span class="input-group-text">Bulan</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mb-4">
                                        <label class="col-form-label col-lg-3">Sistem Angsuran</label>
                                        <div class="col-lg-9">
                                            <select name="sitem_angsuran" id="sitem_angsuran" class="form-select">
                                                <option selected disabled>-- Pilih Sistem Angsuran --</option>
                                                <option value="1">Bulanan</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row mb-4">
                                        <label class="col-form-label col-lg-3">Keterangan</label>
                                        <div class="col-lg-9">
                                            <select name="sitem_angsuran" id="sitem_angsuran" class="form-select">
                                                <option selected disabled>-- Pilih Sistem Angsuran --</option>
                                                <option value="1">Layak</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-lg-12">
                                    <label class="col-form-label col-lg-3">Catatan Khusus untuk tim Verifikasi</label>
                                    <textarea class="form-control" id="billing-address" rows="3" placeholder="Enter full address"></textarea>
                                </div>

                            </div>

                        </div>
                    </div>

                    <div class="card-footer bg-transparent border-top text-end">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" onclick="history.back()"><i class="fa fa-arrow-left"></i> Kembali</button>
                        <button type="submit" class="btn btn-success"><i class="bx bxs-check-shield align-middle me-1"></i> Verifikasi</button>
                    </div>

                </form>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->



@endsection
@section('script')
    <!-- Required datatable js -->
    <script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/jszip/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('assets/libs/inputmask/inputmask.min.js')}}"></script>
    <script src="{{asset('assets/js/pages/register.init.js')}}"></script>
    <script>
        $(document).ready(function () {
            let pengajuan = $('#jumlah_pengajuan')
            pengajuan.val(formatRupiah(pengajuan.val(), 'Rp. '))

            let pinjaman = $('#jumlah_pinjaman')
            pinjaman.val(formatRupiah(pinjaman.val(), 'Rp. '))
        })
    </script>
    <!-- Datatable init js -->
    <script src="{{ URL::asset('/assets/js/pages/datatables.init.js') }}"></script>
@endsection
