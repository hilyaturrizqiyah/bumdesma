@extends('layouts.master')

@section('title') {{ $title }} @endsection

@section('css')
    <!-- DataTables -->
    <link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/libs/sweetalert2/sweetalert2.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Proposal @endslot
        @slot('title') Daftar Kelompok Pengajuan Proposal @endslot
        @slot('title_2') Daftar Anggota Kelompok @endslot
        @slot('title_3') Registrasi Kelompok @endslot
        @slot('title_4') Cetak Dokumen @endslot
    @endcomponent

    <div class="row">
        <div class="col-12">

            @if ($message = Session::get('success'))
                <div class="alert border-0 border-start border-5 border-primary alert-dismissible fade show">
                    <div>{{ $message }}</div>
                </div>
            @endif
            <div class="row mb-4">
                <div class="col-xl-12">
                    <button class="btn btn-secondary" onclick="history.back()"> <i class="fa fa-arrow-left"></i> Kembali</button>
                    <a href="{{route('proposal-kelompok.index')}}" class="btn btn-danger"> <i class="fa fa-times"></i> Keluar</a>
                </div>
            </div>

            <div class="row">

                <div class="col-xl-4">
                    <div class="card">
                        <a href="javascript: void(0);" class="text-dark">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div class="flex-shrink-0 me-4" style="align-self: center;">
                                        <div class="avatar-md">
                                            <span class="avatar-title rounded-circle bg-light text-danger font-size-16">
                                                <i class="fa fa-file-pdf"></i>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="flex-grow-1 overflow-hidden" style="align-self: center;">
                                        <h5 class="text-truncate font-size-15">
                                            Lembar verifikasi
                                        </h5>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-xl-4">
                    <div class="card">
                        <a href="javascript: void(0);" class="text-dark">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div class="flex-shrink-0 me-4" style="align-self: center;">
                                        <div class="avatar-md">
                                            <span class="avatar-title rounded-circle bg-light text-danger font-size-16">
                                                <i class="fa fa-file-pdf"></i>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="flex-grow-1 overflow-hidden" style="align-self: center;">
                                        <h5 class="text-truncate font-size-15">
                                            Sampul Berkas
                                        </h5>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-xl-4">
                    <div class="card">
                        <a href="javascript: void(0);" class="text-dark">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div class="flex-shrink-0 me-4" style="align-self: center;">
                                        <div class="avatar-md">
                                            <span class="avatar-title rounded-circle bg-light text-danger font-size-16">
                                                <i class="fa fa-file-pdf"></i>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="flex-grow-1 overflow-hidden" style="align-self: center;">
                                        <h5 class="text-truncate font-size-15">
                                            Surat Rekomendasi
                                        </h5>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xl-4">
                    <div class="card">
                        <a href="javascript: void(0);" class="text-dark">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div class="flex-shrink-0 me-4" style="align-self: center;">
                                        <div class="avatar-md">
                                            <span class="avatar-title rounded-circle bg-light text-danger font-size-16">
                                                <i class="fa fa-file-pdf"></i>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="flex-grow-1 overflow-hidden" style="align-self: center;">
                                        <h5 class="text-truncate font-size-15">
                                            Surat Permohonan Kredit
                                        </h5>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-xl-4">
                    <div class="card">
                        <a href="javascript: void(0);" class="text-dark">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div class="flex-shrink-0 me-4" style="align-self: center;">
                                        <div class="avatar-md">
                                            <span class="avatar-title rounded-circle bg-light text-danger font-size-16">
                                                <i class="fa fa-file-pdf"></i>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="flex-grow-1 overflow-hidden" style="align-self: center;">
                                        <h5 class="text-truncate font-size-15">
                                            Daftar Anggota Kelompok
                                        </h5>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-xl-4">
                    <div class="card">
                        <a href="javascript: void(0);" class="text-dark">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div class="flex-shrink-0 me-4" style="align-self: center;">
                                        <div class="avatar-md">
                                            <span class="avatar-title rounded-circle bg-light text-danger font-size-16">
                                                <i class="fa fa-file-pdf"></i>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="flex-grow-1 overflow-hidden" style="align-self: center;">
                                        <h5 class="text-truncate font-size-15">
                                            Daftar Anggota Pemanfaat
                                        </h5>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xl-4">
                    <div class="card">
                        <a href="javascript: void(0);" class="text-dark">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div class="flex-shrink-0 me-4" style="align-self: center;">
                                        <div class="avatar-md">
                                        <span class="avatar-title rounded-circle bg-light text-danger font-size-16">
                                            <i class="fa fa-file-pdf"></i>
                                        </span>
                                        </div>
                                    </div>

                                    <div class="flex-grow-1 overflow-hidden" style="align-self: center;">
                                        <h5 class="text-truncate font-size-15">
                                            Rencana Angsuran Kelompok
                                        </h5>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-xl-4">
                    <div class="card">
                        <a href="javascript: void(0);" class="text-dark">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div class="flex-shrink-0 me-4" style="align-self: center;">
                                        <div class="avatar-md">
                                            <span class="avatar-title rounded-circle bg-light text-danger font-size-16">
                                                <i class="fa fa-file-pdf"></i>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="flex-grow-1 overflow-hidden" style="align-self: center;">
                                        <h5 class="text-truncate font-size-15">
                                            Pernyataan Tanggung Renteng
                                        </h5>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-xl-4">
                    <div class="card">
                        <a href="javascript: void(0);" class="text-dark">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div class="flex-shrink-0 me-4" style="align-self: center;">
                                        <div class="avatar-md">
                                            <span class="avatar-title rounded-circle bg-light text-danger font-size-16">
                                                <i class="fa fa-file-pdf"></i>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="flex-grow-1 overflow-hidden" style="align-self: center;">
                                        <h5 class="text-truncate font-size-15">
                                            Pernyataan Jaminan Anggota
                                        </h5>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-xl-4">
                    <div class="card">
                        <a href="javascript: void(0);" class="text-dark">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div class="flex-shrink-0 me-4" style="align-self: center;">
                                        <div class="avatar-md">
                                            <span class="avatar-title rounded-circle bg-light text-danger font-size-16">
                                                <i class="fa fa-file-pdf"></i>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="flex-grow-1 overflow-hidden" style="align-self: center;">
                                        <h5 class="text-truncate font-size-15">
                                            Pernataan penjamin
                                        </h5>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

            </div>

        </div> <!-- end col -->
    </div> <!-- end row -->




@endsection
@section('script')
    <!-- Required datatable js -->
    <script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/jszip/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{asset('assets/libs/sweetalert2/sweetalert2.min.js')}}"></script>

    <!-- Datatable init js -->
    <script src="{{ URL::asset('/assets/js/pages/datatables.init.js') }}"></script>

    <script>
        $(document).ready(function () {
            Swal.fire({
                title: "Selamat !",
                text: "Proses Registrasi Proposal baru berhasil. Silahkan cetak dokumen yang dibutuhkan!",
                icon: "success",
                confirmButtonColor: "#556ee6",
            })
        })
    </script>
@endsection
