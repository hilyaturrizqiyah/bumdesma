@extends('layouts.master')

@section('title') {{ $title }} @endsection

@section('css')
    <!-- DataTables -->
    <link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Proposal @endslot
        @slot('title') Daftar Kelompok Pengajuan Proposal @endslot
        @slot('title_2') Daftar Anggota Kelompok @endslot
        @slot('title_3') Registrasi Kelompok @endslot
    @endcomponent

    <div class="row">
        <div class="col-12">
            <div class="card">
                <form method="post" action="{{route('proposal-kelompok.register-process', 2)}}">
                    @csrf
                    <div class="row">
                        <div class="col-xl-12" {{-- style="border-right: 1px solid #dddfe1 !important;" --}}>
                            <div class="p-4 text-center text-xl-start">
                                <div class="row mb-0">
                                    <div class="col-12">
                                        <div>
                                            <p class="font-size-18 mb-0 text-truncate">#321239399234 | Teratai</p>
                                            <p class="font-size-14 text-muted">Alamat : Desa Gunung kidung</p>
                                        </div>
                                    </div>
                                </div>
                                <hr class="mt-0">

                            </div>
                        </div>

                    </div>

                    <div class="card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert border-0 border-start border-5 border-primary alert-dismissible fade show">
                                <div>{{ $message }}</div>
                            </div>
                        @endif

                        <div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="row mb-4">
                                        <label class="col-form-label col-lg-3">Tanggal</label>
                                        <div class="col-lg-9">
                                            <div class="input-group">
                                                <input type="text" name="tanggal" value="{{date('d M, Y')}}" class="form-control" readonly>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-4">
                                        <label class="col-form-label col-lg-3">Kode Registrasi</label>
                                        <div class="col-lg-9">
                                            <input id="kode_registrasi" name="kode_registrasi" value="REG-01928" type="text" class="form-control" placeholder="Kode Registrasi" readonly>
                                        </div>
                                    </div>

                                    <div class="row mb-4">
                                        <label class="col-form-label col-lg-3">Jumlah Pengajuan</label>
                                        <div class="col-lg-9">
                                            <input id="jumlah_pengajuan" name="jumlah_pengajuan" value="10000000" type="text" class="form-control" placeholder="Jumlah Pengajuan" readonly>
                                        </div>
                                    </div>

                                    <div class="row mb-4">
                                        <label class="col-form-label col-lg-3">Jumlah Pemanfaat</label>
                                        <div class="col-lg-9">
                                            <div class="input-group" id="datepicker2">
                                                <input type="text" name="jumlah_pemanfaat" value="1" class="form-control" placeholder="Jumlah Pemanfaat" readonly>
                                                <span class="input-group-text">orang</span>
                                            </div>
                                        </div>
                                    </div>

                                    <hr>

                                    <div class="row mb-4">
                                        <label class="col-form-label col-lg-3">Pokok</label>
                                        <div class="col-lg-9">
                                            <input id="pokok" name="pokok" value="800000" type="text" class="form-control" placeholder="Pokok" readonly>
                                        </div>
                                    </div>

                                    <div class="row mb-4">
                                        <label class="col-form-label col-lg-3">Bunga</label>
                                        <div class="col-lg-9">
                                            <input id="bunga" name="bunga" value="100000" type="text" class="form-control" placeholder="Bunga" readonly>
                                        </div>
                                    </div>

                                    <div class="row mb-4">
                                        <label class="col-form-label col-lg-3">Angsuran</label>
                                        <div class="col-lg-9">
                                            <input id="angsuran" name="angsuran" value="500000" type="text" class="form-control" placeholder="Angsuran" readonly>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-6">

                                    <div class="row mb-4">
                                        <label class="col-form-label col-lg-3">Tanggal Proposal</label>
                                        <div class="col-lg-9">
                                            <div class="input-group" id="datepicker2">
                                                <input type="text" name="tanggal" value="{{date('d M, Y')}}" class="form-control" placeholder="dd M, yyyy"
                                                       data-date-format="dd M, yyyy" data-date-container='#datepicker2' data-provide="datepicker"
                                                       data-date-autoclose="true">

                                                <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row mb-4">
                                        <label class="col-form-label col-lg-3">Nomor Proposal</label>
                                        <div class="col-lg-9">
                                            <input id="nomor_proposal" name="nomor_proposal" type="text" class="form-control" placeholder="Nomor Proposal">
                                        </div>
                                    </div>

                                    <div class="row mb-4">
                                        <label class="col-form-label col-lg-3">Nomor Rekom. Kades</label>
                                        <div class="col-lg-9">
                                            <input id="nomor_rekom_kades" name="nomor_rekom_kades" type="text" class="form-control" placeholder="Nomor Rekom. Kades">
                                        </div>
                                    </div>

                                    <div class="row mb-4">
                                        <label class="col-form-label col-lg-3">Jasa</label>
                                        <div class="col-lg-9">
                                            <div class="input-group" id="datepicker2">
                                                <input type="text" name="jumlah_pemanfaat" class="form-control" placeholder="Jasa">
                                                <span class="input-group-text">% per Tahun</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mb-4">
                                        <label class="col-form-label col-lg-3">Jangka</label>
                                        <div class="col-lg-9">
                                            <div class="input-group" id="datepicker2">
                                                <input type="text" name="jumlah_pemanfaat" class="form-control" placeholder="Jangka">
                                                <span class="input-group-text">Bulan</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mb-4">
                                        <label class="col-form-label col-lg-3">Sistem Angsuran</label>
                                        <div class="col-lg-9">
                                            <select name="sitem_angsuran" id="sitem_angsuran" class="form-select">
                                                <option selected disabled>-- Pilih Sistem Angsuran --</option>
                                                <option value="1">----</option>
                                            </select>
                                        </div>
                                    </div>

                                    <hr>

                                    <div class="row mb-4">
                                        <label class="col-form-label col-lg-3">Rencana Periode Perguliran</label>
                                        <div class="col-lg-9">
                                            <div class="row gy-2 gx-3 align-items-center">
                                                <div class="col-sm-6">
                                                    <select name="perguliran_bulan" id="perguliran_bulan" class="form-select">
                                                        <option selected disabled>-- Pilih Bulan --</option>
                                                        <option value="1">----</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-6">
                                                    <select name="perguliran_tahun" id="perguliran_tahun" class="form-select">
                                                        <option selected disabled>-- Pilih Tahun --</option>
                                                        <option value="1">----</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="card-footer bg-transparent border-top text-end">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" onclick="history.back()"><i class="fa fa-arrow-left"></i> Kembali</button>
                        <button type="submit" class="btn btn-primary">Lanjut</button>
                    </div>

                </form>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->



@endsection
@section('script')
    <!-- Required datatable js -->
    <script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/jszip/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('assets/libs/inputmask/inputmask.min.js')}}"></script>
    <script src="{{asset('assets/js/pages/register.init.js')}}"></script>
    <script>
        $(document).ready(function () {
            let pengajuan = $('#jumlah_pengajuan')
            pengajuan.val(formatRupiah(pengajuan.val(), 'Rp. '))

            let pokok = $('#pokok')
            pokok.val(formatRupiah(pokok.val(), 'Rp. '))

            let bunga = $('#bunga')
            bunga.val(formatRupiah(bunga.val(), 'Rp. '))

            let angsuran = $('#angsuran')
            angsuran.val(formatRupiah(angsuran.val(), 'Rp. '))
        })
    </script>
    <!-- Datatable init js -->
    <script src="{{ URL::asset('/assets/js/pages/datatables.init.js') }}"></script>
@endsection
