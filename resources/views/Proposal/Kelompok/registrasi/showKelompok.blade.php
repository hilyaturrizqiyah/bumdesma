@extends('layouts.master')

@section('title') {{ $title }} @endsection

@section('css')
    <!-- DataTables -->
    <link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Proposal @endslot
        @slot('title') Daftar Kelompok Pengajuan Proposal @endslot
        @slot('title_2') Daftar Anggota Kelompok @endslot
    @endcomponent

    <div class="row">
        <div class="col-lg-4">

            <div class="card">
                <div class="card-body">
                    <div class="d-flex">

                        <div class="flex-grow-1">
                            <div class="d-flex">
                                <div class="flex-grow-1">
                                    <div class="text-muted">
                                        <h5 class="mb-1">#321239399234 | Teratai</h5>
                                        <p class="mb-0">Alamat : Desa Gunung kidung</p>
                                    </div>
                                </div>

{{--                                <div class="flex-shrink-0 dropdown ms-2">--}}
{{--                                    <button class="btn btn-light btn-sm" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
{{--                                        <i class="bx bxs-cog align-middle me-1"></i> Setting--}}
{{--                                    </button>--}}
{{--                                    <div class="dropdown-menu dropdown-menu-end">--}}
{{--                                        <a class="dropdown-item" href="#">Action</a>--}}
{{--                                        <a class="dropdown-item" href="#">Another action</a>--}}
{{--                                        <a class="dropdown-item" href="#">Something else</a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                            </div>


                            <hr>

                            <div class="row">
                                <div class="col-4">
                                    <div>
                                        <p class="text-muted text-truncate mb-2">Jumlah Anggota</p>
                                        <h5 class="mb-0">32</h5>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div>
                                        <p class="text-muted text-truncate mb-2">Pemanfaat</p>
                                        <h5 class="mb-0">10k</h5>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div>
                                        <p class="text-muted text-truncate mb-2">Alokasi Pinjaman</p>
                                        <h5 class="mb-0">Rp. 10.000.000</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <div class="col-lg-8">
            <div class="card">

                <div class="p-4 border-bottom ">
                    <div class="row">
                        <div class="col-md-4 col-9">
                            <button class="btn btn-light btn-sm" type="button" onclick="history.back()"><i class="fa fa-arrow-left"></i> Kembali</button>
                        </div>
                        <div class="col-md-8 col-3">
                            <ul class="list-inline user-chat-nav text-end mb-0">
                                <li class="list-inline-item d-none d-sm-inline-block">
                                    <button class="btn btn-outline-primary btn-sm" type="button" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                                        <i class="fa fa-user-plus"></i> Tambah Anggota
                                    </button>
                                </li>
                                <li class="list-inline-item d-none d-sm-inline-block">
                                    <a class="btn btn-primary btn-sm" href="{{route('proposal-kelompok.registering', 2)}}">
                                        <i class="fa fa-arrow-right"></i> Lanjut
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    @if ($message = Session::get('success'))
                        <div class="alert border-0 border-start border-5 border-primary alert-dismissible fade show">
                            <div>{{ $message }}</div>
                        </div>
                    @endif

                    <table id="datatable" class="table table-bordered dt-responsive  nowrap w-100">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>NIK</th>
                            <th>Nama</th>
                            <th>Jabatan</th>
                            <th>Usaha</th>
                            <th>Proposal</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach ($kelompok->anggota as $anggota )
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $anggota->nik }}</td>
                                <td>{{ $anggota->nama_lengkap }}</td>
                                <td>{{ $anggota->jabatan }}</td>
                                <td>{{ $anggota->usaha->jenis_usaha }}</td>
                                <td>{{ $anggota->pengajuan_proposal }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

    <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
            <div class="modal-content">
                <form method="post" action="{{route('proposal-kelompok.add-member')}}">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Tambah Anggota</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-6">
                                <div class="row mb-4">
                                    <label for="projectname" class="col-form-label col-lg-3">Kode Anggota</label>
                                    <div class="col-lg-9">
                                        <input id="kode" name="kode" type="text" class="form-control" placeholder="Kode Anggota">
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <label for="projectdesc" class="col-form-label col-lg-3">Nomor Identitas</label>
                                    <div class="col-lg-9">
                                        <input id="nik" name="nik" type="text" class="form-control" placeholder="Nomor Identitas">
                                    </div>
                                </div>

                                <div class="row mb-4">
                                    <label class="col-form-label col-lg-3">Nama Lengkap</label>
                                    <div class="col-lg-9">
                                        <div class="row gy-2 gx-3 align-items-center">
                                            <div class="col-sm-10">
                                                <input id="nama_lengkap" name="nama_lengkap" type="text" class="form-control" placeholder="Nama Lengkap">
                                            </div>
                                            <div class="col-sm-2">
                                                <select name="jenis_kelamin" id="jenis_kelamin" class="form-select">
                                                    <option>L</option>
                                                    <option>P</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mb-4">
                                    <label for="projectbudget" class="col-form-label col-lg-3">Jabatan</label>
                                    <div class="col-lg-9">
                                        <select name="jabatan" id="jabatan" class="form-select">
                                            <option selected disabled>-- Pilih Jabatan --</option>
                                            <option value="1">......</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row mb-4">
                                    <label for="projectbudget" class="col-form-label col-lg-3">Tempat, Tgl lahir</label>
                                    <div class="col-lg-9">
                                        <div class="row gy-2 gx-3 align-items-center">
                                            <div class="col-sm-6">
                                                <input type="text" name="tempat_lahir" class="form-control" id="autoSizingInput" placeholder="Tempat Lahir">
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="input-group" id="datepicker2">
                                                    <input type="text" name="tanggal_lahir" class="form-control" placeholder="dd M, yyyy"
                                                           data-date-format="dd M, yyyy" data-date-container='#datepicker2' data-provide="datepicker"
                                                           data-date-autoclose="true">

                                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mb-4">
                                    <label for="projectbudget" class="col-form-label col-lg-3">No Hp</label>
                                    <div class="col-lg-9">
                                        <input id="telp" name="telp" type="text" class="form-control input-mask" data-inputmask="'mask': '+99-999999999999'" im-insert="true">
                                    </div>
                                </div>

                                <div class="row mb-4">
                                    <label for="projectbudget" class="col-form-label col-lg-3">Jumlah Tabungan</label>
                                    <div class="col-lg-9">
                                        <input id="jumlah_tabungan" name="jumlah_tabungan" type="text" class="form-control" placeholder="Jumlah Tabungan">
                                    </div>
                                </div>

                                <div class="row mb-4">
                                    <label class="col-form-label col-lg-3">Alamat Lengkap</label>
                                    <div class="col-lg-9">
                                        <textarea id="alamat" name="alamat" class="form-control" rows="3" placeholder="Alamat Lengkap"></textarea>
                                    </div>
                                </div>

                                <div class="row mb-4">
                                    <label class="col-form-label col-lg-3">Status</label>
                                    <div class="col-lg-9">
                                        <select name="status" id="status_" class="form-select">
                                            <option selected disabled>-- Pilih Status --</option>
                                            <option value="1">Aktif</option>
                                            <option value="0">Pasif</option>
                                        </select>
                                    </div>
                                </div>

                            </div>

                            <div class="col-6">

                                <div class="d-flex mb-4">
                                    <div class="flex-shrink-0 me-3">
                                        <img class="rounded avatar-lg" id="profile" src="{{asset('images/1620974914.jpeg')}}" width="200" height="200" />
                                    </div>
                                    <div class="flex-grow-1" style="align-self: center">
                                        <input class="form-control" type="file" id="formFile" onchange="document.getElementById('profile').src = window.URL.createObjectURL(this.files[0])">
                                    </div>
                                </div>

                                <div class="row mb-4">
                                    <label class="col-form-label col-lg-3">Jumlah Pengajuan</label>
                                    <div class="col-lg-9">
                                        <input id="pengajuan_proposal" name="pengajuan_proposal" type="text" class="form-control" placeholder="Jumlah Pengajuan">
                                    </div>
                                </div>

                                <div class="row mb-4">
                                    <label class="col-form-label col-lg-3">Jenis Usaha</label>
                                    <div class="col-lg-9">
                                        <select name="jenis_usaha" id="jenis_usaha" class="form-select">
                                            <option selected disabled>-- Pilih Jenis Usaha --</option>
                                            <option value="1">----</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row mb-4">
                                    <label class="col-form-label col-lg-3">Jaminan</label>
                                    <div class="col-lg-9">
                                        <input id="jaminan" name="jaminan" type="number" class="form-control" placeholder="Jaminan">
                                    </div>
                                </div>

                                <div class="row mb-4">
                                    <label class="col-form-label col-lg-3">Nama Penjamin</label>
                                    <div class="col-lg-9">
                                        <div class="row gy-2 gx-3 align-items-center">
                                            <div class="col-sm-10">
                                                <input id="nama_penjamin" name="nama_penjamin" type="text" class="form-control" placeholder="Nama Penjamin">
                                            </div>
                                            <div class="col-sm-2">
                                                <select name="jenis_kelamin_penjamin" id="jenis_kelamin_penjamin" class="form-select">
                                                    <option>L</option>
                                                    <option>P</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mb-4">
                                    <label class="col-form-label col-lg-3">Kekeluargaan</label>
                                    <div class="col-lg-9">
                                        <select name="status_keluarga" id="status_keluarga" class="form-select">
                                            <option selected disabled>-- Pilih Kekeluargaan --</option>
                                            <option value="1">----</option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



@endsection
@section('script')
    <!-- Required datatable js -->
    <script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/jszip/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('assets/libs/inputmask/inputmask.min.js')}}"></script>
    <script src="{{asset('assets/js/pages/register.init.js')}}"></script>
    <script>
        formatter('jumlah_tabungan')
        formatter('pengajuan_proposal')
        $(".input-mask").inputmask();
    </script>
    <!-- Datatable init js -->
    <script src="{{ URL::asset('/assets/js/pages/datatables.init.js') }}"></script>
@endsection
