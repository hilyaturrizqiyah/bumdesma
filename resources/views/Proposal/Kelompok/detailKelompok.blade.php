@extends('layouts.master')

@section('title') {{ $title }} @endsection

@section('css')
    <!-- DataTables -->
    <link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <style>
        .darker{
            box-shadow: 0 .75rem 1.5rem rgba(0, 0, 0, 0.05) !important;
        }
    </style>
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Proposal @endslot
        @slot('title') Detail @endslot
    @endcomponent


    @if ($message = Session::get('success'))
        <div class="alert border-0 border-start border-5 border-primary alert-dismissible fade show">
            <div>{{ $message }}</div>
        </div>
    @endif
    <div class="checkout-tabs">
        <div class="row">
            <div class="col-xl-2 col-sm-3">
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link active" id="v-pills-confir-tab" data-bs-toggle="pill" href="#v-pills-confir" role="tab" aria-controls="v-pills-confir" aria-selected="false">
                        <i class= "fa fa-info-circle d-block check-nav-icon mt-4 mb-2"></i>
                        <p class="fw-bold mb-4">Detail</p>
                    </a>
                    <a class="nav-link" id="v-pills-shipping-tab" data-bs-toggle="pill" href="#v-pills-shipping" role="tab" aria-controls="v-pills-shipping" aria-selected="true">
                        <i class= "fa fa-users check-nav-icon mt-4 mb-2"></i>
                        <p class="fw-bold mb-4">Pemanfaat</p>
                    </a>
                    <a class="nav-link" id="v-pills-reverif-tab" data-bs-toggle="pill" href="#v-pills-reverif" role="tab" aria-controls="v-pills-reverif" aria-selected="true">
                        <i class= "fa fa-recycle check-nav-icon mt-4 mb-2"></i>
                        <p class="fw-bold mb-4">Verifikasi Ulang</p>
                    </a>
                    <a class="nav-link" id="v-pills-docs-tab" data-bs-toggle="pill" href="#v-pills-docs" role="tab" aria-controls="v-pills-docs" aria-selected="true">
                        <i class= "fa fa-file-pdf check-nav-icon mt-4 mb-2"></i>
                        <p class="fw-bold mb-4">Berkas Proposal</p>
                    </a>
                </div>
            </div>
            <div class="col-xl-10 col-sm-9">
                <div class="card">
                    <div class="card-body">
                        <div class="tab-content" id="v-pills-tabContent">
                            <div class="tab-pane fade show active" id="v-pills-confir" role="tabpanel" aria-labelledby="v-pills-confir-tab">
                                <div class="card shadow-none border mb-0">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-6">
                                                <h4 class="pb-2">Proposal</h4>
                                                <div class="row mb-4">
                                                    <label class="col-form-label col-lg-3">Tanggal</label>
                                                    <div class="col-lg-9">
                                                        <div class="input-group">
                                                            <input type="text" name="tanggal" value="{{date('d M, Y')}}" class="form-control" disabled>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row mb-4">
                                                    <label class="col-form-label col-lg-3">Kode Registrasi</label>
                                                    <div class="col-lg-9">
                                                        <input id="kode_registrasi" name="kode_registrasi" value="REG-01928" type="text" class="form-control" placeholder="Kode Registrasi" disabled>
                                                    </div>
                                                </div>

                                                <div class="row mb-4">
                                                    <label class="col-form-label col-lg-3">Proposal</label>
                                                    <div class="col-lg-9">
                                                        <input id="jumlah_pengajuan" name="jumlah_pengajuan" value="10000000" type="text" class="form-control" placeholder="Jumlah Pengajuan" disabled>
                                                    </div>
                                                </div>

                                                <div class="row mb-4">
                                                    <label class="col-form-label col-lg-3">Jasa</label>
                                                    <div class="col-lg-9">
                                                        <div class="input-group" id="datepicker2">
                                                            <input type="text" class="form-control" placeholder="Jasa" disabled>
                                                            <span class="input-group-text">% per Tahun</span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row mb-4">
                                                    <label class="col-form-label col-lg-3">Jangka</label>
                                                    <div class="col-lg-9">
                                                        <div class="input-group" id="datepicker2">
                                                            <input type="text" class="form-control" placeholder="Jangka" disabled>
                                                            <span class="input-group-text">Bulan</span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row mb-4">
                                                    <label class="col-form-label col-lg-3">Periodisasi</label>
                                                    <div class="col-lg-9">
                                                        <select name="sitem_angsuran" id="sitem_angsuran" class="form-select" disabled>
                                                            <option selected value="1">----</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row mb-4">
                                                    <label class="col-form-label col-lg-3">Pemanfaat</label>
                                                    <div class="col-lg-9">
                                                        <div class="input-group" id="datepicker2">
                                                            <input type="text" name="jumlah_pemanfaat" value="1" class="form-control" placeholder="Jumlah Pemanfaat" disabled>
                                                            <span class="input-group-text">orang</span>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-6">
                                                <h4 class="pb-2">Verifikasi</h4>

                                                <div class="row mb-4">
                                                    <label class="col-form-label col-lg-3">Tanggal</label>
                                                    <div class="col-lg-9">
                                                        <div class="input-group" id="datepicker2">
                                                            <input type="text" name="tanggal" value="{{date('d M, Y')}}" class="form-control" placeholder="dd M, yyyy"
                                                                   data-date-format="dd M, yyyy" data-date-container='#datepicker2' data-provide="datepicker"
                                                                   data-date-autoclose="true" disabled>

                                                            <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row mb-4">
                                                    <label class="col-form-label col-lg-3">Kode Registrasi</label>
                                                    <div class="col-lg-9">
                                                        <input id="kode_registrasi" name="kode_registrasi" value="REG-01928" type="text" class="form-control" placeholder="Kode Registrasi" disabled>
                                                    </div>
                                                </div>

                                                <div class="row mb-4">
                                                    <label class="col-form-label col-lg-3">Rekomendasi</label>
                                                    <div class="col-lg-9">
                                                        <input id="jumlah_pinjaman" name="jumlah_pinjaman" value="10000000" type="text" class="form-control" placeholder="Jumlah Pinjaman" disabled>
                                                    </div>
                                                </div>

                                                <div class="row mb-4">
                                                    <label class="col-form-label col-lg-3">Jasa</label>
                                                    <div class="col-lg-9">
                                                        <div class="input-group" id="datepicker2">
                                                            <input type="text" name="jumlah_pemanfaat" class="form-control" placeholder="Jasa" disabled>
                                                            <span class="input-group-text">% per Tahun</span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row mb-4">
                                                    <label class="col-form-label col-lg-3">Jangka Waktu</label>
                                                    <div class="col-lg-9">
                                                        <div class="input-group" id="datepicker2">
                                                            <input type="text" name="jumlah_pemanfaat" class="form-control" placeholder="Jangka" disabled>
                                                            <span class="input-group-text">Bulan</span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row mb-4">
                                                    <label class="col-form-label col-lg-3">Periodisasi</label>
                                                    <div class="col-lg-9">
                                                        <select name="sitem_angsuran" id="sitem_angsuran" class="form-select" disabled>
                                                            <option selected disabled>-- Pilih Sistem Angsuran --</option>
                                                            <option value="1">Bulanan</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row mb-4">
                                                    <label class="col-form-label col-lg-3">Pemanfaat</label>
                                                    <div class="col-lg-9">
                                                        <div class="input-group" id="datepicker2">
                                                            <input type="text" name="jumlah_pemanfaat" value="1" class="form-control" placeholder="Jumlah Pemanfaat" disabled>
                                                            <span class="input-group-text">orang</span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row mb-4">
                                                    <label class="col-form-label col-lg-3">Keterangan</label>
                                                    <div class="col-lg-9">
                                                        <select name="sitem_angsuran" id="sitem_angsuran" class="form-select" disabled>
                                                            <option selected disabled>-- Pilih Sistem Angsuran --</option>
                                                            <option value="1">Layak</option>
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-12">
                                                <div class="row mb-4">
                                                    <label class="col-form-label col-lg-2">Catatan Khusus</label>
                                                    <div class="col-lg-10">
                                                        <textarea class="form-control" id="billing-address" rows="3" placeholder="Enter full address" disabled></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="v-pills-shipping" role="tabpanel" aria-labelledby="v-pills-shipping-tab">
                                <div class="card shadow-none border mb-0">
                                    <div class="card-body">
                                        <table id="pemanfaat" class="table table-bordered dt-responsive  nowrap w-100">
                                            <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>NIK</th>
                                                <th>Nama Lengkap</th>
                                                <th>Jenis Kelamin</th>
                                                <th>Proposal</th>
                                                <th>Rekomendasi</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>3243454545</td>
                                                <td>Bunga Mawar</td>
                                                <td>P</td>
                                                <td>50.000.000</td>
                                                <td>50.000.000</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="v-pills-reverif" role="tabpanel" aria-labelledby="v-pills-reverif-tab">
                                <div class="border border-danger">
                                    <div class="card-header bg-transparent border-danger">
                                        <h5 class="my-0 text-danger"><i class="mdi mdi-block-helper me-3"></i>PERHATIAN</h5>
                                    </div>
                                    <div class="card-body">
                                        <h5 class="card-title">Verifikasi Ulang?</h5>
                                        <p class="card-text">
                                            Verifikasi ulang akan mengembalikan status proposal kembali ke proses awal.
                                            Untuk melanjutkan proses re-verifikasi, silahkan klik tombol di bawah ini.
                                        </p>
                                        <div class="text-center mt-4 pt-2">
                                            <a href="javascript: void(0);" class="btn btn-warning btn-lg">Verifikasi Ulang</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="v-pills-docs" role="tabpanel" aria-labelledby="v-pills-docs-tab">
                                <div class="row">

                                    <div class="col-xl-4">
                                        <div class="card darker">
                                            <a href="javascript: void(0);" class="text-dark">
                                                <div class="card-body">
                                                    <div class="d-flex">
                                                        <div class="flex-shrink-0 me-4" style="align-self: center;">
                                                            <div class="avatar-md">
                                            <span class="avatar-title rounded-circle bg-light text-danger font-size-16">
                                                <i class="fa fa-file-pdf"></i>
                                            </span>
                                                            </div>
                                                        </div>

                                                        <div class="flex-grow-1 overflow-hidden" style="align-self: center;">
                                                            <h5 class="text-truncate font-size-15">
                                                                Lembar verifikasi
                                                            </h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="col-xl-4">
                                        <div class="card darker">
                                            <a href="javascript: void(0);" class="text-dark">
                                                <div class="card-body">
                                                    <div class="d-flex">
                                                        <div class="flex-shrink-0 me-4" style="align-self: center;">
                                                            <div class="avatar-md">
                                            <span class="avatar-title rounded-circle bg-light text-danger font-size-16">
                                                <i class="fa fa-file-pdf"></i>
                                            </span>
                                                            </div>
                                                        </div>

                                                        <div class="flex-grow-1 overflow-hidden" style="align-self: center;">
                                                            <h5 class="text-truncate font-size-15">
                                                                Sampul Berkas
                                                            </h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="col-xl-4">
                                        <div class="card darker">
                                            <a href="javascript: void(0);" class="text-dark">
                                                <div class="card-body">
                                                    <div class="d-flex">
                                                        <div class="flex-shrink-0 me-4" style="align-self: center;">
                                                            <div class="avatar-md">
                                            <span class="avatar-title rounded-circle bg-light text-danger font-size-16">
                                                <i class="fa fa-file-pdf"></i>
                                            </span>
                                                            </div>
                                                        </div>

                                                        <div class="flex-grow-1 overflow-hidden" style="align-self: center;">
                                                            <h5 class="text-truncate font-size-15">
                                                                Surat Rekomendasi
                                                            </h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-xl-4">
                                        <div class="card darker">
                                            <a href="javascript: void(0);" class="text-dark">
                                                <div class="card-body">
                                                    <div class="d-flex">
                                                        <div class="flex-shrink-0 me-4" style="align-self: center;">
                                                            <div class="avatar-md">
                                            <span class="avatar-title rounded-circle bg-light text-danger font-size-16">
                                                <i class="fa fa-file-pdf"></i>
                                            </span>
                                                            </div>
                                                        </div>

                                                        <div class="flex-grow-1 overflow-hidden" style="align-self: center;">
                                                            <h5 class="text-truncate font-size-15">
                                                                Surat Permohonan Kredit
                                                            </h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="col-xl-4">
                                        <div class="card darker">
                                            <a href="javascript: void(0);" class="text-dark">
                                                <div class="card-body">
                                                    <div class="d-flex">
                                                        <div class="flex-shrink-0 me-4" style="align-self: center;">
                                                            <div class="avatar-md">
                                            <span class="avatar-title rounded-circle bg-light text-danger font-size-16">
                                                <i class="fa fa-file-pdf"></i>
                                            </span>
                                                            </div>
                                                        </div>

                                                        <div class="flex-grow-1 overflow-hidden" style="align-self: center;">
                                                            <h5 class="text-truncate font-size-15">
                                                                Daftar Anggota Kelompok
                                                            </h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="col-xl-4">
                                        <div class="card darker">
                                            <a href="javascript: void(0);" class="text-dark">
                                                <div class="card-body">
                                                    <div class="d-flex">
                                                        <div class="flex-shrink-0 me-4" style="align-self: center;">
                                                            <div class="avatar-md">
                                            <span class="avatar-title rounded-circle bg-light text-danger font-size-16">
                                                <i class="fa fa-file-pdf"></i>
                                            </span>
                                                            </div>
                                                        </div>

                                                        <div class="flex-grow-1 overflow-hidden" style="align-self: center;">
                                                            <h5 class="text-truncate font-size-15">
                                                                Daftar Anggota Pemanfaat
                                                            </h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-xl-4">
                                        <div class="card darker">
                                            <a href="javascript: void(0);" class="text-dark">
                                                <div class="card-body">
                                                    <div class="d-flex">
                                                        <div class="flex-shrink-0 me-4" style="align-self: center;">
                                                            <div class="avatar-md">
                                        <span class="avatar-title rounded-circle bg-light text-danger font-size-16">
                                            <i class="fa fa-file-pdf"></i>
                                        </span>
                                                            </div>
                                                        </div>

                                                        <div class="flex-grow-1 overflow-hidden" style="align-self: center;">
                                                            <h5 class="text-truncate font-size-15">
                                                                Rencana Angsuran Kelompok
                                                            </h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="col-xl-4">
                                        <div class="card darker">
                                            <a href="javascript: void(0);" class="text-dark">
                                                <div class="card-body">
                                                    <div class="d-flex">
                                                        <div class="flex-shrink-0 me-4" style="align-self: center;">
                                                            <div class="avatar-md">
                                            <span class="avatar-title rounded-circle bg-light text-danger font-size-16">
                                                <i class="fa fa-file-pdf"></i>
                                            </span>
                                                            </div>
                                                        </div>

                                                        <div class="flex-grow-1 overflow-hidden" style="align-self: center;">
                                                            <h5 class="text-truncate font-size-15">
                                                                Pernyataan Tanggung Renteng
                                                            </h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="col-xl-4">
                                        <div class="card darker">
                                            <a href="javascript: void(0);" class="text-dark">
                                                <div class="card-body">
                                                    <div class="d-flex">
                                                        <div class="flex-shrink-0 me-4" style="align-self: center;">
                                                            <div class="avatar-md">
                                            <span class="avatar-title rounded-circle bg-light text-danger font-size-16">
                                                <i class="fa fa-file-pdf"></i>
                                            </span>
                                                            </div>
                                                        </div>

                                                        <div class="flex-grow-1 overflow-hidden" style="align-self: center;">
                                                            <h5 class="text-truncate font-size-15">
                                                                Pernyataan Jaminan Anggota
                                                            </h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="col-xl-4">
                                        <div class="card darker">
                                            <a href="javascript: void(0);" class="text-dark">
                                                <div class="card-body">
                                                    <div class="d-flex">
                                                        <div class="flex-shrink-0 me-4" style="align-self: center;">
                                                            <div class="avatar-md">
                                            <span class="avatar-title rounded-circle bg-light text-danger font-size-16">
                                                <i class="fa fa-file-pdf"></i>
                                            </span>
                                                            </div>
                                                        </div>

                                                        <div class="flex-grow-1 overflow-hidden" style="align-self: center;">
                                                            <h5 class="text-truncate font-size-15">
                                                                Pernyataan Penjamin
                                                            </h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="row mt-4">
                                <div class="col-sm-6">
                                    <a href="{{route('proposal-kelompok.index')}}" class="btn text-muted d-none d-sm-inline-block btn-link">
                                        <i class="mdi mdi-arrow-left me-1"></i> Keluar </a>
                                </div> <!-- end col -->
                            </div> <!-- end row -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('script')
    <!-- Required datatable js -->
    <script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/jszip/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/pdfmake/pdfmake.min.js') }}"></script>
    <!-- Datatable init js -->
    <script src="{{ URL::asset('/assets/js/pages/datatables.init.js') }}"></script>
    <script src="{{asset('assets/js/pages/register.init.js')}}"></script>
    <script>
        $(document).ready(function () {
            let pengajuan = $('#jumlah_pengajuan')
            pengajuan.val(formatRupiah(pengajuan.val(), 'Rp. '))

            let pinjaman = $('#jumlah_pinjaman')
            pinjaman.val(formatRupiah(pinjaman.val(), 'Rp. '))

            function initDatatable(id) {
                $(`#${id}`).DataTable();
            }

            initDatatable('pemanfaat')
        })
    </script>
@endsection
