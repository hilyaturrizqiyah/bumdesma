@extends('layouts.master')

@section('title') {{ $title }} @endsection

@section('css')
    <!-- DataTables -->
    <link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Verifikasi @endslot
        @slot('title') Verifikasi Anggota @endslot
        @slot('title_2') Konfirmasi Hasil Verifikasi @endslot
        @slot('title_3') Rangkuman Verifikasi @endslot
    @endcomponent


    @if ($message = Session::get('success'))
        <div class="alert border-0 border-start border-5 border-primary alert-dismissible fade show">
            <div>{{ $message }}</div>
        </div>
    @endif
    <div class="checkout-tabs">
        <div class="row">
            <div class="col-xl-2 col-sm-3">
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link active" id="v-pills-confir-tab" data-bs-toggle="pill" href="#v-pills-confir" role="tab" aria-controls="v-pills-confir" aria-selected="false">
                        <i class= "bx bx-badge-check d-block check-nav-icon mt-4 mb-2"></i>
                        <p class="fw-bold mb-4">Data Verifikasi</p>
                    </a>
                    <a class="nav-link" id="v-pills-shipping-tab" data-bs-toggle="pill" href="#v-pills-shipping" role="tab" aria-controls="v-pills-shipping" aria-selected="true">
                        <i class= "fa fa-users check-nav-icon mt-4 mb-2"></i>
                        <p class="fw-bold mb-4">Pemanfaat</p>
                    </a>
                </div>
            </div>
            <div class="col-xl-10 col-sm-9">
                <div class="card">
                    <div class="card-body">
                        <div class="tab-content" id="v-pills-tabContent">
                            <div class="tab-pane fade show active" id="v-pills-confir" role="tabpanel" aria-labelledby="v-pills-confir-tab">
                                <div class="card shadow-none border mb-0">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-6">
                                                <h4 class="pb-2">Proposal</h4>
                                                <div class="row mb-4">
                                                    <label class="col-form-label col-lg-3">Tanggal</label>
                                                    <div class="col-lg-9">
                                                        <div class="input-group">
                                                            <input type="text" name="tanggal" value="{{date('d M, Y')}}" class="form-control" disabled>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row mb-4">
                                                    <label class="col-form-label col-lg-3">Kode Registrasi</label>
                                                    <div class="col-lg-9">
                                                        <input id="kode_registrasi" name="kode_registrasi" value="REG-01928" type="text" class="form-control" placeholder="Kode Registrasi" disabled>
                                                    </div>
                                                </div>

                                                <div class="row mb-4">
                                                    <label class="col-form-label col-lg-3">Proposal</label>
                                                    <div class="col-lg-9">
                                                        <input id="jumlah_pengajuan" name="jumlah_pengajuan" value="10000000" type="text" class="form-control" placeholder="Jumlah Pengajuan" disabled>
                                                    </div>
                                                </div>

                                                <div class="row mb-4">
                                                    <label class="col-form-label col-lg-3">Jasa</label>
                                                    <div class="col-lg-9">
                                                        <div class="input-group" id="datepicker2">
                                                            <input type="text" class="form-control" placeholder="Jasa" disabled>
                                                            <span class="input-group-text">% per Tahun</span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row mb-4">
                                                    <label class="col-form-label col-lg-3">Jangka</label>
                                                    <div class="col-lg-9">
                                                        <div class="input-group" id="datepicker2">
                                                            <input type="text" class="form-control" placeholder="Jangka" disabled>
                                                            <span class="input-group-text">Bulan</span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row mb-4">
                                                    <label class="col-form-label col-lg-3">Periodisasi</label>
                                                    <div class="col-lg-9">
                                                        <select name="sitem_angsuran" id="sitem_angsuran" class="form-select" disabled>
                                                            <option selected value="1">----</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row mb-4">
                                                    <label class="col-form-label col-lg-3">Pemanfaat</label>
                                                    <div class="col-lg-9">
                                                        <div class="input-group" id="datepicker2">
                                                            <input type="text" name="jumlah_pemanfaat" value="1" class="form-control" placeholder="Jumlah Pemanfaat" disabled>
                                                            <span class="input-group-text">orang</span>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-6">
                                                <h4 class="pb-2">Verifikasi</h4>

                                                <div class="row mb-4">
                                                    <label class="col-form-label col-lg-3">Tanggal</label>
                                                    <div class="col-lg-9">
                                                        <div class="input-group" id="datepicker2">
                                                            <input type="text" name="tanggal" value="{{date('d M, Y')}}" class="form-control" placeholder="dd M, yyyy"
                                                                   data-date-format="dd M, yyyy" data-date-container='#datepicker2' data-provide="datepicker"
                                                                   data-date-autoclose="true" disabled>

                                                            <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row mb-4">
                                                    <label class="col-form-label col-lg-3">Kode Registrasi</label>
                                                    <div class="col-lg-9">
                                                        <input id="kode_registrasi" name="kode_registrasi" value="REG-01928" type="text" class="form-control" placeholder="Kode Registrasi" disabled>
                                                    </div>
                                                </div>

                                                <div class="row mb-4">
                                                    <label class="col-form-label col-lg-3">Rekomendasi</label>
                                                    <div class="col-lg-9">
                                                        <input id="jumlah_pinjaman" name="jumlah_pinjaman" value="10000000" type="text" class="form-control" placeholder="Jumlah Pinjaman" disabled>
                                                    </div>
                                                </div>

                                                <div class="row mb-4">
                                                    <label class="col-form-label col-lg-3">Jasa</label>
                                                    <div class="col-lg-9">
                                                        <div class="input-group" id="datepicker2">
                                                            <input type="text" name="jumlah_pemanfaat" class="form-control" placeholder="Jasa" disabled>
                                                            <span class="input-group-text">% per Tahun</span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row mb-4">
                                                    <label class="col-form-label col-lg-3">Jangka Waktu</label>
                                                    <div class="col-lg-9">
                                                        <div class="input-group" id="datepicker2">
                                                            <input type="text" name="jumlah_pemanfaat" class="form-control" placeholder="Jangka" disabled>
                                                            <span class="input-group-text">Bulan</span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row mb-4">
                                                    <label class="col-form-label col-lg-3">Periodisasi</label>
                                                    <div class="col-lg-9">
                                                        <select name="sitem_angsuran" id="sitem_angsuran" class="form-select" disabled>
                                                            <option selected disabled>-- Pilih Sistem Angsuran --</option>
                                                            <option value="1">Bulanan</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row mb-4">
                                                    <label class="col-form-label col-lg-3">Pemanfaat</label>
                                                    <div class="col-lg-9">
                                                        <div class="input-group" id="datepicker2">
                                                            <input type="text" name="jumlah_pemanfaat" value="1" class="form-control" placeholder="Jumlah Pemanfaat" disabled>
                                                            <span class="input-group-text">orang</span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row mb-4">
                                                    <label class="col-form-label col-lg-3">Keterangan</label>
                                                    <div class="col-lg-9">
                                                        <select name="sitem_angsuran" id="sitem_angsuran" class="form-select" disabled>
                                                            <option selected disabled>-- Pilih Sistem Angsuran --</option>
                                                            <option value="1">Layak</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row mb-4">
                                                    <label class="col-form-label col-lg-3">Memo</label>
                                                    <div class="col-lg-9">
                                                        <textarea class="form-control" id="billing-address" rows="3" placeholder="Enter full address" disabled></textarea>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="v-pills-shipping" role="tabpanel" aria-labelledby="v-pills-shipping-tab">
                                <div class="card shadow-none border mb-0">
                                    <div class="card-body">
                                        <table id="pemanfaat" class="table table-bordered dt-responsive  nowrap w-100">
                                            <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>NIK</th>
                                                <th>Nama Lengkap</th>
                                                <th>Jenis Kelamin</th>
                                                <th>Proposal</th>
                                                <th>Rekomendasi</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>3243454545</td>
                                                <td>Bunga Mawar</td>
                                                <td>P</td>
                                                <td>50.000.000</td>
                                                <td>50.000.000</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-sm-6">
                        <a href="{{route('proposal-kelompok.index')}}" class="btn text-muted d-none d-sm-inline-block btn-link">
                            <i class="mdi mdi-arrow-left me-1"></i> Keluar </a>
                    </div> <!-- end col -->
                    <div class="col-sm-6">
                        <div class="text-end">
                            <a href="ecommerce-checkout.html" class="btn btn-success">
                                <i class="mdi mdi-file-pdf me-1"></i> Cetak Dokumen Verifikasi </a>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->
            </div>
        </div>
    </div>


@endsection
@section('script')
    <!-- Required datatable js -->
    <script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/jszip/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/pdfmake/pdfmake.min.js') }}"></script>
    <!-- Datatable init js -->
    <script src="{{ URL::asset('/assets/js/pages/datatables.init.js') }}"></script>
    <script>
        function initDatatable(id) {
            $(`#${id}`).DataTable();
        }

        initDatatable('pemanfaat')

    </script>
@endsection
