@extends('layouts.master')

@section('title'){{$title}} @endsection

@section('css')
<!-- select2 css -->
<link href="{{ url('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />

<!-- dropzone css -->
<link href="{{ url('assets/libs/dropzone/dropzone.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

@component('components.breadcrumb')
@slot('li_1') Master @endslot
@slot('title') Edit Jenis Kegiatan @endslot
@endcomponent


<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @foreach ($data as $data )

                <form action="{{ route('master-jenis-usaha.update',$data->id) }}" method="post" class="custom-validation">
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="jenisKegiatan">Nama Jenis Usaha</label>
                            <input type="text" class="form-control" name="jenisUsaha" id="jenisUsaha" value="{{ $data->jenis_usaha }}" required>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="keterangan">Keterangan</label>
                            <input id="keterangan" name="keterangan" type="text" class="form-control" value="{{ $data->keterangan }}">
                        </div>
                        <div class="d-flex flex-wrap gap-2">
                            <button type="submit" class="btn btn-primary waves-effect waves-light">Save Changes</button>
                            <button type="button" class="btn btn-secondary waves-effect waves-light">Cancel</button>
                        </div>
                    </div>
                </form>
                @endforeach
            </div>
        </div>
    </div>
</div>
<!-- end row -->

@endsection
@section('script')


<!-- select 2 plugin -->
<script src="{{ url('assets/libs/select2/select2.min.js') }}"></script>

<!-- dropzone plugin -->
<script src="{{ url('assets/libs/dropzone/dropzone.min.js') }}"></script>

<!-- init js -->
<script src="{{ url('assets/js/pages/ecommerce-select2.init.js') }}"></script>
<script src="{{ URL::asset('/assets/libs/parsleyjs/parsleyjs.min.js') }}"></script>
<script src="{{ URL::asset('/assets/js/pages/form-validation.init.js') }}"></script>
<script type="text/javascript">
    $('#desa').change(function () {
        var desa = $('#desa').val();
        if (desa == 'other') {
            $('#fieldLainnya').show();
            $('#desaLainnya').attr('required', true);
        } else {
            $('#fieldLainnya').hide();
            $('#desaLainnya').val('');
            $('#desaLainnya').attr('required', false);
        }
    })

</script>

@endsection
