<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <script>document.write(new Date().getFullYear())</script> © SMT ID
            </div>
            <div class="col-sm-6">
                <div class="text-sm-end d-none d-sm-block">
                    Develop by Sarana Mandiri Teknologi Indonesia
                </div>
            </div>
        </div>
    </div>
</footer>