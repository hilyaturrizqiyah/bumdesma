@extends('layouts.master')

@section('title') {{ $title }} @endsection

@section('css')
    <!-- DataTables -->
    <link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Daftar Pengurus @endslot
        @slot('title') Pengurus @endslot
    @endcomponent

    <form action="{{ route('userSubmit') }}"  method="post" class="custom-validation" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="row">
                            <h4>Create User</h4>
                            <div class="col-6">
                                <div class="mb-2">
                                    <label class="col-md-2 col-form-label" for="pengurus">Select Pengurus</label>
                                    <select name="pengurus" id="pengurus" class="w-75 form-select" required>
                                        <option disabled value="">Choose...</option>
                                        <option value="">Choose...</option>
                                        <option value="1">Ahmad</option>
                                        <option value="2">Daniel</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-sm-6">
                                <div class="mb-3">
                                    <label for="nik">NIK</label>
                                    <input type="text" name="nik" id="nik" class="w-75 form-control" required minlength="16" maxlength="16"
                                           placeholder="Please enter 16 chars." disabled/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label" for="email">E-Mail</label>
                                    <div>
                                        <input type="email" name="email" id="email" class="w-75 form-control" required parsley-type="email"
                                               placeholder="Enter a valid e-mail" disabled/>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label class="col-md-2 col-form-label" for="jabatan">Jabatan</label>
                                    <select name="jabatan" id="jabatan" class="w-75 form-select" disabled>
                                        <option selected disabled value="">Choose ...</option>
                                        <option value="ketua">Ketua</option>
                                        <option value="sektertaris">Seketratis</option>
                                        <option value="bendahara">Bendahara</option>
                                        <option value="staff">Staff</option>
                                        <option value="anggota">Anggota</option>
                                    </select>
                                </div>
                                <div class="row">
                                    <div class="col-4">
                                        <label for="pbod">Tempat Tanggal Lahir</label>
                                        <input id="pbod" name="pbod" type="text" class="form-control" placeholder="Tempat" disabled>
                                    </div>
                                    <div class="col-8">
                                        <label for="bod">Tanggal Lahir</label>
                                        <input id="bod" name="bod" type="date" style="width: 60%" class="form-control" disabled>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label class="col-md-3 col-form-label" for="gender">Gender</label>
                                    <select name="gender" id="gender" class="w-75 form-select" disabled>
                                        <option selected disabled value="">Choose ...</option>
                                        <option value="laki-laki">Laki Laki</option>
                                        <option value="perempuan">Perempuan</option>
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label class="col-md-3 col-form-label" for="address">Alamat</label>
                                    <textarea id="address" name="address" required class="w-75 form-control" rows="3" disabled></textarea>
                                </div>

                            </div>

                            <div class="col-sm-6">
                                <div class="mb-3">
                                    <label for="fullname">Nama Lengkap</label>
                                    <input type="text" id="fullname" name="fullname" class="w-75 form-control" required placeholder="Type something" disabled/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label" for="telephone">Telephone</label>
                                    <input type="tel" id="telephone" name="telephone" class="w-75 form-control" required parsley-type="email"
                                           placeholder="8123456789" disabled/>
                                </div>
                                <div class="mb-3">
                                    <label class="col-md-3 col-form-label" for="divisi">Kepengurusan</label>
                                    <select id="divisi" name="divisi" class="w-75 form-select" disabled>
                                        <option selected disabled value="">Choose ...</option>
                                        <option value="bkad">BKAD</option>
                                        <option value="upk">UPK</option>
                                        <option value="bpupk">BPUPK</option>
                                        <option value="tv">TV</option>
                                        <option value="tpd">TPD</option>
                                    </select>
                                </div>

                            </div>

                        </div>
                        <div class="row mt-5">
                            <h4 class="mb-3">Select Roles</h4>
                            <div class="">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th style="width: 1rem;">#</th>
                                        <th>Roles</th>
                                        <th>Deskripsi</th>
                                        <th>Active</th>
                                    </tr>
                                    </thead>
                                    <tbody
                                    <?php $i=1;?>
                                    @foreach($roles as $role)
                                    <tr>
                                        <th scope="row">{{$i++}}</th>
                                        <td>{{$role['name']}}</td>
                                        <td>{{$role['desc']}}</td>
                                        <td>
                                            <input type='checkbox' name='roles[]' value='{{$role['id']}}'><br>
                                        </td>
                                    </tr>
                                    @endforeach
                                </table>

                            </div>
                        </div>

                            <div class="d-flex flex-wrap gap-2">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                                <button type="button" onclick="window.history.back()" class="btn btn-secondary waves-effect waves-light">Cancel</button>
                            </div>
                    </div>
                </div>

            </div>
        </div>
    </form>


@endsection
@section('script')
    <!-- Required datatable js -->
    <script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/jszip/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/pdfmake/pdfmake.min.js') }}"></script>
    <!-- Datatable init js -->
    <script src="{{ URL::asset('/assets/js/pages/datatables.init.js') }}"></script>
@endsection
