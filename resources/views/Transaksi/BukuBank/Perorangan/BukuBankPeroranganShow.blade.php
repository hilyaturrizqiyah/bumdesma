@extends('layouts.master')

@section('title') {{ $title }} @endsection

@section('css')
    <!-- DataTables -->
    <link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Buku Bank @endslot
        @slot('title') Perorangan @endslot
    @endcomponent

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    @if ($message = Session::get('success'))
                    <div class="alert border-0 border-start border-5 border-primary alert-dismissible fade show">
                        <div>{{ $message }}</div>
                    </div>
	                @endif
                    {{-- <h4 class="card-title">Default Datatable</h4>
                    <p class="card-title-desc">DataTables has most features enabled by
                        default, so all you need to do to use it with your own tables is to call
                        the construction function: <code>$().DataTable();</code>.
                    </p> --}}
                    <p>
                        <a class="btn btn-primary btn-sm" href="{{ route('buku-bank-perorangan.create') }}">Tambah</a>
                    </p>
                    <h1 align="right">Rp. 43.131.821</h1>
                    <table id="datatable" class="table table-bordered dt-responsive  nowrap w-100">
                        <thead>
                            <tr>
                                <th>Tanggal</th>
                                <th>Keterangan</th>
                                <th>Bukti</th>
                                <th>Masuk</th>
                                <th>Keluar</th>
                                <th>Petugas</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>24-10-2018</td>
                                <td>Pajak Bank</td>
                                <td>1221810002</td>
                                <td>0</td>
                                <td>3.00.000</td>
                                <td>ADE SUTINI</td>
                                <td><a class="btn btn-outline-primary btn-sm" href="{{ route('buku-bank-ops.edit',1) }}">Edit</a>
                                <a class="btn btn-outline-danger btn-sm" href="#">Hapus</a>
                                </td>
                            </tr>
                            <tr>
                                <td>03-12-2018</td>
                                <td>Tarik Dari Rekening</td>
                                <td>1221812003</td>
                                <td>0</td>
                                <td>10.000.000</td>
                                <td>ADE SUTINI</td>
                                <td><a class="btn btn-outline-primary btn-sm" href="{{ route('buku-bank-perorangan.edit',2) }}">Edit</a>
                                <a class="btn btn-outline-danger btn-sm" href="#">Hapus</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->



@endsection
@section('script')
    <!-- Required datatable js -->
    <script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/jszip/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/pdfmake/pdfmake.min.js') }}"></script>
    <!-- Datatable init js -->
    <script src="{{ URL::asset('/assets/js/pages/datatables.init.js') }}"></script>
@endsection
