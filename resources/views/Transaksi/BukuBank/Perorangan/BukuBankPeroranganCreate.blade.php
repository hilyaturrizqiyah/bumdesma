@extends('layouts.master')

@section('title'){{$title}} @endsection

@section('css')
<!-- select2 css -->
<link href="{{ url('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />

<!-- dropzone css -->
<link href="{{ url('assets/libs/dropzone/dropzone.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

@component('components.breadcrumb')
@slot('li_1') Buku Bank @endslot
@slot('title') Tambah Perorangan @endslot
@endcomponent
<form action="{{ route('pengaturan-desa.store') }}"  method="post" class="custom-validation">
    @csrf
<div class="row">
    <div class="col-12">

        <div class="card">
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="row">
                    <div class="col-sm-6">
                        <div class="mb-3">
                            <label for="tanggal">Tanggal</label>
                            <input id="tanggal" name="tanggal" type="date" class="form-control" required>
                        </div>
                        <div class="mb-3">
                            <label for="keterangan">Keterangan</label>
                            <input id="keterangan" name="keterangan" type="text" class="form-control" required>
                        </div>
                        <div class="mb-3">
                            <label for="bukti">Bukti</label>
                            <input id="bukti" name="bukti" type="text" class="form-control" disabled>
                        </div>
                        <div class="mb-3">
                            <label for="nominal">Nominal</label>
                            <input id="nominal" name="nominal" type="number" class="form-control" required>
                        </div>

                        <div class="mb-3">
                            <label for="nama">Kode Transaksi</label>
                            <select class="form-control select2" name="desa" id="desa" required>
                                <option value="" selected disabled>--Masuk</option>
                                <option value="Bunga Bank">12302 - Bunga Bank</option>
                                <option value="" selected disabled>--Keluar</option>
                                <option value="Penarikan">12303 - Penarikan</option>
                                <option value="Pajak">12304 - Pajak</option>
                                <option value="Biaya Adm">12305 - Biaya Adm</option>
                            </select>
                        </div>
                    </div>
                    <div class="d-flex flex-wrap gap-2">
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save Changes</button>
                        <a  href="{{ URL('buku-bank-perorangan') }}"><button type="button"  class="btn btn-secondary waves-effect waves-light">Cancel</button></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
    <!-- end row -->

@endsection
@section('script')


<!-- select 2 plugin -->
<script src="{{ url('assets/libs/select2/select2.min.js') }}"></script>

<!-- dropzone plugin -->
<script src="{{ url('assets/libs/dropzone/dropzone.min.js') }}"></script>

<!-- init js -->
<script src="{{ url('assets/js/pages/ecommerce-select2.init.js') }}"></script>
<script src="{{ URL::asset('/assets/libs/parsleyjs/parsleyjs.min.js') }}"></script>
<script src="{{ URL::asset('/assets/js/pages/form-validation.init.js') }}"></script>
<script type="text/javascript">
    $('#desa').change(function(){
        var desa = $('#desa').val();
        if(desa == 'other'){
           $('#fieldLainnya').show();
           $('#desaLainnya').attr('required',true);
        }else{
            $('#fieldLainnya').hide();
            $('#desaLainnya').val('');
             $('#desaLainnya').attr('required',false);
        }
    })
  </script>

@endsection

