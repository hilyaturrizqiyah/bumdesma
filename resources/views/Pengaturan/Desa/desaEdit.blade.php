@extends('layouts.master')

@section('title'){{$title}} @endsection

@section('css')
<!-- select2 css -->
<link href="{{ url('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />

<!-- dropzone css -->
<link href="{{ url('assets/libs/dropzone/dropzone.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

@component('components.breadcrumb')
@slot('li_1') Pengaturan @endslot
@slot('title') Edit Desa/Kelurahan @endslot
@endcomponent
@foreach ($data as $data)
<form action="{{ route('pengaturan-desa.update',$data->id) }}"  method="post" class="custom-validation">
    @csrf
    @method('PUT')
<div class="row">
    <div class="col-12">

        <div class="card">
            <div class="card-body">

                @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif


                <div class="row">
                    <div class="col-sm-6">
                        <div class="mb-3">
                            <label for="nama">Nama Desa/Kelurahan</label>
                            <select class="form-control select2" name="desa" id="desa" required>
                                <option value="" selected disabled>-- Pilih Kelurahan/Desa</option>
                                @foreach ($desa as $desaCode)
                                    <option value="{{ $desaCode->code }}" {{ $data->kode == $desaCode->code ? 'selected' : '' }}>{{ $desaCode->name }}</option>
                                @endforeach
                                <option value="other" {{ $data->lainnya == 1 ? 'selected' : ''}}>Lainnya</option>
                            </select>
                        </div>

                        @php
                            if($data->lainnya == 0){
                                $namaDesa = '';
                                $display = 'style="display:none;"';
                            }else{
                                $namaDesa = $data->nama_desa;
                                $display = '';
                            }
                        @endphp
                        <div class="mb-3" id="fieldLainnya" @php echo $display @endphp >
                            <label for="desaLainnya">Kelurahan/Desa Lainnya</label>
                            <input type="text" class="form-control" name="desaLainnya" id="desaLainnya" placeholder="Lainnya" value=" @php
                                echo $namaDesa
                            @endphp" >
                        </div>
                        <div class="mb-3">
                            <label for="alamat">Alamat Kantor</label>
                            <input id="alamat" name="alamat" type="text" class="form-control" value="{{ $data->alamat_desa }}" required>
                        </div>
                        <div class="mb-3">
                            <label for="telp">Telp</label>
                            <input id="telp" name="telp" type="text" class="form-control" value="{{ $data->telp }}" required>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="mb-3">
                            <label for="kepalaDesa">Kepala Desa / Lurah</label>
                            <input id="kepalaDesa" name="kepalaDesa" type="text" class="form-control" value="{{ $data->kepala_desa }}">
                        </div>
                        <div class="mb-3">
                            <label for="status">Status</label>
                            <select name="status" class="form-control select2" required>
                                <option value="kelurahan" {{ $data->status=='kelurahan' ? 'selected' : ''}} >Kelurahan</option>
                                <option value="desa" {{ $data->status == 'desa' ? 'selected' : '' }}>Desa</option>
                            </select>
                        </div>
                    </div>
                    <div class="d-flex flex-wrap gap-2">
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save Changes</button>
                        <button type="button" class="btn btn-secondary waves-effect waves-light">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endforeach
    <!-- end row -->

@endsection
@section('script')


<!-- select 2 plugin -->
<script src="{{ url('assets/libs/select2/select2.min.js') }}"></script>

<!-- dropzone plugin -->
<script src="{{ url('assets/libs/dropzone/dropzone.min.js') }}"></script>

<!-- init js -->
<script src="{{ url('assets/js/pages/ecommerce-select2.init.js') }}"></script>
<script src="{{ URL::asset('/assets/libs/parsleyjs/parsleyjs.min.js') }}"></script>
<script src="{{ URL::asset('/assets/js/pages/form-validation.init.js') }}"></script>
<script type="text/javascript">
    $('#desa').change(function(){
        var desa = $('#desa').val();
        if(desa == 'other'){
           $('#fieldLainnya').show();
           $('#desaLainnya').attr('required',true);
        }else{
            $('#fieldLainnya').hide();
            $('#desaLainnya').val('');
             $('#desaLainnya').attr('required',false);
        }
    })
  </script>

@endsection

