@extends('layouts.master')

@section('title') {{ $title }} @endsection

@section('css')
    <!-- DataTables -->
    <link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Daftar Pengurus @endslot
        @slot('title') Pengurus @endslot
    @endcomponent

    <form action="{{ route('submitPengurus') }}"  method="post" class="custom-validation" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-12">

                <div class="card">
                    <div class="card-body">

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="row">

                            <div class="col-sm-6">
                                <div class="mb-3">
                                    <label for="nik">NIK</label>
                                    <input type="text" name="nik" id="nik" class="w-75 form-control" required data-parsley-minlength="16"
                                               placeholder="Please enter 12 chars." value="{{$pengurus['nik']}}" />
                                </div>
                                <div class="mb-3">
                                    <label class="form-label" for="email">E-Mail</label>
                                    <div>
                                        <input type="email" name="email" id="email" class="w-75 form-control" required parsley-type="email"
                                               value="{{$pengurus['email']}}" placeholder="Enter a valid e-mail" />
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label class="col-md-2 col-form-label" for="jabatan">Jabatan</label>
                                        <select name="jabatan" id="jabatan" class="w-75 form-select" >
                                                <option {{ $pengurus['jabatan'] == null ? 'selected' : ''}} disabled value="">Choose ...</option>
                                                <option {{ $pengurus['jabatan'] == 'ketua' ? 'selected' : '' }} value="ketua">Ketua</option>
                                                <option {{ $pengurus['jabatan'] == 'sektertaris' ? 'selected' : '' }} value="sektertaris">Seketratis</option>
                                                <option {{ $pengurus['jabatan'] == 'bendahara' ? 'selected' : '' }} value="bendahara">Bendahara</option>
                                                <option {{ $pengurus['jabatan'] == 'staff' ? 'selected' : '' }} value="staff">Staff</option>
                                                <option {{ $pengurus['jabatan'] == 'anggota' ? 'selected' : '' }} value="anggota">Anggota</option>
                                        </select>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-4">
                                        <label for="pbod">Tempat Tanggal Lahir</label>
                                        <input id="pbod" name="pbod" type="text" class="form-control" placeholder="Tempat"
                                               value="{{$pengurus['pbod']}}">
                                    </div>
                                    <div class="col-8">
                                        <label for="bod">Tanggal Lahir</label>
                                        <input id="bod" name="bod" type="date" style="width: 60%" class="form-control"
                                               value="{{$pengurus['bod']}}">
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label class="col-md-3 col-form-label" for="gender">Gender</label>
                                    <select name="gender" id="gender" class="w-75 form-select">
                                        <option {{ $pengurus['jabatan'] == null ? 'selected' : ''}} disabled value="">Choose ...</option>
                                        <option {{ $pengurus['jabatan'] == 'laki-laki' ? 'selected' : ''}} value="laki-laki">Laki Laki</option>
                                        <option {{ $pengurus['jabatan'] == 'perempuan' ? 'selected' : ''}} value="perempuan">Perempuan</option>
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label class="col-md-3 col-form-label" for="address">Alamat</label>
                                    <textarea id="address" name="address" required class="w-75 form-control" rows="3">{{$pengurus['address']}}</textarea>
                                </div>

                            </div>

                            <div class="col-sm-6">
                                <div class="mb-3">
                                    <label for="fullname">Nama Lengkap</label>
                                    <input type="text" id="fullname" name="fullname" class="w-75 form-control" required placeholder="Type something"
                                        value="{{$pengurus['fullname']}}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label" for="telephone">Telephone</label>
                                        <input type="tel" id="telephone" name="telephone" class="w-75 form-control" required parsley-type="email"
                                               placeholder="8123456789" value="{{$pengurus['telephone']}}" />
                                </div>
                                <div class="mb-3">
                                    <label class="col-md-3 col-form-label" for="divisi">Kepengurusan</label>
                                    <select id="divisi" name="divisi" class="w-75 form-select">
                                        <option {{ $pengurus['divisi'] == null ? 'selected' : '' }} selected disabled value="">Choose ...</option>
                                        <option {{ $pengurus['divisi'] == 'bkad' ? 'selected' : '' }} value="bkad">BKAD</option>
                                        <option {{ $pengurus['divisi'] == 'upk' ? 'selected' : '' }} value="upk">UPK</option>
                                        <option {{ $pengurus['divisi'] == 'bpupk' ? 'selected' : '' }} value="bpupk">BPUPK</option>
                                        <option {{ $pengurus['divisi'] == 'tv' ? 'selected' : '' }} value="tv">TV</option>
                                        <option {{ $pengurus['divisi'] == 'tpd' ? 'selected' : '' }} value="tpd">TPD</option>
                                    </select>
                                </div>
                                <div class="mb-4">
                                    <label for="file">Image</label>
                                    <input class="w-75 form-control" type="file" id="file" name="file">
                                    @if($pengurus['image'] != null)
                                    <div class="d-flex flex-wrap gap-2">
                                        <button type="button" class="mt-3 btn btn-primary waves-effect waves-light">Preview</button>
                                    </div>
                                    @endif
                                </div>
                            </div>


                            <div class="d-flex flex-wrap gap-2">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">Save Changes</button>
                                <button type="button" onclick="window.history.back()" class="btn btn-secondary waves-effect waves-light">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>


@endsection
@section('script')
    <!-- Required datatable js -->
    <script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/jszip/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/pdfmake/pdfmake.min.js') }}"></script>
    <!-- Datatable init js -->
    <script src="{{ URL::asset('/assets/js/pages/datatables.init.js') }}"></script>
@endsection
