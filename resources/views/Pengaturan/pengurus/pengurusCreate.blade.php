@extends('layouts.master')

@section('title') {{ $title }} @endsection

@section('css')
    <!-- DataTables -->
    <link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Daftar Pengurus @endslot
        @slot('title') Pengurus @endslot
    @endcomponent

    <form action="{{ route('submitPengurus') }}"  method="post" class="custom-validation" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-12">

                <div class="card">
                    <div class="card-body">

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="row">

                            <div class="col-sm-6">
                                <div class="mb-3">
                                    <label for="nik">NIK</label>
                                    <input type="text" name="nik" id="nik" class="w-75 form-control" required minlength="16" maxlength="16"
                                               placeholder="Please enter 16 chars." />
                                </div>
                                <div class="mb-3">
                                    <label class="form-label" for="email">E-Mail</label>
                                    <div>
                                        <input type="email" name="email" id="email" class="w-75 form-control" required parsley-type="email"
                                               placeholder="Enter a valid e-mail" />
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label class="col-md-2 col-form-label" for="jabatan">Jabatan</label>
                                        <select name="jabatan" id="jabatan" class="w-75 form-select">
                                            <option selected disabled value="">Choose ...</option>
                                            <option value="ketua">Ketua</option>
                                            <option value="sektertaris">Seketratis</option>
                                            <option value="bendahara">Bendahara</option>
                                            <option value="staff">Staff</option>
                                            <option value="anggota">Anggota</option>
                                        </select>
                                </div>
                                <div class="row">
                                    <div class="col-4">
                                        <label for="pbod">Tempat Tanggal Lahir</label>
                                        <input id="pbod" name="pbod" type="text" class="form-control" placeholder="Tempat">
                                    </div>
                                    <div class="col-8">
                                        <label for="bod">Tanggal Lahir</label>
                                        <input id="bod" name="bod" type="date" style="width: 60%" class="form-control">
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label class="col-md-3 col-form-label" for="gender">Gender</label>
                                    <select name="gender" id="gender" class="w-75 form-select">
                                        <option selected disabled value="">Choose ...</option>
                                        <option value="laki-laki">Laki Laki</option>
                                        <option value="perempuan">Perempuan</option>
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label class="col-md-3 col-form-label" for="address">Alamat</label>
                                    <textarea id="address" name="address" required class="w-75 form-control" rows="3"></textarea>
                                </div>

                            </div>

                            <div class="col-sm-6">
                                <div class="mb-3">
                                    <label for="fullname">Nama Lengkap</label>
                                    <input type="text" id="fullname" name="fullname" class="w-75 form-control" required placeholder="Type something" />
                                </div>
                                <div class="mb-3">
                                    <label class="form-label" for="telephone">Telephone</label>
                                        <input type="tel" id="telephone" name="telephone" class="w-75 form-control" required parsley-type="email"
                                               placeholder="8123456789" />
                                </div>
                                <div class="mb-3">
                                    <label class="col-md-3 col-form-label" for="divisi">Kepengurusan</label>
                                    <select id="divisi" name="divisi" class="w-75 form-select">
                                        <option selected disabled value="">Choose ...</option>
                                        <option value="bkad">BKAD</option>
                                        <option value="upk">UPK</option>
                                        <option value="bpupk">BPUPK</option>
                                        <option value="tv">TV</option>
                                        <option value="tpd">TPD</option>
                                    </select>
                                </div>
                                <div class="mb-4">
                                    <label for="file">Image</label>
                                    <input class="w-75 form-control" type="file" id="file" name="file">

{{--                                    <div class="mt-4 col-lg-9">--}}
{{--                                        <div class="card border border-success">--}}
{{--                                            <div class="card-header bg-transparent border-success">--}}
{{--                                                <h5 class="my-0 text-success"><i class="mdi mdi-check-all me-3"></i>Success Card</h5>--}}
{{--                                            </div>--}}
{{--                                            <div class="card-body">--}}
{{--                                                <h5 class="card-title mt-0">card title</h5>--}}
{{--                                                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the--}}
{{--                                                    card's content.</p>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

                                </div>
                            </div>


                            <div class="d-flex flex-wrap gap-2">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">Save Changes</button>
                                <button type="button" onclick="window.history.back()" class="btn btn-secondary waves-effect waves-light">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>


@endsection
@section('script')
    <!-- Required datatable js -->
    <script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/jszip/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/pdfmake/pdfmake.min.js') }}"></script>
    <!-- Datatable init js -->
    <script src="{{ URL::asset('/assets/js/pages/datatables.init.js') }}"></script>
@endsection
