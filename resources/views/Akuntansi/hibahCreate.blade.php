@extends('layouts.master')

@section('title') {{ $title }} @endsection

@section('css')
    <!-- DataTables -->
    <link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Saldo Awal @endslot
        @slot('title') Create Saldo Awal @endslot
    @endcomponent

    <form action="{{ route('saldoAwalSubmit') }}"  method="post" class="custom-validation" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-12">

                <div class="card">
                    <div class="card-body">

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="row">
                            <h4>Create User</h4>
                            <div class="col-6">
                                <div class="mb-2">
                                    <label class="col-md-2 col-form-label" for="kode">Kode</label>
                                    <input id="kode" name="kode" class="w-75 form-control" value="101123" required disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="mb-3">
                                    <label for="nama">Nama</label>
                                    <input type="text" name="nama" id="nama" class="w-75 form-control" required minlength="16" maxlength="16"
                                           placeholder="Please enter 16 chars."/>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="mb-3">
                                    <label for="Jumlah">Jumlah</label>
                                    <input type="number" id="Jumlah" name="Jumlah" class="w-75 form-control" required placeholder="Type something"/>
                                </div>
                            </div>
                        </div>

                        <div class="d-flex flex-wrap gap-2">
                            <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                            <button type="button" onclick="window.history.back()" class="btn btn-secondary waves-effect waves-light">Cancel</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </form>


@endsection
@section('script')
    <!-- Required datatable js -->
    <script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/jszip/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/pdfmake/pdfmake.min.js') }}"></script>
    <!-- Datatable init js -->
    <script src="{{ URL::asset('/assets/js/pages/datatables.init.js') }}"></script>
@endsection
