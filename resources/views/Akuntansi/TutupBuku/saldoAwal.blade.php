@extends('layouts.master')

@section('title') {{ $title }} @endsection

@section('css')
    <!-- DataTables -->
    <link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/css/style.css') }}">
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Daftar Pengguna @endslot
        @slot('title') Pengguna @endslot
    @endcomponent

    <style>
        .action-table{
            padding-left: 5px;
            padding-right: 5px;
        }
    </style>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    @if ($message = Session::get('success'))
                        <div class="alert border-0 border-start border-5 border-primary alert-dismissible fade show">
                            <div>{{ $message }}</div>
                        </div>
                    @endif
                    {{-- <h4 class="card-title">Default Datatable</h4>
                    <p class="card-title-desc">DataTables has most features enabled by
                        default, so all you need to do to use it with your own tables is to call
                        the construction function: <code>$().DataTable();</code>.
                    </p> --}}
                    <div class="row">
                        <div class="text-end pb-2">
                            <a class="btn btn-primary btn-sm" href="{{ url('/akuntansi/saldo-awal/create') }}">Tambah</a>
                        </div>
                    </div>
                    <table id="datatable" class="table table-bordered dt-responsive nowraps w-100">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Tanggal</th>
                            <th>Jumlah</th>
                        </tr>
                        </thead>

                        @php
                            $i = 1;
                        @endphp
                        <tbody>
                        {{--                        @foreach ($users as $user )--}}
                        <tr>
                            <td>{{$i++}}</td>
                            <td>example</td>
                            <td>example</td>
                            <td>example</td>
                        </tr>
                        {{--                        @endforeach--}}
                        </tbody>
                    </table>

                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->



@endsection
@section('script')
    <!-- Required datatable js -->
    <script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/jszip/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/pdfmake/pdfmake.min.js') }}"></script>
    <!-- Datatable init js -->
    <script src="{{ URL::asset('/assets/js/pages/datatables.init.js') }}"></script>
@endsection
