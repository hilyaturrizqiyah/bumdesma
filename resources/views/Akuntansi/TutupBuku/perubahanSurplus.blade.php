@extends('layouts.master')

@section('title') {{ $title }} @endsection

@section('css')
    <!-- DataTables -->
    <link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/css/style.css') }}">
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Daftar Pengguna @endslot
        @slot('title') Pengguna @endslot
    @endcomponent

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="mb-3 row">
                        <h4 class="mb-4">Heading</h4>
                        <div class="col-sm-12">
                            <div class="mb-3 row">
                                <label class="col-form-label col-sm-4" for="surplus-ditahan-tahun-lalu">Surplus / Defisit Ditahan s/d Tahun Lalu</label>
                                <div class="col-sm-8">
                                    <input id="surplus-ditahan-tahun-lalu" name="surplus-ditahan-tahun-lalu" class="form-control">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label class="col-form-label col-sm-4" for="penambahan-modal">1. Penambahan Modal</label>
                                <div class="col-sm-8">
                                    <input id="penambahan-modal" name="penambahan-modal" class="form-control">
                                </div>
                            </div>
                            <div class="mb-4 row">
                                <label class="col-form-label col-sm-4" for="resiko-pinjaman">2. Resiko Pinjaman + 2% non SPP</label>
                                <div class="col-sm-8">
                                    <input id="resiko-pinjaman" name="resiko-pinjaman" class="form-control">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label class="col-form-label col-sm-4" for="surplus-ditahan">Surplus / Defisit Ditahan</label>
                                <div class="col-sm-8">
                                    <input id="surplus-ditahan" name="surplus-ditahan" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <!-- Required datatable js -->
    <script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/jszip/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/pdfmake/pdfmake.min.js') }}"></script>
    <!-- Datatable init js -->
    <script src="{{ URL::asset('/assets/js/pages/datatables.init.js') }}"></script>
@endsection
