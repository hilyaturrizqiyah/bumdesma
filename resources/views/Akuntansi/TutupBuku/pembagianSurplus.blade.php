@extends('layouts.master')

@section('title') {{ $title }} @endsection

@section('css')
    <!-- DataTables -->
    <link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/css/style.css') }}">
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Daftar Pengguna @endslot
        @slot('title') Pengguna @endslot
    @endcomponent

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="mb-3 row">
                        <div class="col-sm-12">
                            <div class="mb-4 row">
                                <label for="periode" class="col-sm-4 col-form-label">Periode Tutup Buku</label>
                                <div class="col-sm-4">
                                    <input type="month" class="form-control">
                                </div>
                                <div class="col-sm-2">
                                    <button type="button" class="btn btn-secondary waves-effect waves-light">Proses</button>
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <div class="col-sm-6">
                                    <h4 class="mb-3">Heading</h4>
                                    <div class="mb-3 row">
                                        <label for="surplus-tahun-lalu" class="col-sm-5 col-form-label">Surplus Tahun Lalu</label>
                                        <div class="col-sm-7">
                                            <input id="surplus-tahun-lalu" name="surplus-tahun-lalu" class="form-control">
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label for="surplus-tahun-ini" class="col-sm-5 col-form-label">Surplus Tahun Ini</label>
                                        <div class="col-sm-7">
                                            <input id="surplus-tahun-ini" name="surplus-tahun-ini" class="form-control">
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label for="resiko-pinjaman" class="col-sm-5 col-form-label">Resiko Pinjaman</label>
                                        <div class="col-sm-7">
                                            <input id="resiko-pinjaman" name="resiko-pinjaman" class="form-control">
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label for="operasional" class="col-sm-5 col-form-label">Operasional</label>
                                        <div class="col-sm-7">
                                            <input id="operasional" name="operasional" class="form-control">
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label for="surplus-tahun-ini-netto" class="col-sm-5 col-form-label">Surplus Tahun Ini (Netto)</label>
                                        <div class="col-sm-7">
                                            <input id="surplus-tahun-ini-netto" name="surplus-tahun-ini-netto" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <h4 class="mb-3">Heading</h4>
                                    <div class="mb-3 row">
                                        <label for="bantuan-sosial-rtm" class="col-sm-5 col-form-label">Bantuan Sosial RTM</label>
                                        <div class="col-sm-2">
                                            <input id="bantuan-percent" name="bantuan-percent" class="form-control"
                                                type="number">
                                        </div>
                                        <div class="col-sm-5">
                                            <input id="bantuan-sosial-rtm" name="bantuan-sosial-rtm" class="form-control">
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label for="dana-kelembagaan-kelompok" class="col-sm-5 col-form-label">Dana Kelembagaan Kelompok</label>
                                        <div class="col-sm-2">
                                            <input id="dana-kelembagaan-kelompok-percent" name="dana-kelembagaan-kelompok-percent" class="form-control"
                                                   type="number">
                                        </div>
                                        <div class="col-sm-5">
                                            <input id="dana-kelembagaan-kelompok" name="dana-kelembagaan-kelompok" class="form-control">
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label for="bonus-pengurus" class="col-sm-5 col-form-label">Bonus Pengurus</label>
                                        <div class="col-sm-2">
                                            <input id="bonus-pengurus-percent" name="bonus-pengurus-percent" class="form-control"
                                                   type="number">
                                        </div>
                                        <div class="col-sm-5">
                                            <input id="bonus-pengurus" name="bonus-pengurus" class="form-control">
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label for="lain-lain" class="col-sm-5 col-form-label">Lain Lain</label>
                                        <div class="col-sm-2">
                                            <input id="lain-lain-percent" name="lain-lain-percent" class="form-control"
                                                   type="number">
                                        </div>
                                        <div class="col-sm-5">
                                            <input id="lain-lain" name="lain-lain" class="form-control">
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label for="jumlah-surplus-dibagi" class="col-sm-5 col-form-label">Jumlah Surplus Dibagi</label>
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-5">
                                            <input id="jumlah-surplus-dibagi" name="jumlah-surplus-dibagi" class="form-control">
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label for="penambahan-modal" class="col-sm-5 col-form-label">Untuk Penambahaan Modal</label>
                                        <div class="col-sm-2">
                                            <input id="penambahan-modal-percent" name="penambahan-modal-percent" class="form-control"
                                                   type="number">
                                        </div>
                                        <div class="col-sm-5">
                                            <input id="penambahan-modal" name="penambahan-modal" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <!-- Required datatable js -->
    <script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/jszip/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/pdfmake/pdfmake.min.js') }}"></script>
    <!-- Datatable init js -->
    <script src="{{ URL::asset('/assets/js/pages/datatables.init.js') }}"></script>
@endsection
