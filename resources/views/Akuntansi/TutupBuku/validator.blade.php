@extends('layouts.master')

@section('title') {{ $title }} @endsection

@section('css')
    <!-- DataTables -->
    <link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/css/style.css') }}">
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Daftar Pengguna @endslot
        @slot('title') Pengguna @endslot
    @endcomponent

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <form action="" enctype="multipart/form-data" method="post">
                        @csrf
                        <div class="mb-3 row">
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="mb-3 row">
                                            <label class="col-sm-3 col-sm-form-label" for="month">Month</label>
                                            <div class="col-sm-3">
                                                <input id="month" name="month" type="month" class="form-control">
                                            </div>
                                            <label class="col-sm-2 col-sm-form-label" for="year">Tahun</label>
                                            <div class="col-sm-4">
                                                <input id="year" name="year" type="number" min="1900" max="2099" step="1" placeholder="2022" class="w-75 form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="mb-3 row">
                                            <label class="col-sm-3 col-sm-form-label" for="nama-rek">Nama Rekening</label>
                                            <div class="col-sm-9">
                                                <select id="nama-rek" class="form-select" style="width: 90%!important;">
                                                    <option disabled value="">Choose ...</option>
                                                    <option value="">Nama rekening 1</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <button type="button" class="btn btn-primary waves-effect waves-light">Test</button>
                            </div>
                        </div>
                    </form>
                    <div class="mb-3 row">
                        <div class="col-sm-8">
                            <div class="table-responsive">
                                <table class="table table-borderless">
                                    <thead>
                                        <tr>
                                            <th>Uraian</th>
                                            <th>Master</th>
                                            <th>Kas</th>
                                            <th>Selisih</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">Pokok</th>
                                            <td>Example</td>
                                            <td>Example</td>
                                            <td>Example</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Bunga</th>
                                            <td>Example</td>
                                            <td>Example</td>
                                            <td>Example</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Total</th>
                                            <td>Example</td>
                                            <td>Example</td>
                                            <td>Example</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('script')
    <!-- Required datatable js -->
    <script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/jszip/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/pdfmake/pdfmake.min.js') }}"></script>
    <!-- Datatable init js -->
    <script src="{{ URL::asset('/assets/js/pages/datatables.init.js') }}"></script>
@endsection
