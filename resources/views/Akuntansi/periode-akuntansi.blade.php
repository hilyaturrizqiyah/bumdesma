@extends('layouts.master')

@section('title') @lang('translation.Form_Mask') @endsection

@section('content')

@component('components.breadcrumb')
@slot('li_1') Forms @endslot
@slot('title') Form Mask @endslot
@endcomponent

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <h4 class="card-title">Textual inputs</h4>
                <p class="card-title-desc">Here are examples of <code>.form-control</code> applied to
                    each
                    textual HTML5 <code>&lt;input&gt;</code> <code>type</code>.</p>

                <div class="mb-3 row">
                    <label for="example-date-input" class="col-md-2 col-form-label">Date</label>
                    <div class="col-md-10">
                        <input class="w-75 form-control" type="date" id="date-start" oninput="myFunction()">
                    </div>
                </div>


                <div class="form-group mb-3 row">
                    <label for="input-date2" class="col-md-2 col-form-label">Date </label>
                    <div class="col-md-10">
                        <input id="date-end" type="text" class="w-75 form-control" disabled>
                    </div>
                </div>


            </div>
        </div> <!-- end col -->
    </div>
<!-- end row -->

<script>
    // $('#date-start').oninput( function (){
    //     var d = new Date(document.getElementById('date-start').value)
    //     d.setMonth(d.getMonth()+12)
    //     const dt = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate()
    //     $('#date-end').val(dt)
    // })

    function myFunction(){
        var d = new Date(document.getElementById('date-start').value)
        d.setMonth(d.getMonth() + 12)

        const dt = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate()

        const output = document.getElementById('date-end');
        output.value = dt;
        console.log(output.value)
    }
</script>

@endsection
@section('script')
<!-- form mask -->
<script src="{{ URL::asset('/assets/libs/inputmask/inputmask.min.js') }}"></script>

<!-- form mask init -->
<script src="{{ URL::asset('/assets/js/pages/form-mask.init.js') }}"></script>
@endsection
