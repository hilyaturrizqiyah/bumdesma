@extends('layouts.master')

@section('title') {{ $title }} @endsection

@section('css')
    <!-- DataTables -->
    <link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <style>
        .darker{
            box-shadow: 0 .75rem 1.5rem rgba(0, 0, 0, 0.05) !important;
        }
    </style>
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Pinjaman @endslot
        @slot('title') Detail @endslot
    @endcomponent


    @if ($message = Session::get('success'))
        <div class="alert border-0 border-start border-5 border-primary alert-dismissible fade show">
            <div>{{ $message }}</div>
        </div>
    @endif
    <div class="checkout-tabs">
        <div class="row">
            <div class="col-xl-2 col-sm-3">
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link active" id="v-pills-confir-tab" data-bs-toggle="pill" href="#v-pills-confir" role="tab" aria-controls="v-pills-confir" aria-selected="false">
                        <i class= "fa fa-info-circle d-block check-nav-icon mt-4 mb-2"></i>
                        <p class="fw-bold mb-4">Detail</p>
                    </a>
                    <a class="nav-link" id="v-pills-shipping-tab" data-bs-toggle="pill" href="#v-pills-shipping" role="tab" aria-controls="v-pills-shipping" aria-selected="true">
                        <i class= "fa fa-calendar check-nav-icon mt-4 mb-2"></i>
                        <p class="fw-bold mb-4">Jadwal Angsuran</p>
                    </a>
                </div>
            </div>
            <div class="col-xl-10 col-sm-9">
                <div class="card">
                    <div class="card-body">
                        <form action="">
                            <div class="tab-content" id="v-pills-tabContent">
                                <div class="tab-pane fade show active" id="v-pills-confir" role="tabpanel" aria-labelledby="v-pills-confir-tab">
                                    <div class="card shadow-none border mb-0">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-6">
                                                    <div class="row mb-4">
                                                        <label for="projectname" class="col-form-label col-lg-3">Kode Kelompok</label>
                                                        <div class="col-lg-9">
                                                            <input id="kode" name="kode" type="text" class="form-control" placeholder="Kode Kelompok">
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4">
                                                        <label for="projectdesc" class="col-form-label col-lg-3">Nama Kelompok</label>
                                                        <div class="col-lg-9">
                                                            <input id="nama_kelompok" name="nama_kelompok" type="text" class="form-control" placeholder="Nama Kelompok">
                                                        </div>
                                                    </div>

                                                    <div class="row mb-4">
                                                        <label for="projectbudget" class="col-form-label col-lg-3">Desa</label>
                                                        <div class="col-lg-9">
                                                            <select name="desa" id="desa" class="form-select">
                                                                <option selected disabled>-- Pilih Desa --</option>
                                                                <option value="1">......</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="row mb-4">
                                                        <label for="projectbudget" class="col-form-label col-lg-3">Jumlah Pemanfaat</label>
                                                        <div class="col-lg-4">
                                                            <input id="jml_pemanfaat" name="jml_pemanfaat" type="text" class="form-control input-mask" data-inputmask="'mask': '+99-999999999999'" im-insert="true">
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <label for="projectbudget" class="col-form-label col-lg-4">Orang</label>
                                                        </div>
                                                    </div>

                                                    <div class="row mb-4">
                                                        <label for="projectbudget" class="col-form-label col-lg-3">Tgl. Penyaluran</label>
                                                        <div class="col-lg-9">
                                                            <input id="tgl_penyaluran" name="tgl_penyaluran" type="date" class="form-control input-mask" data-inputmask="'mask': '+99-999999999999'" im-insert="true">
                                                        </div>
                                                    </div>

                                                    <div class="row mb-4">
                                                        <label for="projectbudget" class="col-form-label col-lg-3">Kode Pinjaman</label>
                                                        <div class="col-lg-9">
                                                            <input id="kode_pinjaman" name="kode_pinjaman" type="text" class="form-control" disabled>
                                                        </div>
                                                    </div>

                                                    <div class="row mb-4">
                                                        <label for="projectbudget" class="col-form-label col-lg-3">Suku Bunga</label>
                                                        <div class="col-lg-4">
                                                            <input id="suku_bunga" name="suku_bunga" type="text" class="form-control" >
                                                        </div>
                                                        <div class="col-lg-5">
                                                            <label for="projectbudget" class="col-form-label col-lg-6">% per Tahun</label>
                                                        </div>
                                                    </div>

                                                    <div class="row mb-4">
                                                        <label for="projectbudget" class="col-form-label col-lg-3">Jangka Waktu</label>
                                                        <div class="col-lg-4">
                                                            <input id="suku_bunga" name="suku_bunga" type="text" class="form-control" >
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <label for="projectbudget" class="col-form-label col-lg-4">Bulan</label>
                                                        </div>
                                                    </div>

                                                    <div class="row mb-4">
                                                        <label class="col-form-label col-lg-3">Periode Angsuran</label>
                                                        <div class="col-lg-9">
                                                            <select name="periode_angsuran" id="periode_angsuran" class="form-select">
                                                                <option selected disabled>-- Pilih Periode Angsuran --</option>
                                                                <option value="1">--</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="row mb-4">
                                                        <label for="projectbudget" class="col-form-label col-lg-3">Jumlah Pinjaman</label>
                                                        <div class="col-lg-9">
                                                            <input id="jml_pinjaman" name="jml_pinjaman" type="text" class="form-control" >
                                                        </div>
                                                    </div>

                                                    <div class="row mb-4">
                                                        <label for="projectbudget" class="col-form-label col-lg-3">Jumlah Bunga</label>
                                                        <div class="col-lg-9">
                                                            <input id="jml_bunga" name="jml_bunga" type="text" class="form-control" >
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="col-6">

                                                    <div class="row mb-4">
                                                        <label class="col-form-label col-lg-3">Nama Ketua</label>
                                                        <div class="col-lg-9">
                                                            <input id="nama_ketua" name="nama_ketua" type="text" class="form-control" placeholder="Nama Ketua">
                                                        </div>
                                                    </div>

                                                    <div class="row mb-4">
                                                        <label class="col-form-label col-lg-3">Alamat Ketua</label>
                                                        <div class="col-lg-9">
                                                            <textarea id="alamat" name="alamat" class="form-control" rows="3" placeholder="Alamat Ketua"></textarea>
                                                        </div>
                                                    </div>

                                                    <div class="row mb-4">
                                                        <label for="projectbudget" class="col-form-label col-lg-3">Tgl. Pelunasan</label>
                                                        <div class="col-lg-9">
                                                            <input id="tgl_pelunasan" name="tgl_pelunasan" type="date" class="form-control input-mask" data-inputmask="'mask': '+99-999999999999'" im-insert="true">
                                                        </div>
                                                    </div>

                                                    <div class="row mb-4">
                                                        <label class="col-form-label col-lg-3">Perguliran ke</label>
                                                        <div class="col-lg-9">
                                                            <input id="perguliran_ke" name="perguliran_ke" type="number" class="form-control" class="form-control input-mask" data-inputmask="'mask': '+99-999999999999'" im-insert="true">
                                                        </div>
                                                    </div>

                                                    <div class="row mb-4">
                                                        <label class="col-form-label col-lg-3">Angsuran Pokok</label>
                                                        <div class="col-lg-9">
                                                            <input id="angsuran_pokok" name="angsuran_pokok" type="text" class="form-control" >
                                                        </div>
                                                    </div>

                                                    <div class="row mb-4">
                                                        <label class="col-form-label col-lg-3">Angsuran Bunga</label>
                                                        <div class="col-lg-9">
                                                            <input id="angsuran_bunga" name="angsuranangsuran_bunga_pokok" type="text" class="form-control" >
                                                        </div>
                                                    </div>

                                                    <div class="row mb-4">
                                                        <label class="col-form-label col-lg-3">Angsuran per Bulan</label>
                                                        <div class="col-lg-9">
                                                            <input id="angsuran_per_bulan" name="angsuran_per_bulan" type="text" class="form-control" disabled>
                                                        </div>
                                                    </div>

                                                    <div class="row mb-4">
                                                        <label class="col-form-label col-lg-3">Sumber Pendanaan</label>
                                                        <div class="col-lg-9">
                                                            <select name="sumber_pendanaan" id="sumber_pendanaan" class="form-select">
                                                                <option selected disabled>-- Pilih Sumber Pendanaan --</option>
                                                                <option value="1">----</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="row mb-4">
                                                        <label class="col-form-label col-lg-3">Status</label>
                                                        <div class="col-lg-9">
                                                            <select name="status" id="" class="form-select">
                                                                <option selected disabled>-- Pilih Status --</option>
                                                                <option value="1">----</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="v-pills-shipping" role="tabpanel" aria-labelledby="v-pills-shipping-tab">
                                    <div class="card shadow-none border mb-0">
                                        <div class="card-body">
                                        <h4 class="pb-2">Jadwal Angsuran</h4>
                                            <table id="jadwal_angsuran" class="table table-bordered dt-responsive  nowrap w-100">
                                                <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Tanggal</th>
                                                    <th>Pokok</th>
                                                    <th>Bunga</th>
                                                    <th>Total</th>
                                                    <th>Aksi</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>08-04-2022</td>
                                                    <td>25.000</td>
                                                    <td>0</td>
                                                    <td>25.000</td>
                                                    <td>
                                                    <button class="btn btn-outline-info btn-sm" type="button" data-bs-toggle="modal" data-bs-target="#angsuran-uep-modal"> Edit</button>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Batal</button>
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div> <!-- end row -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- modal uep -->
<div class="modal fade" id="angsuran-uep-modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
        <div class="modal-content">
            <form method="post" action="">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Edit Angsuran</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="row mb-4">
                                <label for="projectbudget" class="col-form-label col-lg-3">Tanggal</label>
                                <div class="col-lg-9">
                                    <input id="tgl_penyaluran" name="tgl_penyaluran" type="date" class="form-control input-mask" data-inputmask="'mask': '+99-999999999999'" im-insert="true">
                                </div>
                            </div>
                            <div class="row mb-4">
                                <label for="projectname" class="col-form-label col-lg-3">Pokok</label>
                                <div class="col-lg-9">
                                    <input id="kode" name="kode" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="row mb-4">
                                <label for="projectdesc" class="col-form-label col-lg-3">Bunga</label>
                                <div class="col-lg-9">
                                    <input id="nama_kelompok" name="nama_kelompok" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="row mb-4">
                                <label for="projectdesc" class="col-form-label col-lg-3">Total</label>
                                <div class="col-lg-9">
                                    <input id="nama_kelompok" name="nama_kelompok" type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@section('script')
    <!-- Required datatable js -->
    <script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/jszip/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/pdfmake/pdfmake.min.js') }}"></script>
    <!-- Datatable init js -->
    <script src="{{ URL::asset('/assets/js/pages/datatables.init.js') }}"></script>
    <script src="{{asset('assets/js/pages/register.init.js')}}"></script>
    <script>
        $(document).ready(function () {
            let pengajuan = $('#jumlah_pengajuan')
            pengajuan.val(formatRupiah(pengajuan.val(), 'Rp. '))

            let pinjaman = $('#jumlah_pinjaman')
            pinjaman.val(formatRupiah(pinjaman.val(), 'Rp. '))

            function initDatatable(id) {
                $(`#${id}`).DataTable();
            }

            initDatatable('jadwal_angsuran')
        })
    </script>
@endsection
