@extends('layouts.master')

@section('title') {{ $title }} @endsection

@section('css')
<!-- DataTables -->
<link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

@component('components.breadcrumb')
@slot('li_1') Pinjaman @endslot
@slot('title') Daftar Pinjaman Kelompok @endslot
@endcomponent


<div class="col-xl-12">
    <div class="card">
        <div class="card-body">
            @if ($message = Session::get('success'))
            <div class="alert border-0 border-start border-5 border-primary alert-dismissible fade show">
                <div>{{ $message }}</div>
            </div>
            @endif

            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-bs-toggle="tab" href="#home" role="tab">
                        <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                        <span class="d-none d-sm-block">UEP</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-bs-toggle="tab" href="#profile" role="tab">
                        <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                        <span class="d-none d-sm-block">SPP</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-bs-toggle="tab" href="#messages" role="tab">
                        <span class="d-block d-sm-none"><i class="far fa-envelope"></i></span>
                        <span class="d-none d-sm-block">Lainnya</span>
                    </a>
                </li>

            </ul>

            <!-- Tab panes -->
            <div class="tab-content p-3 text-muted">
                <div class="tab-pane active" id="home" role="tabpanel">
                    <p class="mb-0">
                        <table id="uep" class="table table-bordered dt-responsive  nowrap w-100">
                            <thead>
                                <tr>
                                    <th>Kode</th>
                                    <th>Nama Kelompok</th>
                                    <th>Desa</th>
                                    <th>Ke</th>
                                    <th>Tgl. Cair</th>
                                    <th>Pinjaman</th>
                                    <th>J/W</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>

                            @php
                            $i = 1;
                            @endphp
                            <tbody>
                                {{-- @foreach ($data as $data ) --}}
                                <tr>
                                    <td>3210250320101</td>
                                    <td>CEMPAKA</td>
                                    <td>GARAWASTU</td>
                                    <td>1</td>
                                    <td>08-10-2021</td>
                                    <td>300.000</td>
                                    <td>12</td>
                                    <td>LUNAS</td>
                                    <td>
                                        <a class="btn btn-outline-info btn-sm" href="{{ route('pinjaman-kelompok-uep.detail', 2) }}">Detail</a>
                                        <button type="submit" class="btn btn-outline-danger btn-sm">Hapus</button>
                                    </td>
                                </tr>
                                {{-- @endforeach --}}
                            </tbody>
                        </table>
                    </p>
                </div>

                <div class="tab-pane" id="profile" role="tabpanel">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-8">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="card-block">
                                            <div class="dt-responsive table-responsive"><br>
                                                <table id="spp" class="table table-bordered table-bordered nowrap">
                                                    <thead>
                                                        <tr>
                                                            <th>Kode</th>
                                                            <th>Nama Kelompok</th>
                                                            <th>Desa</th>
                                                            <th>Ke</th>
                                                            <th>Tgl. Cair</th>
                                                            <th>Pinjaman</th>
                                                            <th>J/W</th>
                                                            <th>Status</th>
                                                            <th>Aksi</th>
                                                        </tr>
                                                    </thead>

                                                    @php
                                                    $i = 1;
                                                    @endphp
                                                    <tbody>
                                                        {{-- @foreach ($data as $data ) --}}
                                                        <tr>
                                                            <td>3210250320101</td>
                                                            <td>CEMPAKA</td>
                                                            <td>GARAWASTU</td>
                                                            <td>1</td>
                                                            <td>08-10-2021</td>
                                                            <td>300.000</td>
                                                            <td>12</td>
                                                            <td>LUNAS</td>
                                                            <td>
                                                                <a class="btn btn-outline-info btn-sm" href="{{ route('pinjaman-kelompok-spp.detail', 2) }}">Detail</a>
                                                                <button type="submit" class="btn btn-outline-danger btn-sm">Hapus</button>
                                                            </td>
                                                        </tr>
                                                        {{-- @endforeach --}}
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-4">
                                <div class="card">
                                    <form id="form-cart" method="POST">
                                        <div class="card-body">
                                            <center><h4>Kartu Angsuran</h3></center>
                                            <div class="dt-responsive table-responsive"><br>
                                                <table id="spp" class="table table-bordered table-bordered nowrap">
                                                    <thead>
                                                        <tr>
                                                            <th>Tanggal</th>
                                                            <th>Pokok</th>
                                                            <th>Bunga</th>
                                                            <th>Total</th>
                                                        </tr>
                                                    </thead>

                                                    @php
                                                    $i = 1;
                                                    @endphp
                                                    <tbody>
                                                        {{-- @foreach ($data as $data ) --}}
                                                        <tr>
                                                            <td>08-10-2021</td>
                                                            <td>300.000</td>
                                                            <td>0</td>
                                                            <td>300.000</td>
                                                        </tr>
                                                        {{-- @endforeach --}}
                                                        <tr>
                                                            <td><b>Jumlah</b></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="messages" role="tabpanel">
                    <p class="mb-0">
                        <table id="lainnya" class="table table-bordered dt-responsive  nowrap w-100">
                            <thead>
                                <tr>
                                    <th>Kode</th>
                                    <th>Nama Kelompok</th>
                                    <th>Desa</th>
                                    <th>Post</th>
                                    <th>Pokok</th>
                                    <th>Bunga</th>
                                    <th>Jenis Kelompok</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>

                            @php
                            $i = 1;
                            @endphp
                            <tbody>
                                {{-- @foreach ($data as $data ) --}}
                                <tr>
                                    <td>3210250320101</td>
                                    <td>CEMPAKA</td>
                                    <td>GARAWASTU</td>
                                    <td>08-10-2021</td>
                                    <td>300.000</td>
                                    <td>300.000</td>
                                    <td>SPP</td>
                                    <td>
                                        <button class="btn btn-outline-primary btn-sm" type="button" data-bs-toggle="modal" data-bs-target="#lainnya-modal"> Detail</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>3210250320101</td>
                                    <td>CEMPAKA</td>
                                    <td>GARAWASTU</td>
                                    <td>08-10-2021</td>
                                    <td>300.000</td>
                                    <td>300.000</td>
                                    <td>UEP</td>
                                    <td>
                                        <button class="btn btn-outline-primary btn-sm" type="button" data-bs-toggle="modal" data-bs-target="#lainnya-modal"> Detail</button>
                                    </td>
                                </tr>
                                {{-- @endforeach --}}
                            </tbody>
                        </table>
                    </p>
            </div>

        </div>
    </div>
</div>

<!-- modal uep -->
<div class="modal fade" id="uep-modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
        <div class="modal-content">
            <form method="post" action="">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Update Data</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-6">
                            <div class="row mb-4">
                                <label for="projectname" class="col-form-label col-lg-3">Kode Kelompok</label>
                                <div class="col-lg-9">
                                    <input id="kode" name="kode" type="text" class="form-control" placeholder="Kode Kelompok">
                                </div>
                            </div>
                            <div class="row mb-4">
                                <label for="projectdesc" class="col-form-label col-lg-3">Nama Kelompok</label>
                                <div class="col-lg-9">
                                    <input id="nama_kelompok" name="nama_kelompok" type="text" class="form-control" placeholder="Nama Kelompok">
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label for="projectbudget" class="col-form-label col-lg-3">Desa</label>
                                <div class="col-lg-9">
                                    <select name="desa" id="desa" class="form-select">
                                        <option selected disabled>-- Pilih Desa --</option>
                                        <option value="1">......</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label for="projectbudget" class="col-form-label col-lg-3">Jumlah Pemanfaat</label>
                                <div class="col-lg-4">
                                    <input id="jml_pemanfaat" name="jml_pemanfaat" type="text" class="form-control input-mask" data-inputmask="'mask': '+99-999999999999'" im-insert="true">
                                </div>
                                <div class="col-lg-4">
                                    <label for="projectbudget" class="col-form-label col-lg-3">Orang</label>
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label for="projectbudget" class="col-form-label col-lg-3">Tgl. Penyaluran</label>
                                <div class="col-lg-9">
                                    <input id="tgl_penyaluran" name="tgl_penyaluran" type="date" class="form-control input-mask" data-inputmask="'mask': '+99-999999999999'" im-insert="true">
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label for="projectbudget" class="col-form-label col-lg-3">Kode Pinjaman</label>
                                <div class="col-lg-9">
                                    <input id="kode_pinjaman" name="kode_pinjaman" type="text" class="form-control" disabled>
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label for="projectbudget" class="col-form-label col-lg-3">Suku Bunga</label>
                                <div class="col-lg-4">
                                    <input id="suku_bunga" name="suku_bunga" type="text" class="form-control" >
                                </div>
                                <div class="col-lg-5">
                                    <label for="projectbudget" class="col-form-label col-lg-5">% per Tahun</label>
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label for="projectbudget" class="col-form-label col-lg-3">Jangka Waktu</label>
                                <div class="col-lg-4">
                                    <input id="suku_bunga" name="suku_bunga" type="text" class="form-control" >
                                </div>
                                <div class="col-lg-4">
                                    <label for="projectbudget" class="col-form-label col-lg-3">Bulan</label>
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label class="col-form-label col-lg-3">Periode Angsuran</label>
                                <div class="col-lg-9">
                                    <select name="periode_angsuran" id="periode_angsuran" class="form-select">
                                        <option selected disabled>-- Pilih Periode Angsuran --</option>
                                        <option value="1">--</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label for="projectbudget" class="col-form-label col-lg-3">Jumlah Pinjaman</label>
                                <div class="col-lg-9">
                                    <input id="jml_pinjaman" name="jml_pinjaman" type="text" class="form-control" >
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label for="projectbudget" class="col-form-label col-lg-3">Jumlah Bunga</label>
                                <div class="col-lg-9">
                                    <input id="jml_bunga" name="jml_bunga" type="text" class="form-control" >
                                </div>
                            </div>

                        </div>

                        <div class="col-6">

                            <div class="row mb-4">
                                <label class="col-form-label col-lg-3">Nama Ketua</label>
                                <div class="col-lg-9">
                                    <input id="nama_ketua" name="nama_ketua" type="text" class="form-control" placeholder="Nama Ketua">
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label class="col-form-label col-lg-3">Alamat Ketua</label>
                                <div class="col-lg-9">
                                    <textarea id="alamat" name="alamat" class="form-control" rows="3" placeholder="Alamat Ketua"></textarea>
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label for="projectbudget" class="col-form-label col-lg-3">Tgl. Pelunasan</label>
                                <div class="col-lg-9">
                                    <input id="tgl_pelunasan" name="tgl_pelunasan" type="date" class="form-control input-mask" data-inputmask="'mask': '+99-999999999999'" im-insert="true">
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label class="col-form-label col-lg-3">Pergliran ke</label>
                                <div class="col-lg-9">
                                    <input id="perguliran_ke" name="perguliran_ke" type="number" class="form-control" class="form-control input-mask" data-inputmask="'mask': '+99-999999999999'" im-insert="true">
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label class="col-form-label col-lg-3">Angsuran Pokok</label>
                                <div class="col-lg-9">
                                    <input id="angsuran_pokok" name="angsuran_pokok" type="text" class="form-control" >
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label class="col-form-label col-lg-3">Angsuran Bunga</label>
                                <div class="col-lg-9">
                                    <input id="angsuran_bunga" name="angsuranangsuran_bunga_pokok" type="text" class="form-control" >
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label class="col-form-label col-lg-3">Angsuran per Bulan</label>
                                <div class="col-lg-9">
                                    <input id="angsuran_per_bulan" name="angsuran_per_bulan" type="text" class="form-control" disabled>
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label class="col-form-label col-lg-3">Sumber Pendanaan</label>
                                <div class="col-lg-9">
                                    <select name="sumber_pendanaan" id="sumber_pendanaan" class="form-select">
                                        <option selected disabled>-- Pilih Sumber Pendanaan --</option>
                                        <option value="1">----</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label class="col-form-label col-lg-3">Status</label>
                                <div class="col-lg-9">
                                    <select name="status" id="" class="form-select">
                                        <option selected disabled>-- Pilih Status --</option>
                                        <option value="1">----</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- modal spp -->
<div class="modal fade" id="spp-modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
        <div class="modal-content">
            <form method="post" action="">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Update Data</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-6">
                            <div class="row mb-4">
                                <label for="projectname" class="col-form-label col-lg-3">Kode Kelompok</label>
                                <div class="col-lg-9">
                                    <input id="kode" name="kode" type="text" class="form-control" placeholder="Kode Kelompok">
                                </div>
                            </div>
                            <div class="row mb-4">
                                <label for="projectdesc" class="col-form-label col-lg-3">Nama Kelompok</label>
                                <div class="col-lg-9">
                                    <input id="nama_kelompok" name="nama_kelompok" type="text" class="form-control" placeholder="Nama Kelompok">
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label for="projectbudget" class="col-form-label col-lg-3">Desa</label>
                                <div class="col-lg-9">
                                    <select name="desa" id="desa" class="form-select">
                                        <option selected disabled>-- Pilih Desa --</option>
                                        <option value="1">......</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label for="projectbudget" class="col-form-label col-lg-3">Jumlah Pemanfaat</label>
                                <div class="col-lg-4">
                                    <input id="jml_pemanfaat" name="jml_pemanfaat" type="text" class="form-control input-mask" data-inputmask="'mask': '+99-999999999999'" im-insert="true">
                                </div>
                                <div class="col-lg-4">
                                    <label for="projectbudget" class="col-form-label col-lg-3">Orang</label>
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label for="projectbudget" class="col-form-label col-lg-3">Tgl. Penyaluran</label>
                                <div class="col-lg-9">
                                    <input id="tgl_penyaluran" name="tgl_penyaluran" type="date" class="form-control input-mask" data-inputmask="'mask': '+99-999999999999'" im-insert="true">
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label for="projectbudget" class="col-form-label col-lg-3">Kode Pinjaman</label>
                                <div class="col-lg-9">
                                    <input id="kode_pinjaman" name="kode_pinjaman" type="text" class="form-control" >
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label for="projectbudget" class="col-form-label col-lg-3">Suku Bunga</label>
                                <div class="col-lg-4">
                                    <input id="suku_bunga" name="suku_bunga" type="text" class="form-control" >
                                </div>
                                <div class="col-lg-5">
                                    <label for="projectbudget" class="col-form-label col-lg-5">% per Tahun</label>
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label for="projectbudget" class="col-form-label col-lg-3">Jangka Waktu</label>
                                <div class="col-lg-4">
                                    <input id="suku_bunga" name="suku_bunga" type="text" class="form-control" >
                                </div>
                                <div class="col-lg-4">
                                    <label for="projectbudget" class="col-form-label col-lg-3">Bulan</label>
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label class="col-form-label col-lg-3">Periode Angsuran</label>
                                <div class="col-lg-9">
                                    <select name="periode_angsuran" id="periode_angsuran" class="form-select">
                                        <option selected disabled>-- Pilih Periode Angsuran --</option>
                                        <option value="1">--</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label for="projectbudget" class="col-form-label col-lg-3">Jumlah Pinjaman</label>
                                <div class="col-lg-9">
                                    <input id="jml_pinjaman" name="jml_pinjaman" type="text" class="form-control" >
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label for="projectbudget" class="col-form-label col-lg-3">Jumlah Bunga</label>
                                <div class="col-lg-9">
                                    <input id="jml_bunga" name="jml_bunga" type="text" class="form-control" >
                                </div>
                            </div>

                        </div>

                        <div class="col-6">

                            <div class="row mb-4">
                                <label class="col-form-label col-lg-3">Nama Ketua</label>
                                <div class="col-lg-9">
                                    <input id="nama_ketua" name="nama_ketua" type="text" class="form-control" placeholder="Nama Ketua">
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label class="col-form-label col-lg-3">Alamat Ketua</label>
                                <div class="col-lg-9">
                                    <textarea id="alamat" name="alamat" class="form-control" rows="3" placeholder="Alamat Ketua"></textarea>
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label for="projectbudget" class="col-form-label col-lg-3">Tgl. Pelunasan</label>
                                <div class="col-lg-9">
                                    <input id="tgl_pelunasan" name="tgl_pelunasan" type="date" class="form-control input-mask" data-inputmask="'mask': '+99-999999999999'" im-insert="true">
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label class="col-form-label col-lg-3">Pergliran ke</label>
                                <div class="col-lg-9">
                                    <input id="perguliran_ke" name="perguliran_ke" type="number" class="form-control" class="form-control input-mask" data-inputmask="'mask': '+99-999999999999'" im-insert="true">
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label class="col-form-label col-lg-3">Angsuran Pokok</label>
                                <div class="col-lg-9">
                                    <input id="angsuran_pokok" name="angsuran_pokok" type="text" class="form-control" >
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label class="col-form-label col-lg-3">Angsuran Bunga</label>
                                <div class="col-lg-9">
                                    <input id="angsuran_bunga" name="angsuranangsuran_bunga_pokok" type="text" class="form-control" >
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label class="col-form-label col-lg-3">Angsuran per Bulan</label>
                                <div class="col-lg-9">
                                    <input id="angsuran_per_bulan" name="angsuran_per_bulan" type="text" class="form-control" >
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label class="col-form-label col-lg-3">Sumber Pendanaan</label>
                                <div class="col-lg-9">
                                    <select name="sumber_pendanaan" id="sumber_pendanaan" class="form-select">
                                        <option selected disabled>-- Pilih Sumber Pendanaan --</option>
                                        <option value="1">----</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label class="col-form-label col-lg-3">Status</label>
                                <div class="col-lg-9">
                                    <select name="status" id="" class="form-select">
                                        <option selected disabled>-- Pilih Status --</option>
                                        <option value="1">----</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- modal lainnya -->
<div class="modal fade" id="lainnya-modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
        <div class="modal-content">
            <form method="post" action="">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Update Data</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-6">
                            <label for="projectbudget" class="col-form-label col-lg-5" style="color: blue;">Data Utama</label>
                            <div class="row mb-4">
                                <label for="projectname" class="col-form-label col-lg-3">Kode Kelompok</label>
                                <div class="col-lg-9">
                                    <input id="kode" name="kode" type="text" class="form-control" disabled>
                                </div>
                            </div>
                            <div class="row mb-4">
                                <label for="projectdesc" class="col-form-label col-lg-3">Nama Kelompok</label>
                                <div class="col-lg-9">
                                    <input id="nama_kelompok" name="nama_kelompok" type="text" class="form-control" disabled>
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label for="projectbudget" class="col-form-label col-lg-3">Desa</label>
                                <div class="col-lg-9">
                                    <select name="desa" id="desa" class="form-select" disabled>
                                        <option selected disabled>-- Pilih Desa --</option>
                                        <option value="1">......</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label for="projectbudget" class="col-form-label col-lg-3">Jumlah Pemanfaat</label>
                                <div class="col-lg-4">
                                    <input id="jml_pemanfaat" name="jml_pemanfaat" type="text" class="form-control" disabled>
                                </div>
                                <div class="col-lg-4">
                                    <label for="projectbudget" class="col-form-label col-lg-3">Orang</label>
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label for="projectbudget" class="col-form-label col-lg-3">Kode Pinjaman</label>
                                <div class="col-lg-9">
                                    <input id="kode_pinjaman" name="kode_pinjaman" type="text" class="form-control" disabled>
                                </div>
                            </div>

                            <label for="projectbudget" class="col-form-label col-lg-5" style="color: blue;">Data Pinjaman Lain-lain</label>
                            <div class="row mb-4">
                                <label for="projectbudget" class="col-form-label col-lg-3">Tgl. Post</label>
                                <div class="col-lg-9">
                                    <input id="tgl_penyaluran" name="tgl_penyaluran" type="date" class="form-control input-mask" data-inputmask="'mask': '+99-999999999999'" im-insert="true">
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label for="projectbudget" class="col-form-label col-lg-3">Pokok</label>
                                <div class="col-lg-9">
                                    <input id="jml_bunga" name="jml_bunga" type="text" class="form-control" >
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label for="projectbudget" class="col-form-label col-lg-3">Bunga</label>
                                <div class="col-lg-9">
                                    <input id="jml_bunga" name="jml_bunga" type="text" class="form-control" >
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label for="projectbudget" class="col-form-label col-lg-3">Total</label>
                                <div class="col-lg-9">
                                    <input id="jml_bunga" name="jml_bunga" type="text" class="form-control" disabled>
                                </div>
                            </div>

                        </div>

                        <div class="col-6"><br><br>

                            <div class="row mb-4">
                                <label for="projectbudget" class="col-form-label col-lg-3">Tgl. Penyaluran</label>
                                <div class="col-lg-9">
                                    <input id="tgl_pelunasan" name="tgl_pelunasan" type="date" class="form-control" disabled>
                                </div>
                            </div>


                            <div class="row mb-4">
                                <label class="col-form-label col-lg-3">Jumlah Pinjaman</label>
                                <div class="col-lg-9">
                                    <input id="nama_ketua" name="nama_ketua" type="text" class="form-control" disabled>
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label class="col-form-label col-lg-3">Jumlah Bunga</label>
                                <div class="col-lg-9">
                                    <input id="nama_ketua" name="nama_ketua" type="text" class="form-control" disabled>
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label for="projectbudget" class="col-form-label col-lg-3">Jangka Waktu</label>
                                <div class="col-lg-4">
                                    <input id="suku_bunga" name="suku_bunga" type="text" class="form-control" disabled>
                                </div>
                                <div class="col-lg-4">
                                    <label for="projectbudget" class="col-form-label col-lg-3">Bulan</label>
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label for="projectbudget" class="col-form-label col-lg-3">Suku Bunga</label>
                                <div class="col-lg-4">
                                    <input id="suku_bunga" name="suku_bunga" type="text" class="form-control" disabled>
                                </div>
                                <div class="col-lg-5">
                                    <label for="projectbudget" class="col-form-label col-lg-5">% per Tahun</label>
                                </div>
                            </div><br><br><br>

                            <div class="row mb-4">
                                <label class="col-form-label col-lg-3">Masalah</label>
                                <div class="col-lg-9">
                                    <input id="nama_ketua" name="nama_ketua" type="text" class="form-control" >
                                </div>
                            </div>

                            <div class="row mb-4">
                                <label class="col-form-label col-lg-3">Keterangan</label>
                                <div class="col-lg-9">
                                    <textarea id="alamat" name="alamat" class="form-control" rows="3" ></textarea>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@section('script')
<!-- Required datatable js -->
<script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>
<script src="{{ URL::asset('/assets/libs/jszip/jszip.min.js') }}"></script>
<script src="{{ URL::asset('/assets/libs/pdfmake/pdfmake.min.js') }}"></script>
<!-- Datatable init js -->
<script src="{{ URL::asset('/assets/js/pages/datatables.init.js') }}"></script>
<script>
    function initDatatable(id) {
        $(`#${id}`).DataTable();
    }

    initDatatable('uep')
    initDatatable('spp')
    initDatatable('lainnya')

</script>
@endsection
