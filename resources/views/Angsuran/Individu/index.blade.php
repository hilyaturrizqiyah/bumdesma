@extends('layouts.master')

@section('title') {{ $title }} @endsection

@section('css')
    <!-- DataTables -->
    <link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Penerimaan Angsuran @endslot
        @slot('title') Daftar Individu @endslot
    @endcomponent


    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                @if ($message = Session::get('success'))
                    <div class="alert border-0 border-start border-5 border-primary alert-dismissible fade show">
                        <div>{{ $message }}</div>
                    </div>
                @endif

                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-bs-toggle="tab" href="#home" role="tab">
                            <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                            <span class="d-none d-sm-block">UEP</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-bs-toggle="tab" href="#profile" role="tab">
                            <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                            <span class="d-none d-sm-block">SPP</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-bs-toggle="tab" href="#messages" role="tab">
                            <span class="d-block d-sm-none"><i class="far fa-envelope"></i></span>
                            <span class="d-none d-sm-block">Lain-Lain</span>
                        </a>
                    </li>

                </ul>

                <!-- Tab panes -->
                <div class="tab-content p-3 text-muted">
                    <div class="tab-pane active" id="home" role="tabpanel">
                        <p class="mb-0">
                        <table id="uep" class="table table-bordered dt-responsive  nowrap w-100">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode</th>
                                <th>Nama Debitur</th>
                                <th>Desa</th>
                                <th>Tanggal Cair</th>
                                <th>Pinjaman</th>
                                <th>Jasa</th>
                                <th>Jangka Waktu</th>
                                <th>Produk</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>

                            <tr>
                                <td>1</td>
                                <td>32121432342999</td>
                                <td>Cempaka</td>
                                <td>Sindang</td>
                                <td>25 Mar, 2017</td>
                                <td>5000000</td>
                                <td>20.00</td>
                                <td>12</td>
                                <td>HP</td>
                                <td class="text-center">
                                    <a class="btn btn-outline-primary btn-sm" href="{{ route('angsuran-individu-uep.show', 2) }}">Pilih</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        </p>
                    </div>

                    <div class="tab-pane" id="profile" role="tabpanel">
                        <p class="mb-0">
                        <table id="spp" class="table table-bordered dt-responsive  nowrap w-100">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode</th>
                                <th>Nama Debitur</th>
                                <th>Desa</th>
                                <th>Tanggal Cair</th>
                                <th>Pinjaman</th>
                                <th>Jasa</th>
                                <th>Jangka Waktu</th>
                                <th>Produk</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>

                            <tr>
                                <td>1</td>
                                <td>32121432342999</td>
                                <td>Cempaka</td>
                                <td>Sindang</td>
                                <td>25 Mar, 2017</td>
                                <td>5000000</td>
                                <td>20.00</td>
                                <td>12</td>
                                <td>HP</td>
                                <td class="text-center">
                                    <a class="btn btn-outline-primary btn-sm" href="{{ route('angsuran-individu-spp.show', 2) }}">Pilih</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        </p>
                    </div>
                    <div class="tab-pane" id="messages" role="tabpanel">
                        <table id="lain" class="table table-bordered dt-responsive  nowrap w-100">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode</th>
                                <th>Nama Debitur</th>
                                <th>Jenis Kelompok</th>
                                <th>Desa</th>
                                <th>Pokok</th>
                                <th>Bunga</th>
                                <th>Produk</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>3243454545</td>
                                <td>Bunga Mawar</td>
                                <td>UEP</td>
                                <td>Citarum</td>
                                <td>5000000</td>
                                <td>500000</td>
                                <td>HP</td>
                                <td class="text-center">
                                    <a class="btn btn-outline-primary btn-sm" href="{{ route('angsuran-individu-lain.show', 2) }}">Pilih</a>
                                </td>
                            </tr>

                            <tr>
                                <td>1</td>
                                <td>3243992882</td>
                                <td>Bunga Melati</td>
                                <td>SPP</td>
                                <td>Sindang</td>
                                <td>5000000</td>
                                <td>500000</td>
                                <td>HP</td>
                                <td class="text-center">
                                    <a class="btn btn-outline-primary btn-sm" href="{{ route('angsuran-individu-lain.show', 2) }}">Pilih</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
@section('script')
    <!-- Required datatable js -->
    <script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/jszip/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/pdfmake/pdfmake.min.js') }}"></script>
    <!-- Datatable init js -->
    <script src="{{ URL::asset('/assets/js/pages/datatables.init.js') }}"></script>
    <script>
        function initDatatable(id) {
            $(`#${id}`).DataTable();
        }

        initDatatable('uep')
        initDatatable('spp')
        initDatatable('lain')

    </script>
@endsection
