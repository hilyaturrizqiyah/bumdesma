@extends('layouts.master')

@section('title') {{ $title }} @endsection

@section('css')
    <!-- DataTables -->
    <link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Penerimaan Angsuran @endslot
        @slot('title') Daftar Kelompok @endslot
        @slot('title') Lain-lain @endslot
    @endcomponent

    <div class="row">
        <div class="col-xl-4">
            <div class="card">
                <div class="card-header bg-transparent border-bottom text-uppercase">
                    <div class="flex-grow-1">
                        <div class="text-muted">
                            <h4 class="font-weight-semibold mb-0">Pembayaran</h4>
                            <p class="mb-0"><em>Mohon periksa data sebelum anda klik tombol simpan!</em></p>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="row mb-2">
                                <label class="col-form-label col-lg-4">Tanggal</label>
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <input type="text" name="tanggal" value="{{date('d M, Y')}}" class="form-control" readonly>

                                    </div>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <label class="col-form-label col-lg-4">Nomor Bukti</label>
                                <div class="col-lg-8">
                                    <input id="kode_registrasi" name="kode_registrasi" value="REG-01928" type="text" class="form-control" placeholder="Kode Registrasi" readonly>
                                </div>
                            </div>

                            <div class="row mb-2">
                                <label class="col-form-label col-lg-4">Uraian Transaksi</label>
                                <div class="col-lg-8">
                                    <textarea class="form-control" id="billing-address" rows="3" placeholder="Enter full address" disabled=""></textarea>
                                </div>
                            </div>

                            <div class="row mb-2">
                                <label class="col-form-label col-lg-4">Nama Petugas</label>
                                <div class="col-lg-8">
                                    <input id="jumlah_pengajuan" name="jumlah_pengajuan" value="10000000" type="text" class="form-control" placeholder="Jumlah Pengajuan" readonly>
                                </div>
                            </div>

                            <div class="row mb-2">
                                <label class="col-form-label col-lg-4">Nama Penyetor</label>
                                <div class="col-lg-8">
                                    <input id="pokok" name="pokok" value="800000" type="text" class="form-control" placeholder="Pokok" readonly>
                                </div>
                            </div>

                            <h5 class="mt-5" style="color: #1c6ca1">Rincian Pembayaran</h5>

                            <div class="row mb-2">
                                <label class="col-form-label col-lg-4">Bayar Pokok</label>
                                <div class="col-lg-8">
                                    <input id="pokok" name="pokok" value="800000" type="text" class="form-control" placeholder="Pokok" readonly>
                                </div>
                            </div>

                            <div class="row mb-2">
                                <label class="col-form-label col-lg-4">Bayar Bunga</label>
                                <div class="col-lg-8">
                                    <input id="bunga" name="bunga" value="100000" type="text" class="form-control" placeholder="Bunga" readonly>
                                </div>
                            </div>

                            <div class="row mb-2">
                                <label class="col-form-label col-lg-4">Jumlah</label>
                                <div class="col-lg-8">
                                    <input id="angsuran" name="angsuran" value="500000" type="text" class="form-control" placeholder="Angsuran" readonly>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="card-footer bg-transparent border-top text-end">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" onclick="history.back()"><i class="fa fa-times-circle"></i> Batal</button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-check-circle"></i> Simpan</button>
                </div>
            </div>
        </div>
        <div class="col-xl-8">
            <div class="card">
                <div class="card-body">
                    @if ($message = Session::get('success'))
                        <div class="alert border-0 border-start border-5 border-primary alert-dismissible fade show">
                            <div>{{ $message }}</div>
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-12">

                            <div class="row mb-2">
                                <label class="col-form-label col-lg-4">Kode Pinjaman</label>
                                <div class="col-lg-8">
                                    <input id="kode_registrasi" name="kode_registrasi" value="REG-01928" type="text" class="form-control" placeholder="Kode Pinjaman">
                                </div>
                            </div>

                            <div class="row mb-2">
                                <label class="col-form-label col-lg-4">Nama Kelompok</label>
                                <div class="col-lg-8">
                                    <input id="jumlah_pengajuan" name="jumlah_pengajuan" value="10000000" type="text" class="form-control" placeholder="Jumlah Pengajuan">
                                </div>
                            </div>

                            <div class="row mb-2">
                                <label class="col-form-label col-lg-4">Alamat / Desa</label>
                                <div class="col-lg-8">
                                    <textarea class="form-control" id="billing-address" rows="3" placeholder="Enter full address" disabled=""></textarea>
                                </div>
                            </div>

                        </div>
                    </div>

                    <hr>
                    <div class="row">
                        <div class="col-12">
                            <table class="table mb-0">
                                <thead class="table-light">
                                <tr>
                                    <th width="50%">Uraian</th>
                                    <th>Pokok</th>
                                    <th>Bunga</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody class="table-striped">
                                <tr>
                                    <td>Total Pinjaman Lain-lain</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>Kumulatif Realisasi Pengembalian</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>Saldo Pinjaman</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <!-- Required datatable js -->
    <script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/jszip/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/pdfmake/pdfmake.min.js') }}"></script>
@endsection
