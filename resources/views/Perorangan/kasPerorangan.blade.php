@extends('layouts.master')

@section('title') @lang('translation.Invoice_Detail') @endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Perorangan @endslot
        @slot('title') Kas Perorangan @endslot
    @endcomponent


    <style type="text/css">
        td {
            vertical-align : middle;
            text-align:center;
        }
    </style>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <hr>
                    <div class="row">
                        <div class="col-lg-12">
                            <h4 style="text-align: center;">UNIT PENGELOLA KEGIATAN</h4>
                            <h1 style="text-align: center;"><b>BUKU KAS HARIAN LAINNYA</b></h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <p>
                                KECAMATAN : <b>SINDANG</b>
                                <br>
                                KABUPATEN : MAJALENGKA
                                <br>
                                PROVINSI : JAWA BARAT
                            </p>
                        </div>
                        <div class="col-sm-6 text-sm-end">
                            <address class="mt-2 mt-sm-0">
                                <strong>Periode : 31-03-2022</strong><br>
                            </address>
                        </div>
                    </div>
                    <div class="col-xl-12">
                        <div class="table-responsive">
                            <table class="table table-bordered border-primary mb-0">
                                <tbody>
                                    <tr>
                                        <td rowspan="3">No.</td>
                                        <td rowspan="3">Tanggal Transaksi</td>
                                        <td rowspan="3">Keterangan Transaksi</td>
                                        <td rowspan="3">Bukti Transaksi</td>
                                        <td colspan="4">Pemasukan</td>
                                        <td colspan="4">Pengeluaran</td>
                                        <td rowspan="3">Saldo</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">Penerimaan Setoran</td>
                                        <td rowspan="2">Lain-lain</td>
                                        <td rowspan="2">Tarik Dari Rekening</td>
                                        <td rowspan="2">Setor Ke Rekening</td>
                                        <td rowspan="2">Pinjaman/Perguliran</td>
                                        <td rowspan="2">Penggunaan Jasa/Operasional</td>
                                        <td rowspan="2">Lain-lain</td>
                                    </tr>
                                    <tr>
                                        <td>Pokok</td>
                                        <td>Bunga</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">Saldo Transaksi Kumulatif Tahun Lalu</td>
                                        @for ($i = 0; $i < 9; $i++)
                                            <td>{{ $random }}</td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <td colspan="4">Saldo Transaksi Kumulatif Tahun ini s/d Bulan Lalu</td>
                                        @for ($i = 0; $i < 9; $i++)
                                            <td>{{ $random }}</td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>01-03-2022</td>
                                        <td>Tidak Ada Transaksi</td>
                                        <td>&nbsp;</td>
                                        @for ($i = 0; $i < 9; $i++)
                                            <td>{{ $random }}</td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <td colspan="4">Saldo Transaksi Bulan Ini</td>
                                        @for ($i = 0; $i < 8; $i++)
                                            <td>{{ $random }}</td>
                                        @endfor
                                        <td rowspan="3"> {{$random}} </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">Saldo Transaksi Tahun ini s/d Bulan ini</td>
                                        @for ($i = 0; $i < 8; $i++)
                                            <td>{{ $random }}</td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <td colspan="4">Total Saldo Transaksi Kumulatif</td>
                                        @for ($i = 0; $i < 8; $i++)
                                            <td>{{ $random }}</td>
                                        @endfor
                                    </tr>
                                </tbody>
                            </table>
                            <table width="100%"style="padding-top: 25px; margin-top: 10px; align:right; border:none ; margin-left:100px;">
                                <tr>
                                    <td></td>
                                    <td style="text-align: center;">
                                        <br><br>
                                        UPK KECAMATAN SINDANG
                                        <br>
                                        KETUA
                                        <br><br><br><br><br><br>
                                        <b>( IMA ROHIMA AR, ST )</b>
                                    </td>
                                    <td></td>
                                    <td style="text-align: center;">
                                        <br><br>
                                        UPK KECAMATAN SINDANG
                                        <br>
                                        KETUA
                                        <br><br><br><br><br><br>
                                        <b>( IMA ROHIMA AR, ST )</b>
                                    </td>
                                    <td></td>
                                    <td style="text-align: center;">
                                        SINDANG, 31-03-2022
                                        <br><br>
                                        UPK KECAMATAN SINDANG
                                        <br>
                                        KETUA
                                        <br><br><br><br><br><br>
                                        <b>( IMA ROHIMA AR, ST )</b>
                                    </td>
                                </tr>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->

@endsection
