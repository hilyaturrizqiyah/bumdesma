<!DOCTYPE html>
<html>
<head>
	<title>BUKU BANK PENGEMBALIAN UEP</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<style type="text/css">
        table.table1 {
		float:right;
		border:0;
        }
        table.table1 td {
		border:0;
		width:230px;
        }

        table.table2  th, td {
        border:1px solid black;
        padding:5px;
		font-size:12px;
		text-align:center;"
        }

        .grow { width: 100%; }

        table.table3 td{
            border:1px solid black;
			border:0;
			text-align:left;"
        }
		p{
			font-size:10px;
		}
        
    </style>

	<center>
		<h5>UNIT PENGELOLA KEGIATAN</h5>
		<h5>BUKU BANK PENGEMBALIAN UEP</h5>
	</center>
	<table class="table3" >
	<tr>
		<td>KECAMATAN </td>
		<td> : SINDANG</td>
		<td width="800px" style="text-align:right;"> PERIODE : 31-12-2018</tr>
	</tr>
	<tr>
		<td>KABUPATEN</td>
		<td> : MAJALENGKA</td>
		<tr>
		<td>PROVINSI</td>
		<td> : JAWA BARAT</td>
		</tr>
	</table>
	<br>
	<table class='table2'>
		<thead>
			<tr>
				<th rowspan="2" width="15px">No</th>
				<th rowspan="2" width="75px">Tanggal Transaksi</th>
				<th rowspan="2" width="250px">Keterangan Transaksi</th>
				<th rowspan="2" width="50px">Bukti Transaksi</th>
				<th colspan="2" width="100px">Pemasukkan </th>
				<th colspan="3" width="150px">Pengeluaran</th>
				<th rowspan="2" width="75px">Saldo </th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td width="90px">Setoran dari Kas</td>
				<td width="90px">Bunga Bank</td>
				<td width="90px">Tarik dari rekening</td>
				<td width="90px">Pajak</td>
				<td width="90px">Biaya Adm</td>
			</tr>
			<tr>
				<td colspan="4">Saldo Transaksi Kumulatif Tahun Lalu</td>
				<td>2,206,274,650</td>
				<td>5,762,731</td>
				<td>2,135,404,075</td>
				<td>1,144,664</td>
				<td>522,000</td>
				<td>74,966,642</td>
			</tr>
			<tr>
				<td colspan="4">Saldo Transaksi Kumulatif Tahun ini s/d Bulan Lalu</td>
				<td>275,617,300</td>
				<td> 488,342</td>
				<td> 300,000,000</td>
				<td>97,669</td>
				<td> 55,000</td>
				<td> 50,919,615</td>
			</tr>
			<tr>
				<td>1</td>
				<td>03-12-2018</td>
				<td>BUNGA BANK</td>
				<td>1221812001</td>
				<td>0</td>
				<td> 22,132</td>
				<td>0</td>
				<td>0</td>
				<td>0</td>
				<td> 50,941,747</td>
			</tr>
			<tr>
				<td colspan="4">Saldo Transaksi Bulan Ini</td>
				<td> 2,200,000</td>
				<td> 22,132</td>
				<td> 10,000,000</td>
				<td> 9,926</td>
				<td>0</td>
				<td rowspan="3"> 43,131,821</td>
			</tr>
			<tr>
				<td colspan="4">Saldo Transaksi Tahun ini s/d Bulan ini</td>
				<td>277,817,300</td>
				<td> 510,474</td>
				<td> 310,000,000</td>
				<td>107,595</td>
				<td> 55,000</td>
			</tr>
			<tr>
				<td colspan="4">Total Saldo Transaksi Kumulatif</td>
				<td>< 2,484,091,950</td>
				<td>  6,273,205</td>
				<td> 2,445,404,075</td>
				<td> 1,252,259</td>
				<td> 577,000</td>
			</tr>
		</tbody>
	</table>
<br>
	<table class="table1">
		<tr>
			<td></td>
			<td></td>
			<td>SINDANG, 31-12-2018</td>
		</tr>
		<tr>
			<td>Diperiksa oleh:</td>
			<td>Diketahui dan disetujui oleh:</td>
			<td>Dibuat oleh:</td>
		</tr>
		<br>
		<br>
		<tr>
			<td><b>SUTIAH, A.Md</b></td>
			<td><b>IMA ROHIMA AR, ST</b></td>
			<td><b>ADE SUTINI, S.IP</b></td>
		</tr>
		<tr>
			<td>BP-UPK</td>
			<td>Ketua UPK</td>
			<td>Bendahara UPK</td>
		</tr>
	</table>
</body>
</html>