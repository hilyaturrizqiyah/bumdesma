<!DOCTYPE html>
<html>
<head>
	<title>BUKU BANK PENGEMBALIAN UEP</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<style type="text/css">
        table.table1 {
		float:right;
		border:0;
        }
        table.table1 td {
		border:0;
		width:230px;
        }

        table.table2  th, td {
        border:1px solid black;
        padding:5px;
		font-size:9px;
		text-align:center;"
        }

        .grow { width: 100%; }

        table.table3 td{
            border:1px solid black;
			border:0;
			text-align:left;"
        }
		p{
			font-size:10px;
		}
        
    </style>

	<center>
		<h5>UNIT PENGELOLA KEGIATAN</h5>
		<h5>BUKU KAS HARIAN UEP</h5>
	</center>
	<table class="table3" >
	<tr>
		<td>KECAMATAN </td>
		<td> : SINDANG</td>
		<td width="800px" style="text-align:right;"> PERIODE : 31-12-2018</tr>
	</tr>
	<tr>
		<td>KABUPATEN</td>
		<td> : MAJALENGKA</td>
		<tr>
		<td>PROVINSI</td>
		<td> : JAWA BARAT</td>
		</tr>
	</table>
	<br>
	<table class='table2'>
		<thead>
			<tr>
				<th rowspan="3" width="15px">No</th>
				<th rowspan="3" width="50px">Tanggal Transaksi</th>
				<th rowspan="3" width="200px">Keterangan Transaksi</th>
				<th rowspan="3" width="50px">Bukti Transaksi</th>
				<th colspan="4" width="200px">Pemasukkan </th>
				<th colspan="4" width="200px">Pengeluaran</th>
				<th rowspan="3" width="100px">Saldo </th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td colspan="3" width="150px">Setoran dari Desa</td>
				<td rowspan="2" width="50px">Tarik dari Rekening</td>
				<td rowspan="2" width="50px">Setor ke Rekening</td>
				<td rowspan="2" width="50px">Pinjaman/Perguliran</td>
				<td rowspan="2" width="50px">Penggunaan Jasa/Operasional</td>
				<td rowspan="2" width="50px">Lain-lain</td>
			</tr>
			<tr>
				<td width="50px">Pokok</td>
				<td width="50px">Bunga</td>
				<td width="50px">Lain-lain</td>
			</tr>
			<tr>
				<td colspan="4">Saldo Kumulatif Transaksi Kumulatif Tahun Lalu</td>
				<td>1,837,694,650</td>
				<td>362,482,750</td>
				<td>175,917,833</td>
				<td> 2,135,404,075</td>
				<td> 2,206,274,650</td>
				<td>2,205,466,933</td>
				<td> 54,395,225</td>
				<td> 45,362,500</td>
				<td>0</td>
			</tr>
			<tr>
				<td colspan="4">Saldo Transaksi Kumulatif Tahun ini s/d Bulan Lalu</td>
				<td>234,960,500</td>
				<td>40,656,800</td>
				<td>90,000,000</td>
				<td>300,000,000</td>
				<td>275,617,300</td>
				<td>183,000,000</td>
				<td>67,000,000</td>
				<td>140,000,000</td>
				<td>0</td>
				
			</tr>
			<tr>
				<td>1</td>
				<td>03-12-2018</td>
				<td>Angsuran Ke-16, Kel. APAH,PASIRAYU</td>
				<td>1121812001</td>
				<td>0</td>
				<td>2,000,000</td>
				<td>0</td>
				<td>0</td>
				<td>0</td>
				<td>0</td>
				<td>0</td>
				<td>0</td>
				<td>2,000,000</td>
			</tr>
			<tr>
				<td>2</td>
				<td>03-12-2018</td>
				<td>Angsuran Ke-16, Kel. APAH,PASIRAYU</td>
				<td>1121812001</td>
				<td>200,000</td>
				<td>0</td>
				<td>0</td>
				<td>0</td>
				<td>0</td>
				<td>0</td>
				<td>0</td>
				<td>0</td>
				<td>2,200,000</td>
			</tr>
			
			<tr>
				<td colspan="4">Saldo Transaksi Bulan Ini</td>
				<td>3,616,80</td>
				<td>2,983,200</td>
				<td>0</td>
				<td>2,983,200</td>
				<td>2,200,000</td>
				<td>0</td>
				<td>0</td>
				<td>10,000,000</td>
				<td rowspan="3">4,400,000</td>
			</tr>
			<tr>
				<td colspan="4">Saldo Transaksi Tahun ini s/d Bulan ini</td>
				<td>238,577,300</td>
				<td>43,640,000</td>
				<td>90,000,000</td>
				<td>310,000,000</td>
				<td>277,817,300</td>
				<td>183,000,000</td>
				<td>67,000,000</td>
				<td>150,000,000</td>
			</tr>
			<tr>
				<td colspan="4">Total Saldo Transaksi Kumulatif</td>
				<td>2,076,271,950</td>
				<td> 406,122,750</td>
				<td> 265,917,833</td>
				<td> 2,445,404,075</td>
				<td> 2,484,091,950</td>
				<td>2,388,466,933</td>
				<td>121,395,225</td>
				<td>195,362,500</td>
			</tr>
			<tr>
				<td colspan="4">Saldo Transaksi Kumulatif Tahun Lalu</td>
				<td>1,837,694,650</td>
				<td>362,482,750</td>
				<td> 175,917,833</td>
				<td> 2,135,404,075</td>
				<td>2,206,274,650</td>
				<td> 2,205,466,933</td>
				<td> 54,395,225</td>
				<td> 45,362,500</td>
				<td>0</td>
			</tr>
			<tr>
				<td colspan="4">Saldo Transaksi Kumulatif Tahun ini s/d Bulan Lalu</td>
				<td>234,960,500</td>
				<td>40,656,80</td>
				<td>90,000,000</td>
				<td>300,000,000</td>
				<td>275,617,300</td>
				<td>183,000,000</td>
				<td>67,000,000</td>
				<td>140,000,000</td>
				<td>0</td>
			</tr>
		</tbody>
	</table>
<br>
	<table class="table1">
		<tr>
			<td></td>
			<td></td>
			<td>SINDANG, 31-12-2018</td>
		</tr>
		<tr>
			<td>Diperiksa oleh:</td>
			<td>Diketahui dan disetujui oleh:</td>
			<td>Dibuat oleh:</td>
		</tr>
		<br>
		<br>
		<tr>
			<td><b>SUTIAH, A.Md</b></td>
			<td><b>IMA ROHIMA AR, ST</b></td>
			<td><b>ADE SUTINI, S.IP</b></td>
		</tr>
		<tr>
			<td>BP-UPK</td>
			<td>Ketua UPK</td>
			<td>Bendahara UPK</td>
		</tr>
	</table>
</body>
</html>