@extends('layouts.master')

@section('title')
    {{ $title }}
@endsection

@section('css')
    <!-- DataTables -->
    <link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    @component('components.breadcrumb')
        @slot('li_1')
            Kelompok
        @endslot
        @slot('title')
            Daftar Kelompok UEP
        @endslot
    @endcomponent


    <style type="text/css">
        table.table1 {
            width: 100%;
            border: 1px solid black;
            padding: 5px;
        }

        table.table1 th {
            border: 1px solid black;
            padding: 5px;
            vertical-align: middle;
            font-weight: normal
        }

        table.table1 td {
            border-right: 1px solid black;
            border-bottom: 0px;
            padding: 5px;
            vertical-align: middle;
            font-weight: normal;
        }

        .tright {
            text-align: right;
        }

    </style>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <hr>
                    <div class="row">
                        <div class="col-lg-12">
                            <h4 style="text-align: center;">LAPORAN KELOMPOK</h4>
                            <h1 style="text-align: center;"><b>DAFTAR KELOMPOK UEP</b></h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <p>
                                KECAMATAN : <b>SINDANG</b>
                                <br>
                                KABUPATEN : <b>MAJALENGKA</b>
                                <br>
                                PROVINSI : <b>JAWA BARAT</b>
                            </p>
                        </div>
                        <div class="col-sm-6 text-sm-end">
                            <address class="mt-2 mt-sm-0">
                                Periode : <strong>31-03-2022</strong><br>
                            </address>
                        </div>
                    </div>
                    <div class="col-xl-12">
                        <div class="table-responsive">
                            <table class="table1">
                                <tbody>
                                    <tr>
                                        <th class="text-center" style="width:3%; font-weight: bold;">No</th>
                                        <th class="text-center" style="width:3%; font-weight: bold;">Nama Kelompok</th>
                                        <th class="text-center" style="width:3%; font-weight: bold;">Ketua Kelompok</th>
                                        <th class="text-center" style="width:3%; font-weight: bold;">Alamat Kelompok</th>
                                        <th class="text-center" style="width:3%; font-weight: bold;">Kelurahan/Desa</th>
                                        <th class="text-center" style="width:3%; font-weight: bold;">Telepon</th>
                                        <th class="text-center" style="width:3%; font-weight: bold;">Jenis Usaha</th>
                                        <th class="text-center" style="width:3%; font-weight: bold;">Jenis Kegiatan</th>
                                        <th class="text-center" style="width:3%; font-weight: bold;">Jumlah Anggota</th>
                                        <th class="text-center" style="width:3%; font-weight: bold;">Status</th>
                                    </tr>

                                    @php
                                        $i = 1;
                                        $total_anggota = 0;
                                    @endphp

                                    @foreach ($data as $uep)
                                        <tr>
                                            <td class="text-center">{{ $i++ }}</td>
                                            <td class="text-center">{{ $uep->nama_kelompok }}</td>
                                            <td class="text-center">{{ $uep->nama_ketua }}</td>
                                            <td class="text-center">{{ $uep->alamat_kelompok }}</td>
                                            <td class="text-center">{{ namaDesaKelompok($uep->desa) }}</td>
                                            <td class="text-center">{{ $uep->telp_kelompok }}</td>
                                            <td class="text-center">{{ getNameJenisUsaha($uep->jenis_usaha) }}</td>
                                            <td class="text-center">{{ getNameJenisKegiatan($uep->jenis_kegiatan) }}
                                            </td>
                                            <td class="text-center">{{ $uep->anggota_sekarang }}</td>
                                            @if ($uep->status == 0)
                                                @php
                                                    $status = 'aktif';
                                                @endphp
                                            @else
                                                @php
                                                    $status = 'pasif';
                                                @endphp
                                            @endif
                                            <td class="text-center">{{ $status }}</td>
                                        </tr>

                                        @php
                                            $jmlh = $uep->anggota_sekarang;
                                            $total_anggota += $jmlh;
                                        @endphp
                                    @endforeach
                                    <tr>
                                        <th colspan="8" style="padding-left: 45px;"><b> Total Kelompok</b></th>
                                        <th colspan="2" class="tright text-center"><b>{{ $i - 1 }}</b></th>
                                    </tr>
                                    <tr>
                                        <th colspan="8" style="padding-left: 45px;"><b>Total Anggota</b></th>
                                        <th colspan="2" class="tright text-center"><b>{{ $total_anggota }}</b></th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <table width="90%"
                            style="padding-top: 25px;  margin-top: 10px; align:right; border:none ; margin-left:100px;">
                            <tr>
                                <td style="text-align: center;">
                                    <br><br>
                                    Diperiksa Oleh:
                                    <br><br><br><br><br><br>
                                    <b>SUTIAH, A.Md</b>
                                    <br>
                                    BP-UPK
                                </td>
                                <td></td>
                                <td style="text-align: center;">
                                    <br><br>
                                    Disetujui oleh:
                                    <br><br><br><br><br><br>
                                    <b>IMA ROHIMA AR, ST</b>
                                    <br>
                                    Ketua UPK
                                </td>
                                <td></td>
                                <td style="text-align: center;">
                                    <br>
                                    SINDANG, 31-03-2022
                                    <br>
                                    Dibuat oleh:
                                    <br><br><br><br><br><br>
                                    <b>ADE SUTINI, S.IP</b>
                                    <br>
                                    Bendahara UPK
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- end row -->
@endsection
@section('script')
    <!-- Required datatable js -->
    <script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/jszip/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/pdfmake/pdfmake.min.js') }}"></script>
    <!-- Datatable init js -->
    <script src="{{ URL::asset('/assets/js/pages/datatables.init.js') }}"></script>
@endsection
