<!DOCTYPE html>
<html>
<head>
	<title>LAPORAN PERKEMBANGAN PINJAMAN UEP</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<style type="text/css">
        table.table1 {
		float:right;
		border:0;
        }
        table.table1 td {
		border:0;
		width:230px;
        }

        table.table2  th, td {
        border:1px solid black;
        padding:5px;
		font-size:10px;
		text-align:center;"
        }

        .grow { width: 100%; }

        table.table3 td{
            border:1px solid black;
			border:0;
			text-align:left;"
        }
		p{
			font-size:10px;
		}
        
    </style>

	<center>
		<h5>UNIT PENGELOLA KEGIATAN</h5>
		<h5>LAPORAN PERKEMBANGAN PINJAMAN UEP</h5>
	</center>
	<table class="table3" >
	<tr>
		<td>KECAMATAN </td>
		<td> : SINDANG</td>
		<td width="800px" style="text-align:right;"> PERIODE : 31-12-2018</tr>
	</tr>
	<tr>
		<td>KABUPATEN</td>
		<td> : MAJALENGKA</td>
		<tr>
		<td>PROVINSI</td>
		<td> : JAWA BARAT</td>
		</tr>
	</table>
	<br>
	<table class='table2'>
		<thead>
			<tr>
				<th rowspan="2" width="15px">No</th>
				<th rowspan="2" width="60px">Lokasi/Desa</th>
				<th rowspan="2" width="60px">Alokasi Pinjaman</th>
				<th rowspan="2" width="60px">Jumlah Kelompok</th>
				<th colspan="2" width="120px">Target Pengembalian s/d Bulan Ini</th>
				<th colspan="2" width="120px">Realisasi Pengembalian s/d Bulan Lalu</th>
				<th colspan="2" width="120px">Realisasi Pengembalian Bulan ini</th>
				<th colspan="2" width="120px">Realisasi Pengembalians/d Bulan ini</th>
				<th rowspan="2" width="60px">Saldo Pinjaman </th>
				<th rowspan="2" width="60px">%</th>
				<th colspan="2" width="120px">Tunggakan Pengembalian s/d Bulan ini</th>
			</tr>
			<tr>
				<td>Pokok</td>
				<td>Bunga</td>
				<td>Pokok</td>
				<td>Bunga</td>
				<td>Pokok</td>
				<td>Bunga</td>
				<td>Pokok</td>
				<td>Bunga</td>
				<td>Pokok</td>
				<td>Bunga</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>1</td>
				<td>PASIRAYU</td>
				<td>442,054,033</td>
				<td>39</td>
				<td>442,054,033</td>
				<td>104,081,467</td>
				<td>336,808,500</td>
				<td>94,539,400</td>
				<td>0</td>
				<td>0</td>
				<td>336,808,500</td>
				<td>94,539,400</td>
				<td>105,245,533</td>
				<td>76.19</td>
				<td>105,245,533</td>
				<td>9,942,100</td>
			</tr>
			<tr>
				<td>2</td>
				<td>SINDANG</td>
				<td>389,048,500</td>
				<td>30</td>
				<td>389,048,500</td>
				<td>73,409,450</td>
				<td>359,188,200</td>
				<td>69,558,450</td>
				<td>0</td>
				<td>0</td>
				<td>359,188,200</td>
				<td>69,558,450</td>
				<td>29,860,300</td>
				<td>92.32</td>
				<td>29,860,300</td>
				<td>3,851,000
</td>
			</tr>
			<tr>
				<td colspan="2">Total</td>
				<td>2,367,997,583</td>
				<td></td>
				<td>2,367,997,583</td>
				<td>436,809,067</td>
				<td> 2,075,771,950</td>
				<td>406,122,750</td>
				<td>0</td>
				<td>0</td>
				<td>2,075,771,950</td>
				<td>406,122,750</td>
				<td>292,225,633</td>
				<td> 88.74</td>
				<td> 292,225,633</td>
				<td>31,086,400</td>
			</tr>
		</tbody>
	</table>
<br>
	<table class="table1">
		<tr>
			<td></td>
			<td></td>
			<td>SINDANG, 31-12-2018</td>
		</tr>
		<tr>
			<td>Diperiksa oleh:</td>
			<td>Diketahui dan disetujui oleh:</td>
			<td>Dibuat oleh:</td>
		</tr>
		<br>
		<br>
		<tr>
			<td><b>SUTIAH, A.Md</b></td>
			<td><b>IMA ROHIMA AR, ST</b></td>
			<td><b>ADE SUTINI, S.IP</b></td>
		</tr>
		<tr>
			<td>BP-UPK</td>
			<td>Ketua UPK</td>
			<td>Bendahara UPK</td>
		</tr>
	</table>
</body>
</html>