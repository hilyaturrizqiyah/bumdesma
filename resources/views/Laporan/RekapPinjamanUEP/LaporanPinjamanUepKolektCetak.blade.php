<!DOCTYPE html>
<html>
<head>
	<title>LAPORAN KOLEKTIBILITAS PINJAMAN UEP</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<style type="text/css">
        table.table1 {
		float:right;
		border:0;
        }
        table.table1 td {
		border:0;
		width:250px;
        }

        table.table2  th, td {
        border:1px solid black;
        padding:5px;
		font-size:10px;
		text-align:center;"
        }

        .grow { width: 100%; }

        table.table3 td{
            border:1px solid black;
			border:0;
			text-align:left;"
        }

		.column {
			float: left;
			width: 50%;
			padding-left:18px;
			}

			/* Clear floats after the columns */
		.row:after {
			content: "";
			display: table;
			clear: both;
			}
    </style>

	<center>
		<h5>UNIT PENGELOLA KEGIATAN</h5>
		<h5>LAPORAN KOLEKTIBILITAS PINJAMAN UEP</h5>
	</center>
	<table class="table3" >
	<tr>
		<td>KECAMATAN </td>
		<td> : SINDANG</td>
		<td width="800px" style="text-align:right;"> PERIODE : 31-12-2018</tr>
	</tr>
	<tr>
		<td>KABUPATEN</td>
		<td> : MAJALENGKA</td>
		<tr>
		<td>PROVINSI</td>
		<td> : JAWA BARAT</td>
		</tr>
	</table>
	<br>
	<table class='table2'>
		<thead>
			<tr>
				<th rowspan="2" width="15px">No</th>
				<th rowspan="2" width="90px">Lokasi/Desa</th>
				<th rowspan="2" width="90px">Saldo Pinjaman</th>
				<th width="150px">Angsuran Pokok Lancar (Tanpa Tunggakan)</th>
				<th width="150px">Angsuran Pokok Menunggak (1 s/d 2 Kali Angsuran)</th>
				<th width="150px">Angsuran Pokok Menunggak (3 s/d 4 Kali Angsuran)</th>
				<th width="150px">Angsuran Pokok Menunggak (5 s/d 6 Kali Angsuran)</th>
				<th width="150px">Angsuran Pokok Menunggak(Lebih dari 6 Kali Angsuran)</th>
			</tr>
			<tr>
				<td>KOLEKTIBILITAS I</td>
				<td>KOLEKTIBILITAS II</td>
				<td>KOLEKTIBILITAS III</td>
				<td>KOLEKTIBILITAS IV</td>
				<td>KOLEKTIBILITAS V</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>1</td>
				<td>PASIRAYU</td>
				<td> 105,245,533</td>
				<td>0</td>
				<td>0</td>
				<td>0</td>
				<td>0</td>
				<td>0</td>
			</tr>
			<tr>
				<td>2</td>
				<td>SINDANG</td>
				<td> 29,860,300</td>
				<td>0</td>
				<td>0</td>
				<td>0</td>
				<td>0</td>
				<td>0</td>
			</tr>
			<tr>
				<td colspan="2">Total</td>
				<td>292,225,633</td>
				<td>0</td>
				<td>0</td>
				<td>0</td>
				<td>0</td>
				<td>0</td>
			</tr>
		</tbody>
	</table>
	<br>
	<div class="row">
  		<div class="column">
			<table class="table2">
				<tbody>
					<tr>
						<td width="90">TINGKAT KOLEKTIBILITAS</td>
						<td width="90">CADANGAN RESIKO PENGHAPUSAN</td>
						<td width="90">JUMLAH PINJAMAN</td>
						<td width="90">JUMLAH CADANGAN PENGHAPUSAN</td>
					</tr>
					<tr>
						<td width="90">KOLEKTIBILITAS I</td>
						<td width="90">1 %</td>
						<td width="90">0</td>
						<td width="90">0</td>
					</tr>
					<tr>
						<td width="90">KOLEKTIBILITAS II</td>
						<td width="90">10 %</td>
						<td width="90">0</td>
						<td width="90">0</td>
					</tr>
					<tr>
						<td width="90">KOLEKTIBILITAS III</td>
						<td width="90">25 %</td>
						<td width="90">0</td>
						<td width="90">0</td>
					</tr>
					<tr>
						<td width="90">KOLEKTIBILITAS IV</td>
						<td width="90">50 %</td>
						<td width="90">0</td>
						<td width="90">0</td>
					</tr>
					<tr>
						<td width="90">KOLEKTIBILITAS V</td>
						<td width="90">100 %</td>
						<td width="90">0</td>
						<td width="90">0</td>
					</tr>
					<tr>
						<td width="90" style="border:0px;"></td>
						<td width="90">TOTAL</td>
						<td width="90">0</td>
						<td width="90">0</td>
					</tr>
					<tr>
						<td width="90" style="border:0px;"></td>
						<td colspan="2" width="90">RASIO CADANGAN PENGHAPUSAN</td>
						<td width="90">0.00%</td>
					</tr>
					
				</tbody>
			</table>
		</div>
		<div class="column">
			<table class="table1">
				<tr>
					<td></td>
					<td>SINDANG, 31-12-2018</td>
				</tr>
				<tr>
					<td>Disetujui oleh:</td>
					<td>Dibuat oleh:</td>
				</tr>
				<br>
				<tr>
					<td><b>IMA ROHIMA AR, ST</b></td>
					<td><b>ADE SUTINI, S.IP</b></td>
				</tr>
				<tr>
					<td>Ketua UPK</td>
					<td>Bendahara UPK</td>
				</tr>
				<tr>
					<td></td>
					<td>Diperiksa Oleh:</td>
				</tr>
				<br>
				<tr>
					<td></td>
					<td><b>SUTIAH, A.Md<b></td>
				</tr>
				<tr>
					<td></td>
					<td>BP-UPK</td>
				</tr>
			</table>
		</div>
	</div>

</body>
</html>