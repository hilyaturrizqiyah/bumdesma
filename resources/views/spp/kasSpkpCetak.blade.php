<!DOCTYPE html>
<html>
<head>
	<title>BUKU KAS HARIAN SPKP</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<style type="text/css">
        table.table1 {
		float:right;
		border:0;
        }
        table.table1 td {
		border:0;
		width:230px;
        }

        table.table2  th, td {
        border:1px solid black;
        padding:5px;
		font-size:10px;
		text-align:center;"
        }

        .grow { width: 100%; }

        table.table3 td{
            border:1px solid black;
			border:0;
			text-align:left;"
        }
		p{
			font-size:10px;
		}
        
    </style>

	<center>
		<h6 style="font-weight:normal;">UNIT PENGELOLA KEGIATAN</h6>
		<h4>BUKU KAS HARIAN SPKP</h4>
	</center>
	<table class="table3" >
	<tr>
		<td>KECAMATAN </td>
		<td><b> : SINDANG</b></td>
		<td width="800px" style="text-align:right;"> PERIODE <b>: 31-12-2018</b></tr>
	</tr>
	<tr>
		<td>KABUPATEN</td>
		<td> : MAJALENGKA</td>
		<tr>
		<td>PROVINSI</td>
		<td> : JAWA BARAT</td>
		</tr>
	</table>
	<br>
	<table class='table2'>
		<thead>
			<tr>
				<th rowspan="3" width="15px">No</th>
				<th rowspan="3" width="75px">Tanggal Transaksi</th>
				<th rowspan="3" width="150px">Keterangan Transaksi</th>
				<th rowspan="3" width="50px">Bukti Transaksi</th>
				<th colspan="4" width="100px">Pemasukkan </th>
				<th colspan="4" width="150px">Pengeluaran</th>
				<th rowspan="3" width="75px">Saldo </th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td width="70px" colspan="3">Setoran dari Desa</td>
				<td width="70px" rowspan="2">Tarik Dari Rekening</td>
				<td width="70px" rowspan="2">Setor Ke Rekening</td>
				<td width="70px" rowspan="2">Pinjaman/<br>Perguliran</td>
				<td width="70px" rowspan="2">Penggunaan Jasa/Operasional</td>
				<td width="70px" rowspan="2">Lain-lain</td>
			</tr>
			<tr>
				<td>Pokok</td>
				<td>Bunga</td>
				<td>Lain-lain</td>
			</tr>
			<tr>
				<td colspan="4">Saldo Transaksi Kumulatif Tahun Lalu</td>
				@for ($i = 0; $i < 9; $i++)
					<td>{{ $random }}</td>
				@endfor
			</tr>
			<tr>
				<td colspan="4">Saldo Transaksi Kumulatif Tahun ini s/d Bulan Lalu</td>
				@for ($i = 0; $i < 9; $i++)
					<td>{{ $random }}</td>
				@endfor
			</tr>
			<tr>
				<td>1</td>
				<td>03-12-2018</td>
				<td>Tidak Ada Transaksi</td>
				<td></td>
				@for ($i = 0; $i < 9; $i++)
					<td>{{ $random }}</td>
				@endfor
			</tr>
			<tr>
				<td colspan="4">Saldo Transaksi Bulan Ini</td>
				@for ($i = 0; $i < 8; $i++)
					<td>{{ $random }}</td>
				@endfor
				<td rowspan="3">{{ $random }}</td>
			</tr>
			<tr>
				<td colspan="4">Saldo Transaksi Tahun ini s/d Bulan ini</td>
				@for ($i = 0; $i < 8; $i++)
					<td>{{ $random }}</td>
				@endfor
			</tr>
			<tr>
				<td colspan="4">Total Saldo Transaksi Kumulatif</td>
				@for ($i = 0; $i < 8; $i++)
					<td>{{ $random }}</td>
				@endfor
			</tr>	
		</tbody>
	</table>
<br>
	<table class="table1">
		<tr>
			<td></td>
			<td></td>
			<td>SINDANG, 31-12-2018</td>
		</tr>
		<tr>
			<td>Diperiksa oleh:</td>
			<td>Diketahui dan disetujui oleh:</td>
			<td>Dibuat oleh:</td>
		</tr>
		<br>
		<br>
		<tr>
			<td><b>SUTIAH, A.Md</b></td>
			<td><b>IMA ROHIMA AR, ST</b></td>
			<td><b>ADE SUTINI, S.IP</b></td>
		</tr>
		<tr>
			<td>BP-UPK</td>
			<td>Ketua UPK</td>
			<td>Bendahara UPK</td>
		</tr>
	</table>
</body>
</html>