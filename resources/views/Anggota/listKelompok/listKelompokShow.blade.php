@extends('layouts.master')

@section('title') {{ $title }} @endsection

@section('css')
    <!-- DataTables -->
    <link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Keangotaan @endslot
        @slot('title') Daftar Kelompok @endslot
    @endcomponent

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    @if ($message = Session::get('success'))
                    <div class="alert border-0 border-start border-5 border-primary alert-dismissible fade show">
                        <div>{{ $message }}</div>
                    </div>
	                @endif
                    {{-- <h4 class="card-title">Default Datatable</h4>
                    <p class="card-title-desc">DataTables has most features enabled by
                        default, so all you need to do to use it with your own tables is to call
                        the construction function: <code>$().DataTable();</code>.
                    </p> --}}
                    <p>


                        <a class="btn btn-primary btn-sm" href="{{ route('kelompok-list.create') }}">Tambah</a>
                    </p>
                    <table id="datatable" class="table table-bordered dt-responsive  nowrap w-100">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kelompok</th>
                                <th>Kelurahan/Desa</th>
                                <th>Ketua</th>
                                <th>Telepon</th>
                                <th>Status</th>
                                <th>Jenis</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        @php
                            $i = 1;
                        @endphp
                        <tbody>
                            @foreach ($data as $data )
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $data->nama_kelompok }}</td>
                                <td>{{ namaDesaKelompok($data->desa) }}</td>
                                <td>{{ $data->nama_ketua }}</td>
                                <td>{{ $data->telp_kelompok }}</td>
                                <td>{{  getStatus($data->status) }}</td>
                                <td>{{  getJenisKelompok($data->jenis_kelompok) }}</td>
                                <td>{{ $data->keterangan }}</td>
                                <td>
                                    <form action="{{ route('kelompok-list.destroy',$data->id) }}" method="POST">
                                    <a class="btn btn-outline-primary btn-sm" href="{{ route('kelompok-list.show',$data->id) }}">View</a>
                                    <a class="btn btn-outline-primary btn-sm" href="{{ route('kelompok-list.edit',$data->id) }}">Edit</a>
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-outline-danger btn-sm">Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->



@endsection
@section('script')
    <!-- Required datatable js -->
    <script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/jszip/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/pdfmake/pdfmake.min.js') }}"></script>
    <!-- Datatable init js -->
    <script src="{{ URL::asset('/assets/js/pages/datatables.init.js') }}"></script>
@endsection
