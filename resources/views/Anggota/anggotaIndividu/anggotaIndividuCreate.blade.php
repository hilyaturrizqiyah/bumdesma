@extends('layouts.master')

@section('title'){{$title}} @endsection

@section('css')
<!-- select2 css -->
<link href="{{ url('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{ URL::asset('/assets/libs/datepicker/datepicker.min.css') }}">
<link href="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">

<!-- dropzone css -->
<link href="{{ url('assets/libs/dropzone/dropzone.min.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('content')

@component('components.breadcrumb')
@slot('li_1') Keanggotaan @endslot
@slot('title') Tambah Individu @endslot
@endcomponent

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <form action="{{ route('individu-anggota.store') }}" method="post" class="custom-validation">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="jenisAnggota">Jenis Anggota</label>
                                <select name="tipeAnggota" class="form-control select2">
                                    <option value="" selected disabled>-- Pilih Jenis Anggota --</option>
                                    <option value="1">SPKP</option>
                                    <option value="2">UEP</option>
                                    <option value="3">Perorangan</option>
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="tanggalMasuk">Tanggal Masuk</label>
                                <div class="input-group" id="datepicker2">
                                    <input type="text" class="form-control" placeholder=""
                                        data-date-format="yyyy-mm-dd" data-date-container='#datepicker2'
                                        data-provide="datepicker" data-date-autoclose="true" name="tanggalMasuk">
                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label for="keterangan">Kode Registrasi</label>
                                <input type="text" name="kodeRegistrasi" class="form-control" readonly>
                            </div>
                            <div class="mb-3">
                                <label for="keterangan">Nomor Kartu Keluarga</label>
                                <input type="text" name="noKK" class="form-control" required>
                            </div>
                            <div class="mb-3">
                                <label for="keterangan">Nomor NIK</label>
                                <input type="text" name="nik" class="form-control" required>
                            </div>
                            <div class="row">
                            <div class="col-md-8">
                                <label for="keterangan">Nama Lengkap</label>
                                <input type="text" name="namaLengkap" class="form-control" required>
                            </div>
                            <div class="col-md-4">
                                <label for="jenisKelamin">Jenis Kelamin</label>
                                <select name="jenisKelamin" class="form-control select2">
                                    <option value="" selected disabled>-- Pilih --</option>
                                    <option value="1">Laki-Laki</option>
                                    <option value="2">Perempuan</option>
                                </select>
                            </div>
                            </div>
                            <br>
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label for="tempatLahir" class="form-label">Tempat Lahir</label>
                                        <input type="text" class="form-control" name="tempatLahir">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="tanggalLahir">Tanggal Lahir</label>
                                    <div class="input-group" id="datepicker2">
                                        <input type="text" class="form-control" placeholder=""
                                            data-date-format="yyyy-mm-dd" data-date-container='#datepicker2'
                                            data-provide="datepicker" data-date-autoclose="true" name="tanggalLahir">
                                        <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label for="keterangan">Alamat</label>
                                <input type="text" name="alamat" class="form-control" required>
                            </div>
                            <div class="row">
                            <div class="col-md-6">
                                <label for="keterangan">RT</label>
                                <input type="text" name="rt" class="form-control" required>
                            </div>
                            <div class="col-md-6">
                                <label for="keterangan">RW</label>
                                <input type="text" name="rw" class="form-control" required>
                            </div>
                            </div>
                            <br>
                            <div class="mb-3">
                                <label for="desa">Kelurahan/Desa</label>
                                <select name="desa" class="form-control select2">
                                    <option value="" selected disabled>--- Pilih Desa ---</option>
                                    @foreach ($desa as $desa)
                                    <option value="{{ $desa->kode }}">{{ $desa->nama_desa }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="keterangan">Kecamatan</label>
                                <input type="text" name="kecamatan" class="form-control" required>
                            </div>
                            <div class="mb-3">
                                <label for="keterangan">Kabupaten</label>
                                <input type="text" name="kabupaten" class="form-control" required>
                            </div>
                            <div class="mb-3">
                                <label for="keterangan">Provinsi</label>
                                <input type="text" name="propinsi" class="form-control" required>
                            </div>
                            <div class="mb-3">
                                <label for="keterangan">Kode Pos</label>
                                <input type="text" name="kodePos" class="form-control" required>
                            </div>
                            <div class="mb-3">
                                <label for="keterangan">No Telp</label>
                                <input type="text" name="telp" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="keanggotaan">Keanggotaan</label>
                                    <select name="keanggotaan" class="form-control select2">
                                        <option value="" selected disabled>-- Pilih --</option>
                                        <option value="1">Umum</option>
                                        <option value="2">Pengurus</option>
                                        <option value="3">Karyawan</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label for="statusKeanggotaan">Status Keanggotaan</label>
                                    <select name="statusKeanggotaan" class="form-control select2">
                                        <option value="" selected disabled>-- Pilih --</option>
                                        <option value="0">Aktif</option>
                                        <option value="1">Pasif</option>
                                        <option value="2">Registrasi</option>
                                        <option value="3">Verifikasi</option>
                                        <option value="4">Menunggu</option>
                                    </select>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label for="tanggalKeluar">Tanggal Keluar</label>
                                <div class="input-group" id="datepicker2">
                                    <input type="text" class="form-control" placeholder=""
                                        data-date-format="yyyy-mm-dd" data-date-container='#datepicker2'
                                        data-provide="datepicker" data-date-autoclose="true" name="tanggalKeluar">
                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                </div>
                            </div> 
                            <div class="mb-3">
                                <label for="agama">Agama</label>
                                <select name="agama" class="form-control select2">
                                    <option value="" selected disabled>-- Pilih --</option>
                                    <option value="1">Islam</option>
                                    <option value="2">Kristen</option>
                                    <option value="3">Protestan</option>
                                    <option value="4">Hindu</option>
                                    <option value="5">Budha</option>
                                    <option value="6">Konghucu</option>
                                    <option value="7">Lainnya</option>
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="statusPerkawinan">Status Perkawinan</label>
                                <select name="statusPerkawinan" class="form-control select2">
                                    <option value="" selected disabled>-- Pilih --</option>
                                    <option value="1">Kawin</option>
                                    <option value="2">Belum Kawin</option>
                                    <option value="3">Duda</option>
                                    <option value="4">Janda</option>
                                    <option value="5">Lainnya</option>
                                </select>
                            </div> 
                            <div class="mb-3"> 
                                <label for="pekerjaan">Pekerjaan</label>
                                <select name="pekerjaan" class="form-control select2">
                                    <option value="" selected disabled>-- Pilih --</option>
                                    <option value="1">Wiraswasta</option>
                                    <option value="2">Pegawai Swasta</option>
                                    <option value="3">PNS</option>
                                    <option value="4">TNI</option>
                                    <option value="5">Polri</option>
                                    <option value="6">Lainnya</option>
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="keterangan">Jumlah Tabungan</label>
                                <input type="text" name="jumlahTabungan" class="form-control" required>
                            </div>

                            <div class="mb-3">
                                <label for="keterangan">Jumlah Pinjaman</label>
                                <input type="text" name="jumlahPinjaman" class="form-control" required>
                            </div>

                            <div class="mb-3">
                                <label for="jenisUsaha">Jenis Usaha</label>
                                <select name="jenisUsaha" class="form-control select2">
                                    <option value="" selected disabled>-- Pilih --</option>
                                    <option value="1">Pertanian</option>
                                    <option value="2">Perkebunan</option>
                                    <option value="3">Perternakan</option>
                                    <option value="4">Perikanan</option>
                                    <option value="5">Perdagangan</option>
                                    <option value="6">Aneka Industri</option>
                                    <option value="7">Aneka Usaha</option>
                                    <option value="8">Aneka Jasa</option>
                                    <option value="9">Kelautan</option>
                                    <option value="10">Lainnya</option>
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="catatan">Catatan Khusus</label>
                                <textarea name="catatanKhusus" class="form-control"></textarea>
                            </div>
                            <div class="mb-3 mt-2">
                                <label for="manufacturername">Photo</label>
                                <input name="imagename" type="file" class="form-control" placeholder="Choose image"
                                    id="image" accept="image/*" />
                                <p class="mt-2">

                                    <img id="preview-image" src="{{ url('assets/images/upload.png') }}"
                                        alt="preview image" style="max-height: 100px;">
                                </p>

                                <a class="btn btn-danger btn-sm" id="removePreview" style="display: none;">Hapus</a>
                            </div>
                            <div class="d-flex flex-wrap gap-2">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">Save Changes</button>
                                <a type="button" class="btn btn-secondary waves-effect waves-light" href="{{ url('individu-anggota') }}">Cancel</a>
                            </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- end row -->

@endsection
@section('script')


<!-- select 2 plugin -->
<script src="{{ url('assets/libs/select2/select2.min.js') }}"></script>

<!-- dropzone plugin -->
<script src="{{ url('assets/libs/dropzone/dropzone.min.js') }}"></script>

<!-- init js -->
<script src="{{ url('assets/js/pages/ecommerce-select2.init.js') }}"></script>
<script src="{{ URL::asset('/assets/libs/parsleyjs/parsleyjs.min.js') }}"></script>
<script src="{{ URL::asset('/assets/js/pages/form-validation.init.js') }}"></script>
<script src="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ URL::asset('/assets/libs/datepicker/datepicker.min.js') }}"></script>
<script src="{{ URL::asset('/assets/js/pages/form-advanced.init.js') }}"></script>


@endsection