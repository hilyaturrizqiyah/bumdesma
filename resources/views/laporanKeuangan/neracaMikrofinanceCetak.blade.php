<!DOCTYPE html>
<html>
<head>
	<title>NERACA MIKROFINANCE</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<style type="text/css">
        table.table1 {
		float:right;
		border:0;
        text-align: center;
        }
        table.table1 td {
		border:0;
		width:230px;
        font-size:12px;
        text-align: center;
        }

        table.table2 {
        border:1px solid black;
        padding:5px;
		font-size:12px;
		text-align:left;
        }
        table.table2  th{
        border:1px solid black;
        padding:5px;
        text-align:center;
        font-weight: normal;
        }

        table.table2 td{
        
        border-bottom:0px;
        padding:5px;
        vertical-align : middle;
        font-weight:normal;
        }

        .grow { width: 100%; }

        table.table3 td{
            border:1 px solid black;
			border:0;
			text-align:left;
        }

        table.table3 {
            margin-top : 30px;

        }

		p{
			font-size:12px;
            margin-bottom: 0px;
		}

        .tright{
            text-align: right;
        }

        .tleft{
            text-align: left;
        }
        .blank_row
        {
            height: 10px !important; /* overwrites any other rules */
            background-color: #FFFFFF;
        }
        
    </style>

	<center>
		<p style="font-size: 16px">UPK DAPM SAUYUNAN</p>
		<h4>NERACA MIKROFINANCE</h4>
	</center>
	<table class="table3" >
	<tr>
		<td><p>KECAMATAN</p> </td>
		<td><p><b>: SINDANG</b></p></td>
		<td width="500px" style="text-align:right;"><p>PERIODE <b>: 31-12-2018</b></p></tr>
	</tr>
	<tr>
		<td><p>KABUPATEN</p></td>
		<td><p><b> : MAJALENGKA</b></p></td>
		<tr>
		<td><p>PROVINSI</p></td>
		<td><p><b> : JAWA BARAT</b></p></td>
		</tr>
	</table>
	<br>
	<table class='table2'>
        <tr>
            <th colspan="4" width="800px"><b>AKTIVA</b></th>
        </tr>
		<tbody>
            <tr>
                <td width="3%">1.</td>
                <td colspan="3">Harta</td>
            </tr>
            <tr>
                <td>1.1</td>
                <td colspan="3">Kas</td>
            </tr>
            <tr>
                <td>1.1.1</td>
                <td width="40%">Kas Operasional UPK</td>
                <td width="40%" style="text-align:right;">{{ $random }}</td>
                <td width="40%"></td>
            </tr>
            <tr>
                <td>1.1.2</td>
                <td>Kas Pinjaman UEP</td>
                <td style="text-align:right;">{{ $random }}</td>
                <td></td>
            </tr>
            <tr>
                <td>1.1.3</td>
                <td>Kas Pinjaman SPP</td>
                <td style="text-align:right;">{{ $random }}</td>
                <td></td>
            </tr>
            <tr>
                <td>1.1.4</td>
                <td>Kas Pinjaman Perorangan</td>
                <td style="text-align:right;">{{ $random }}</td>
                <td></td>
            </tr>
            <tr>
                <td colspan="3" style="border-top: 1px solid black; border-bottom:1px solid black; padding-left: 40px;">
                    Total Kas
                </td>
                <td style="border-top: 1px solid black; border-bottom:1px solid black; text-align:right;">
                    {{ $random }}
                </td>
            </tr>
            <tr>
                <td>1.2</td>
                <td colspan="3">Bank</td>
            </tr>
            <tr>
                <td>1.2.1</td>
                <td>Bank Operasional UPK</td>
                <td style="text-align:right;">{{ $random }}</td>
                <td></td>
            </tr>
            <tr>
                <td>1.2.2</td>
                <td>Bank Pinjaman UEP</td>
                <td style="text-align:right;">{{ $random }}</td>
                <td></td>
            </tr>
            <tr>
                <td>1.2.3</td>
                <td>Bank Pinjaman SPP</td>
                <td style="text-align:right;">{{ $random }}</td>
                <td></td>
            </tr>
            <tr>
                <td>1.2.4</td>
                <td>Bank Pinjaman Perorangan</td>
                <td style="text-align:right;">{{ $random }}</td>
                <td></td>
            </tr>
            <tr>
                <td colspan="3" style="border-top: 1px solid black; border-bottom:1px solid black; padding-left: 40px;">
                    Total Bank
                </td>
                <td style="border-top: 1px solid black; border-bottom:1px solid black; text-align:right;">
                    {{ $random }}
                </td>
            </tr>
            <tr>
                <td>1.3</td>
                <td colspan="3">Pinjaman</td>
            </tr>
            <tr>
                <td>1.3.1</td>
                <td>Pinjaman UEP</td>
                <td style="text-align:right;">{{ $random }}</td>
                <td></td>
            </tr>
            <tr>
                <td>1.3.2</td>
                <td>Pinjaman SPP</td>
                <td style="text-align:right;">{{ $random }}</td>
                <td></td>
            </tr>
            <tr>
                <td>1.3.3</td>
                <td>Pinjaman Perorangan</td>
                <td style="text-align:right;">{{ $random }}</td>
                <td></td>
            </tr>
            <tr>
                <td>1.3.4</td>
                <td> Pinjaman Lainnya</td>
                <td style="text-align:right;">{{ $random }}</td>
                <td></td>
            </tr>
            <tr>
                <td colspan="3" style="border-top: 1px solid black; border-bottom:1px solid black; padding-left: 40px;">
                    Total Pinjaman
                </td>
                <td style="border-top: 1px solid black; border-bottom:1px solid black; text-align:right;">
                   {{ $random }}
                </td>
            </tr>
            <tr>
                <td>1.4</td>
                <td>Biaya dibayar dimuka</td>
                <td style="text-align:right;">{{ $random }}</td>
                <td></td>
            </tr>
            <tr>
                <td>1.4.1</td>
                <td>Akumulasi Amortisasi</td>
                <td style="text-align:right;">{{ $random }}</td>
                <td></td>
            </tr>
            <tr>
                <td style="border-top: 1px solid black; border-bottom:1px solid black;">1.4.2</td>
                <td colspan="2" style="border-top: 1px solid black; border-bottom:1px solid black;">
                    Nilai Buku
                </td>
                <td style="border-top: 1px solid black; border-bottom:1px solid black; text-align:right;">
                    {{ $random }}
                </td>
            </tr>
            <tr>
                <td>1.5</td>
                <td>Inventaris</td>
                <td style="text-align:right;">{{ $random }}</td>
                <td></td>
            </tr>
            <tr>
                <td>1.5.1</td>
                <td>Akumulasi Penyusutan</td>
                <td style="text-align:right;">{{ $random }}</td>
                <td></td>
            </tr>
            <tr>
                <td style="border-top: 1px solid black; border-bottom:1px solid black;">1.5.2</td>
                <td colspan="2" style="border-top: 1px solid black; border-bottom:1px solid black;">
                    Nilai Buku
                </td>
                <td style="border-top: 1px solid black; border-bottom:1px solid black; text-align:right;">
                    {{ $random }} 
                </td>
            </tr>
            <tr>
                <td style="border-top: 1px solid black;">1.6</td>
                <td  style="border-top: 1px solid black;" colspan="3">Aktiva Tetap</td>
            </tr>
            <tr>
                <td>1.6.1</td>
                <td>Tanah</td>
                <td style="text-align:right;">{{ $random }}</td>
                <td></td>
            </tr>
            <tr>
                <td>1.6.2</td>
                <td>Bangunan Kantor</td>
                <td style="text-align:right;">{{ $random }}</td>
                <td></td>
            </tr>
            <tr>
                <td>1.6.3</td>
                <td>Akumulasi Penyusutan</td>
                <td style="text-align:right;">{{ $random }}</td>
                <td></td>
            </tr>
            <tr>
                <td style="border-top: 1px solid black; border-bottom:1px solid black;">1.6.4</td>
                <td colspan="2" style="border-top: 1px solid black; border-bottom:1px solid black;">
                    Nilai Buku
                </td>
                <td style="border-top: 1px solid black; border-bottom:1px solid black ; text-align:right;">
                    {{ $random }}
                </td>
            </tr>
            <tr>
                <td style="border-top: 1px solid black; border-bottom:1px solid black;">1.7</td>
                <td colspan="2" style="border-top: 1px solid black; border-bottom:1px solid black;">
                    Rupa-rupa Aktiva
                </td>
                <td style="border-top: 1px solid black; border-bottom:1px solid black; text-align:right;">
                    {{ $random }}
                </td>
            </tr>
            <tr class="blank_row">
                <td colspan="4"></td>
            </tr>
            <tr>
                <td colspan="3" style="border-top: 3px solid black; border-bottom:3px solid black;">
                    <b> TOTAL AKTIVA</b>
                </td>
                <td style="border-top: 3px solid black; border-bottom:3px solid black; text-align:right;">
                    <b>{{ $random }}</b>
                </td>
            </tr>
    </table>
	<table class='table2'>
        <tr>
            <th colspan="3" width="800px"><b>PASIVA</b></th>
        </tr>
		<tbody>
            <tr>
                <td width="3%">2.</td>
                <td colspan="2">HUTANG</td>
            </tr>
            <tr>
                <td>2.1</td>
                <td colspan="2">Kelembagaan dan Kelompok</td>
            </tr>
            <tr>
                <td>2.2</td>
                <td width="40%">Dana Sosial RTM</td>
                <td width="40%" style="text-align:right;">{{ $random }}</td>
            </tr>
            <tr>
                <td>2.3</td>
                <td>Bonus Pengurus UPK</td>
                <td style="text-align:right;">{{ $random }}</td>
            </tr>
            <tr>
                <td>2.4</td>
                <td>Pihak Ketiga</td>
                <td style="text-align:right;">{{ $random }}</td>
            </tr>
            <tr>
                <td>2.5</td>
                <td>Lain-lain</td>
                <td style="text-align:right;">{{ $random }}</td>
            </tr>
            <tr>
                <td colspan="2" style="border-top: 1px solid black; border-bottom:1px solid black; padding-left: 40px;">
                    <b>Jumlah Hutang Lancar</b>
                </td>
                <td style="border-top: 1px solid black; border-bottom:1px solid black; text-align:right;">
                    <b>{{ $random }}</b>
                </td>
            </tr>
            <tr>
                <td width="3%">3.</td>
                <td colspan="2">MODAL</td>
            </tr>
            <tr>
                <td>3.1</td>
                <td>Modal BLM UEP</td>
                <td style="text-align:right;">{{ $random }}</td>
            </tr>
            <tr>
                <td>3.2</td>
                <td>Modal BLM SPP</td>
                <td style="text-align:right;">{{ $random }}</td>
            </tr>
            <tr>
                <td>3.3</td>
                <td>Modal Operasional UPK 2%</td>
                <td style="text-align:right;">{{ $random }}</td>
            </tr>
            <tr>
                <td>3.4</td>
                <td>Dana Hibah</td>
                <td style="text-align:right;">{{ $random }}</td>
            </tr>
            <tr>
                <td>1.3</td>
                <td colspan="2">Pinjaman</td>
            </tr>
            <tr>
                <td>3.4.1</td>
                <td>Dana Hibah UEP</td>
                <td style="text-align:right;">{{ $random }}</td>
            </tr>
            <tr>
                <td>3.4.2</td>
                <td>Dana Hibah SPP</td>
                <td style="text-align:right;">{{ $random }}</td>
            </tr>
            <tr>
                <td>3.4.3</td>
                <td>Dana Hibah Lain-lain</td>
                <td style="text-align:right;">{{ $random }}</td>
            </tr>
            <tr>
                <td>3.5</td>
                <td>Penyertaan Modal Pihak Ketiga</td>
                <td style="text-align:right;">{{ $random }}</td>
            </tr>
            <tr>
                <td>3.6</td>
                <td>Surplus/Defisit Ditahan</td>
                <td style="text-align:right;">{{ $random }}</td>
            </tr>
            <tr>
                <td>3.7</td>
                <td>Suprlus/Defisit Tahun Berjalan</td>
                <td style="text-align:right;">{{ $random }}</td>
            </tr>
            <tr>
                <td colspan="2" style="border-top: 1px solid black; border-bottom:1px solid black;">
                    <b>Jumlah Modal</b>
                </td>
                <td style="border-top: 1px solid black; border-bottom:1px solid black; text-align:right;">
                    <b>{{ $random }}</b>
                </td>
            </tr>
            <tr class="blank_row">
                <td colspan="3"></td>
            </tr>
            <tr>
                <td colspan="2" style="border-top: 3px solid black; border-bottom:3px solid black;">
                    <b> TOTAL AKTIVA</b>
                </td>
                <td style="border-top: 3px solid black; border-bottom:3px solid black; text-align:right;">
                    <b>{{ $random }}</b>
                </td>
            </tr>
    </table>
    <table class="table2" width="705x" style="border: 3px solid black;">
        <tr>
            <td>Total Aset Produktif</td>
            <td>2,827,553,892</td>
        </tr>
    </table>
    <br>
	<table class="table1">
		<tr>
			<td></td>
			<td></td>
			<td>SINDANG, 31-12-2018</td>
		</tr>
		<tr>
			<td>Diperiksa oleh:</td>
			<td>Diketahui dan disetujui oleh:</td>
			<td>Dibuat oleh:</td>
		</tr>
		<br>
		<br>
		<tr>
			<td><b>SUTIAH, A.Md</b></td>
			<td><b>IMA ROHIMA AR, ST</b></td>
			<td><b>ADE SUTINI, S.IP</b></td>
		</tr>
		<tr>
			<td>BP-UPK</td>
			<td>Ketua UPK</td>
			<td>Bendahara UPK</td>
		</tr>
	</table>
</body>
</html>