@extends('layouts.master')

@section('title') {{ $title }} @endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Laporan Keuangan @endslot
        @slot('title') Rugi-Laba Mikrofinance @endslot
    @endcomponent


    <style type="text/css">

        table.table1{
        width: 100%;
        border:1px solid black;
        padding:5px;
        }

        table.table1  th{
        border:1px solid black;
        padding:5px;
        vertical-align : middle;
        font-weight:normal
        }

        table.table1  td {
        border-right:1px solid black;
        border-bottom:0px;
        padding:5px;
        vertical-align : middle;
        font-weight:normal;
        }

        .tright{
            text-align: right;
        }

    </style>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <hr>
                    <div class="row">
                        <div class="col-lg-12">
                            <h4 style="text-align: center;">UPK DAPM SAUYUNAN</h4>
                            <h1 style="text-align: center;"><b>RUGI LABA MIKROFINANCE</b></h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <p>
                                KECAMATAN : <b>SINDANG</b>
                                <br>
                                KABUPATEN : <b>MAJALENGKA</b>
                                <br>
                                PROVINSI : <b>JAWA BARAT</b>
                            </p>
                        </div>
                        <div class="col-sm-6 text-sm-end">
                            <address class="mt-2 mt-sm-0">
                                Periode : <strong>31-03-2022</strong><br>
                            </address>
                        </div>
                    </div>
                    <div class="col-xl-12">
                        <div class="table-responsive">
                            <table class="table1">
                                <tbody>
                                    <tr>
                                        <th style="width:3%; font-weight: bold;">4</th>
                                        <th style="width:40%; font-weight: bold;">Pendapatan</th>
                                        <th style="width:20%; font-weight: bold;">S/D Bulan Lalu</th>
                                        <th style="width:20%; font-weight: bold;">Bulan Ini</th>
                                        <th style="width:20%; font-weight: bold;">Kumulatif</th>
                                    </tr>
                                    <tr>
                                        <td> 4.1</td>
                                        <td>Pendapatan Operasional</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>4.1.1</td>
                                        <td>Jasa Pengembalian UEP</td>
                                        @for ($i = 0; $i < 3; $i++)
                                            <td class="tright">{{ $random }}</td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <td>4.1.2</td>
                                        <td>Jasa Pengembalian SPP</td>
                                        @for ($i = 0; $i < 3; $i++)
                                            <td class="tright">{{ $random }}</td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <td>4.1.3</td>
                                        <td>Jasa Pengembalian Lainnya</td>
                                        @for ($i = 0; $i < 3; $i++)
                                            <td class="tright">{{ $random }}</td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <th colspan="2" style="padding-left: 45px; "> Total Pendapatan Operasional</th>
                                        @for ($i = 0; $i < 3; $i++)
                                            <th class="tright">{{ $random }}</th>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <td>4.2</td>
                                        <td>Pendapatan Operasional</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>4.2.1</td>
                                        <td>Jasa Pengembalian UEP</td>
                                        @for ($i = 0; $i < 3; $i++)
                                            <td class="tright">{{ $random }}</td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <td>4.2.2</td>
                                        <td>Jasa Pengembalian SPP</td>
                                        @for ($i = 0; $i < 3; $i++)
                                            <td class="tright">{{ $random }}</td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <td>4.2.3</td>
                                        <td>Jasa Pengembalian Lainnya</td>
                                        @for ($i = 0; $i < 3; $i++)
                                            <td class="tright">{{ $random }}</td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <td>4.2.4</td>
                                        <td>Jasa Pengembalian Lainnya</td>
                                        @for ($i = 0; $i < 3; $i++)
                                            <td class="tright">{{ $random }}</td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <th colspan="2" style="padding-left: 45px;"> Total Pendapatan Operasional</th>
                                        @for ($i = 0; $i < 3; $i++)
                                            <th class="tright">{{ $random }}</th>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <td>4.3</td>
                                        <th>Pendapatan Lain-lain</th>
                                        @for ($i = 0; $i < 3; $i++)
                                            <th class="tright">{{ $random }}</th>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <th colspan="2" style="padding-left: 45px;"><b>Total Pendapatan</b></th>
                                        @for ($i = 0; $i < 3; $i++)
                                            <th class="tright"><b>{{ $random }}</b></th>
                                        @endfor
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table1" style="margin-top:10px;">
                                <tbody>
                                    <tr>
                                        <th style="width:3%;"><b>5</b></th>
                                        <th style="width:40%;"><b>Biaya</b></th>
                                        <th style="width:20%;"></th>
                                        <th style="width:20%;"></th>
                                        <th style="width:20%;"></th>
                                    </tr>
                                    <tr>
                                        <td>5.1</td>
                                        <td>Biaya Operasional</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>5.1.1</td>
                                        <td>Gaji Pengurus</td>
                                        @for ($i = 0; $i < 3; $i++)
                                            <td class="tright">{{ $random }}</td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <td>5.1.2</td>
                                        <td>Administrasi dan Umum</td>
                                        @for ($i = 0; $i < 3; $i++)
                                            <td class="tright">{{ $random }}</td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <td>5.1.3</td>
                                        <td>Trasport, Tunjangan, dll</td>
                                        @for ($i = 0; $i < 3; $i++)
                                            <td class="tright">{{ $random }}</td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <td>5.1.4</td>
                                        <td>Amortisasi</td>
                                        @for ($i = 0; $i < 3; $i++)
                                            <td class="tright">{{ $random }}</td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <td>5.1.5</td>
                                        <td>Penyusutan</td>
                                        @for ($i = 0; $i < 3; $i++)
                                            <td class="tright">{{ $random }}</td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <td>5.1.6</td>
                                        <td>Insentif BP-UPK</td>
                                        @for ($i = 0; $i < 3; $i++)
                                            <td class="tright">{{ $random }}</td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <td>5.1.7</td>
                                        <td>Insentif Tim Verifikasi Perguliran</td>
                                        @for ($i = 0; $i < 3; $i++)
                                            <td class="tright">{{ $random }}</td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <td>5.1.8</td>
                                        <td>Insentif Tim Pendanaan</td>
                                        @for ($i = 0; $i < 3; $i++)
                                            <td class="tright">{{ $random }}</td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <td>5.1.9</td>
                                        <td>Biaya Rapat</td>
                                        @for ($i = 0; $i < 3; $i++)
                                            <td class="tright">{{ $random }}</td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <td>5.1.10</td>
                                        <td>IPTW</td>
                                        @for ($i = 0; $i < 3; $i++)
                                            <td class="tright">{{ $random }}</td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <td>5.1.11</td>
                                        <td>Biaya Lain-lain</td>
                                        @for ($i = 0; $i < 3; $i++)
                                            <td class="tright">{{ $random }}</td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <th colspan="2" style="padding-left: 45px;"> Total Pendapatan Operasional</th>
                                        @for ($i = 0; $i < 3; $i++)
                                            <th class="tright">{{ $random }}</th>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <th>5.2</th>
                                        <th>Biaya Penghapusan Pinjaman</th>
                                        @for ($i = 0; $i < 3; $i++)
                                            <th class="tright">{{ $random }}</th>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <td>5.3</td>
                                        <td>Biaya Non Operasional</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>5.3.1</td>
                                        <td>Pajak Bunga Bank Operasional UPK</td>
                                        @for ($i = 0; $i < 3; $i++)
                                            <td class="tright">{{ $random }}</td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <td>5.3.2</td>
                                        <td>Pajak Bunga Bank Pengembalian UEP</td>
                                        @for ($i = 0; $i < 3; $i++)
                                            <td class="tright">{{ $random }}</td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <td>5.3.3</td>
                                        <td>Pajak Bunga Bank Pengembalian SPP</td>
                                        @for ($i = 0; $i < 3; $i++)
                                            <td class="tright">{{ $random }}</td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <td>5.3.4</td>
                                        <td>Pajak Bunga Bank Pengembalian Lainnya</td>
                                        @for ($i = 0; $i < 3; $i++)
                                            <td class="tright">{{ $random }}</td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <td>5.3.5</td>
                                        <td>Adm Bank Operasional UPK</td>
                                        @for ($i = 0; $i < 3; $i++)
                                            <td class="tright">{{ $random }}</td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <td>5.3.6</td>
                                        <td>Adm Bank Pengembalian UEP</td>
                                        @for ($i = 0; $i < 3; $i++)
                                            <td class="tright">{{ $random }}</td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <td>5.3.7</td>
                                        <td>Adm Bank Pengembalian SPP</td>
                                        @for ($i = 0; $i < 3; $i++)
                                            <td class="tright">{{ $random }}</td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <td>5.3.8</td>
                                        <td>Adm Bank Pengembalian Lainnya</td>
                                        @for ($i = 0; $i < 3; $i++)
                                            <td class="tright">{{ $random }}</td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <td>5.3.9</td>
                                        <td>Biaya Lain-lain Non Operasional</td>
                                        @for ($i = 0; $i < 3; $i++)
                                            <td class="tright">{{ $random }}</td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <th colspan="2" style="padding-left: 45px;"> Total Pendapatan Operasional</th>
                                        @for ($i = 0; $i < 3; $i++)
                                            <th class="tright">{{ $random }}</th>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <th colspan="2" style="padding-left: 45px;"><b> Total Biaya</b></th>
                                        @for ($i = 0; $i < 3; $i++)
                                            <th class="tright"><b>{{ $random }}</b></th>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <th colspan="2" style="padding-left: 45px;"><b>SURPLUS / DEFISIT</b></th>
                                        @for ($i = 0; $i < 3; $i++)
                                            <th class="tright"><b>{{ $random }}</b></th>
                                        @endfor
                                    </tr>
                                </tbody>
                            </table>

                            </div>
                            <table width="90%"style="padding-top: 25px;  margin-top: 10px; align:right; border:none ; margin-left:100px;">
                                <tr>
                                    <td style="text-align: center;">
                                        <br><br>
                                        Diperiksa Oleh:
                                        <br><br><br><br><br><br>
                                        <b>SUTIAH, A.Md</b>
                                        <br>
                                        BP-UPK
                                    </td>
                                    <td></td>
                                    <td style="text-align: center;">
                                        <br><br>
                                        Disetujui oleh:
                                        <br><br><br><br><br><br>
                                        <b>IMA ROHIMA AR, ST</b>
                                        <br>
                                        Ketua UPK
                                    </td>
                                    <td></td>
                                    <td style="text-align: center;">
                                        <br>
                                        SINDANG, 31-03-2022
                                        <br>
                                        Dibuat oleh:
                                        <br><br><br><br><br><br>
                                        <b>ADE SUTINI, S.IP</b>
                                        <br>
                                        Bendahara UPK
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->

@endsection
