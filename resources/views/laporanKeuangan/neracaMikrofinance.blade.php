@extends('layouts.master')

@section('title') {{ $title }} @endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Laporan Keuangan @endslot
        @slot('title') Neraca Mikrofinance @endslot
    @endcomponent

    <style type="text/css">
        table.table1  th, td {
        border:1px solid black;
        border-right:0px;
        padding:5px;
        }

        table.table2  th, td {
        border:1px solid black;
        padding:5px;
        }

        .grow { width: 100%; }

        table.table3 {
            border:1px solid black;
            border-top:none;
        }

        table.table3 td{
            border:none;
            font-size:14px;
        }

    </style>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="invoice-title">
                        <h4 class="float-end font-size-16">Order # 12345</h4>
                        <div class="mb-4">
                            <img src="{{ URL::asset('/assets/images/logo-dark.png') }}" alt="logo" height="20" />
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-6">
                            <address>
                                <strong>Billed To:</strong><br>
                                John Smith<br>
                                1234 Main<br>
                                Apt. 4B<br>
                                Springfield, ST 54321
                            </address>
                        </div>
                        <div class="col-sm-6 text-sm-end">
                            <address class="mt-2 mt-sm-0">
                                <strong>Shipped To:</strong><br>
                                Kenny Rigdon<br>
                                1234 Main<br>
                                Apt. 4B<br>
                                Springfield, ST 54321
                            </address>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 mt-3">
                            <address>
                                <strong>Payment Method:</strong><br>
                                Visa ending **** 4242<br>
                                jsmith@email.com
                            </address>
                        </div>
                        <div class="col-sm-6 mt-3 text-sm-end">
                            <address>
                                <strong>Order Date:</strong><br>
                                October 16, 2019<br><br>
                            </address>
                        </div>
                    </div>
                    <div class="col-xl-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Striped rows</h4>
                                <p class="card-title-desc">Use <code>.table-striped</code> to add zebra-striping to any table row within
                                    the <code>&lt;tbody&gt;</code>.</p>
                                <div class="row">
                                    <table class='table1' style="width:50%" >
                                        <tr>
                                            <th colspan='8' style='text-align: center;'>AKTIVA</th>
                                        </tr>
                                        <tr >
                                            <td style="border-right:0px;">1.</td>
                                            <td class="grow" style="border-left:0px;">HARTA</td>
                                        </tr>

                                    </table>
                                    <table class='table2' style="width:50%" >
                                        <tr>
                                            <th colspan='2' style='text-align: center;'>PASIVA</th>
                                        </tr>
                                        <tr >
                                            <td style="border-right:0px;">2</td>
                                            <td class="grow" style="border-left:0px;">Hutang</td>
                                        </tr>
                                    </table>
                                    <table class='table3' style="width:50%" >
                                        <tr>
                                            <td>1.1</td>
                                            <td>Kas</td>
                                        </tr>
                                        <tr>
                                            <td>1.1.1</td>
                                            <td>Kas Operasional UPK</td>
                                            <td style="text-align:right;">2,937,000</td>
                                        </tr>
                                        <tr>
                                            <td>1.1.2</td>
                                            <td>Kas Pinjaman UEP</td>
                                            <td colspan="2" style="text-align:right;">4,400,000</td>
                                        </tr>
                                        <tr>
                                            <td>1.1.3</td>
                                            <td>Kas Pinjaman SPP</td>
                                            <td style="text-align:right;">15,055,000</td>
                                        </tr>
                                        <tr>
                                            <td>1.1.4</td>
                                            <td>Kas Pinjaman Perorangan</td>
                                            <td style="text-align:right;">9,657,300</td>
                                        </tr>
                                        <tr style="border-top:1px solid;">
                                            <td> - </td>
                                            <td>Total Kas</td>
                                            <td style="text-align:right;">9,657,300</td>
                                        </tr>
                                    </table>
                                    <table class='table3' style="width:50%" >
                                        <tr>
                                            <td>2.1</td>
                                            <td>Kelembagaan</td>
                                        </tr>
                                        <tr>
                                            <td>1.1.1</td>
                                            <td>Kas Operasional UPK</td>
                                            <td>Kas Pinjaman UEP</td>
                                            <td>Kas Pinjaman SPP</td>
                                        </tr>
                                        <tr>
                                            <td>1.1.2</td>
                                            <td>Kas Pinjaman UEP</td>
                                            <td>Kas Pinjaman SPP</td>
                                            <td>Kas Pinjaman Perorangan</td>
                                        </tr>
                                        <tr>
                                            <td>1.1.3</td>
                                            <td>Kas Pinjaman SPP</td>
                                            <td>Kas Pinjaman Perorangan</td>
                                        </tr>
                                        <tr>
                                            <td>1.1.4</td>
                                            <td>Kas Pinjaman Perorangan</td>
                                        </tr>
                                    </table>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->

@endsection
