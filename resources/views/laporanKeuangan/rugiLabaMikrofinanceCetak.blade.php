<!DOCTYPE html>
<html>
<head>
	<title>RUGI LABA MIKROFINANCE</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<style type="text/css">
        table.table1 {
		float:right;
		border:0;
        text-align: center;
        }
        table.table1 td {
		border:0;
		width:230px;
        font-size:12px;
        text-align: center;
        }

        table.table2 {
        border:1px solid black;
        padding:5px;
		font-size:12px;
		text-align:left;
        }
        table.table2  th{
        border:1px solid black;
        padding:5px;
		font-size:12px;
        text-align:center;
        font-weight: normal;
        }

        table.table2 td{
        border-right:1px solid black;
        border-bottom:0px;
        padding:5px;
        vertical-align : middle;
        font-weight:normal;
        }

        .grow { width: 100%; }

        table.table3 td{
            border:1 px solid black;
			border:0;
			text-align:left;
        }

        table.table3 {
            margin-top : 30px;

        }

		p{
			font-size:12px;
            margin-bottom: 0px;
		}

        .tright{
            text-align: right;
        }

        .tleft{
            text-align: left;
        }
        
    </style>

	<center>
		<p style="font-size: 16px">UPK DAPM SAUYUNAN</p>
		<h4>RUGI LABA MIKROFINANCE</h4>
	</center>
	<table class="table3" >
	<tr>
		<td><p>KECAMATAN</p> </td>
		<td><p><b>: SINDANG</b></p></td>
		<td width="500px" style="text-align:right;"><p>PERIODE <b>: 31-12-2018</b></p></tr>
	</tr>
	<tr>
		<td><p>KABUPATEN</p></td>
		<td><p><b> : MAJALENGKA</b></p></td>
		<tr>
		<td><p>PROVINSI</p></td>
		<td><p><b> : JAWA BARAT</b></p></td>
		</tr>
	</table>
	<br>
	<table class='table2'>
			<tr>
				<th width="35px"><b>4</b></th>
				<th width="275px"><b>PENDAPATAN</b></th>
				<th width="100px"><b>S/D BULAN LALU</b></th>
				<th width="100px"><b>BULAN INI</b></th>
				<th width="100px"><b>KUMULATIF</b></th>
			</tr>
		<tbody>
            <tr>
                <td> 4.1</td>
                <td>Pendapatan Operasional</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>4.1.1</td>
                <td>Jasa Pengembalian UEP</td>
                @for ($i = 0; $i < 3; $i++)
                    <td class="tright">{{ $random }}</td>
                @endfor
            </tr>
            <tr>
                <td>4.1.2</td>
                <td>Jasa Pengembalian SPP</td>
                @for ($i = 0; $i < 3; $i++)
                    <td class="tright">{{ $random }}</td>
                @endfor
            </tr>
            <tr>
                <td>4.1.3</td>
                <td>Jasa Pengembalian Lainnya</td>
                @for ($i = 0; $i < 3; $i++)
                    <td class="tright">{{ $random }}</td>
                @endfor
            </tr>
            <tr>
                <th colspan="2" style="text-align: left; padding-left: 50px; "> Total Pendapatan Operasional</th>
                @for ($i = 0; $i < 3; $i++)
                    <th style="text-align:right;">{{ $random }}</th>
                @endfor
            </tr>
            <tr>
                <td>4.2</td>
                <td>Pendapatan Operasional</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>4.2.1</td>
                <td>Jasa Pengembalian UEP</td>
                @for ($i = 0; $i < 3; $i++)
                    <td class="tright">{{ $random }}</td>
                @endfor
            </tr>
            <tr>
                <td>4.2.2</td>
                <td>Jasa Pengembalian SPP</td>
                @for ($i = 0; $i < 3; $i++)
                    <td class="tright">{{ $random }}</td>
                @endfor
            </tr>
            <tr>
                <td>4.2.3</td>
                <td>Jasa Pengembalian Lainnya</td>
                @for ($i = 0; $i < 3; $i++)
                    <td class="tright">{{ $random }}</td>
                @endfor
            </tr>
            <tr>
                <td>4.2.4</td>
                <td>Jasa Pengembalian Lainnya</td>
                @for ($i = 0; $i < 3; $i++)
                    <td class="tright">{{ $random }}</td>
                @endfor
            </tr>
            <tr>
                <th colspan="2" style="text-align: left; padding-left: 50px; "> Total Pendapatan Non Operasional</th>
                @for ($i = 0; $i < 3; $i++)
                    <th style="text-align:right;">{{ $random }}</th>
                @endfor
            </tr>
            <tr>
                <td>4.3</td>
                <th style="text-align: left;">Pendapatan Lain-lain</th>
                @for ($i = 0; $i < 3; $i++)
                    <th style="text-align:right;">{{ $random }}</th>
                @endfor
            </tr>
            <tr>
                <th colspan="2" style="text-align: left; padding-left: 50px; "><b>Total Pendapatan</b></th>
                @for ($i = 0; $i < 3; $i++)
                    <th style="text-align:right;"><b>{{ $random }}</b></th>
                @endfor
            </tr>
		</tbody>
	</table>
    <table class='table2'>
        <tr>
            <th width="35px"><b>5</b></th>
            <th width="275px"><b>Biaya</b></th>
            <th width="100px"></th>
            <th width="100px"></th>
            <th width="100px"></th>
        </tr>
        <tr>
            <td widtd="15px">5.1</td>
            <td widtd="275px">Biaya Operasional</td>
            <td widtd="100px"></td>
            <td widtd="100px"></td>
            <td widtd="100px"></td>
        </tr>
        <tbody>
        <tr>
            <td>5.1.1</td>
            <td>Gaji Pengurus</td>
            @for ($i = 0; $i < 3; $i++)
                <td class="tright">{{ $random }}</td>
            @endfor
        </tr>
        <tr>
            <td>5.1.2</td>
            <td>Administrasi dan Umum</td>
            @for ($i = 0; $i < 3; $i++)
                <td class="tright">{{ $random }}</td>
            @endfor
        </tr>
        <tr>
            <td>5.1.3</td>
            <td>Trasport, Tunjangan, dll</td>
            @for ($i = 0; $i < 3; $i++)
                <td class="tright">{{ $random }}</td>
            @endfor
        </tr>
        <tr>
            <td>5.1.4</td>
            <td>Amortisasi</td>
            @for ($i = 0; $i < 3; $i++)
                <td class="tright">{{ $random }}</td>
            @endfor
        </tr>
        <tr>
            <td>5.1.5</td>
            <td>Penyusutan</td>
            @for ($i = 0; $i < 3; $i++)
                <td class="tright">{{ $random }}</td>
            @endfor
        </tr>
        <tr>
            <td>5.1.6</td>
            <td>Insentif BP-UPK</td>
            @for ($i = 0; $i < 3; $i++)
                <td class="tright">{{ $random }}</td>
            @endfor
        </tr>
        <tr>
            <td>5.1.7</td>
            <td>Insentif Tim Verifikasi Perguliran</td>
            @for ($i = 0; $i < 3; $i++)
                <td class="tright">{{ $random }}</td>
            @endfor
        </tr>
        <tr>
            <td>5.1.8</td>
            <td>Insentif Tim Pendanaan</td>
            @for ($i = 0; $i < 3; $i++)
                <td class="tright">{{ $random }}</td>
            @endfor
        </tr>
        <tr>
            <td>5.1.9</td>
            <td>Biaya Rapat</td>
            @for ($i = 0; $i < 3; $i++)
                <td class="tright">{{ $random }}</td>
            @endfor
        </tr>
        <tr>
            <td>5.1.10</td>
            <td>IPTW</td>
            @for ($i = 0; $i < 3; $i++)
                <td class="tright">{{ $random }}</td>
            @endfor
        </tr>
        <tr>
            <td>5.1.11</td>
            <td>Biaya Lain-lain</td>
            @for ($i = 0; $i < 3; $i++)
                <td class="tright">{{ $random }}</td>
            @endfor
        </tr>
        <tr>
            <th colspan="2" style="text-align: left; padding-left: 50px; "> Total Pendapatan Operasional</th>
            @for ($i = 0; $i < 3; $i++)
                <th style="text-align:right;">{{ $random }}</th>
            @endfor
        </tr>
        <tr>
            <th style="text-align: left;">5.2</th>
            <th style="text-align: left;">Biaya Penghapusan Pinjaman</th>
            @for ($i = 0; $i < 3; $i++)
                <th style="text-align:right;">{{ $random }}</th>
            @endfor
        </tr>
        <tr>
            <td>5.3</td>
            <td>Biaya Non Operasional</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>5.3.1</td>
            <td>Pajak Bunga Bank Operasional UPK</td>
            @for ($i = 0; $i < 3; $i++)
                <td class="tright">{{ $random }}</td>
            @endfor
        </tr>
        <tr>
            <td>5.3.2</td>
            <td>Pajak Bunga Bank Pengembalian UEP</td>
            @for ($i = 0; $i < 3; $i++)
                <td class="tright">{{ $random }}</td>
            @endfor
        </tr>
        <tr>
            <td>5.3.3</td>
            <td>Pajak Bunga Bank Pengembalian SPP</td>
            @for ($i = 0; $i < 3; $i++)
                <td class="tright">{{ $random }}</td>
            @endfor
        </tr>
        <tr>
            <td>5.3.4</td>
            <td>Pajak Bunga Bank Pengembalian Lainnya</td>
            @for ($i = 0; $i < 3; $i++)
                <td class="tright">{{ $random }}</td>
            @endfor
        </tr>
        <tr>
            <td>5.3.5</td>
            <td>Adm Bank Operasional UPK</td>
            @for ($i = 0; $i < 3; $i++)
                <td class="tright">{{ $random }}</td>
            @endfor
        </tr>
        <tr>
            <td>5.3.6</td>
            <td>Adm Bank Pengembalian UEP</td>
            @for ($i = 0; $i < 3; $i++)
                <td class="tright">{{ $random }}</td>
            @endfor
        </tr>
        <tr>
            <td>5.3.7</td>
            <td>Adm Bank Pengembalian SPP</td>
            @for ($i = 0; $i < 3; $i++)
                <td class="tright">{{ $random }}</td>
            @endfor
        </tr>
        <tr>
            <td>5.3.8</td>
            <td>Adm Bank Pengembalian Lainnya</td>
            @for ($i = 0; $i < 3; $i++)
                <td class="tright">{{ $random }}</td>
            @endfor
        </tr>
        <tr>
            <td>5.3.9</td>
            <td>Biaya Lain-lain Non Operasional</td>
            @for ($i = 0; $i < 3; $i++)
                <td class="tright">{{ $random }}</td>
            @endfor
        </tr>
        <tr>
            <th colspan="2" style="text-align: left; padding-left: 50px; "> Total Pendapatan Operasional</th>
            @for ($i = 0; $i < 3; $i++)
                <th style="text-align:right;">{{ $random }}</th>
            @endfor
        </tr>
        <tr>
            <th colspan="2" style="text-align: left; padding-left: 50px; "><b> Total Biaya</b></th>
            @for ($i = 0; $i < 3; $i++)
                <th style="text-align:right;"><b>{{ $random }}</b></th>
            @endfor
        </tr>
        <tr>
            <th colspan="2" style="text-align: left; padding-left: 50px; "><b>SURPLUS / DEFISIT</b></th>
            @for ($i = 0; $i < 3; $i++)
                <th style="text-align:right;"><b>{{ $random }}</b></th>
            @endfor
        </tr>
    </tbody>
    </table>
<br>
	<table class="table1">
		<tr>
			<td></td>
			<td></td>
			<td>SINDANG, 31-12-2018</td>
		</tr>
		<tr>
			<td>Diperiksa oleh:</td>
			<td>Diketahui dan disetujui oleh:</td>
			<td>Dibuat oleh:</td>
		</tr>
		<br>
		<br>
		<tr>
			<td><b>SUTIAH, A.Md</b></td>
			<td><b>IMA ROHIMA AR, ST</b></td>
			<td><b>ADE SUTINI, S.IP</b></td>
		</tr>
		<tr>
			<td>BP-UPK</td>
			<td>Ketua UPK</td>
			<td>Bendahara UPK</td>
		</tr>
	</table>
</body>
</html>