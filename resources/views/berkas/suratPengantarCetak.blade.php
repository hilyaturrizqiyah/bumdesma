<!DOCTYPE html>
<html>
<head>
	<title>SURAT PENGANTAR</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<style type="text/css">

        table {
			border-style: double;
			border-width: 3px;
			border-color: white;
            width: 100%
		}
		table tr .text2 {
			text-align: right;
			font-size: 13px;
		}
		table tr .text {
			text-align: center;
			font-size: 13px;
		}
		table tr td {
			font-size: 13px;
		}

        table.tb {
            border : 1px solid black;
            width:100%;
            margin-top:20px;

        }
        table.tb td{
            border : 1px solid black;
            padding:5px;
            padding-bottom:30px;
        }

        table.tb th{
            border : 1px solid black;
            padding:5px;
            text-align: center;
        }

    </style>
	<center>
        <table>
            <tr>
            <td><img src="" width="90" height="90"></td>
                <td>
                <center>
                    <font size="4"><b>UNIT PENGELOLA KEGIATAN</b></font><br>
                    <font size="4"><b>( UPK )</b></font><br>
                    <font size="4"><b>KECAMATAN SINDANG</b></font><br>
                    <font size="2">KABUPATEN MAJALENGKA PROVINSI JAWA BARAT</font><br>
                    <font size="1"><i>Sekretariat Jalan Jogja Kecil No. 16 Sindang Majalengka 45471, Telp. (0233)-8514126</i></font>
                </center>
                </td>
            </tr>
            <tr>
                <td colspan="2"><hr style="border: 2px solid black; opacity: 1;"></td>
            </tr>
        </table>
        <br>
        <table>
            <tr>
                <center>
                    <font size="5"><b>SURAT PENGANTAR</b></font><br>
                    <font size="2">Nomor: 127361278</font><br>
                </center>
                </td>
            </tr>
        </table>
        <br><br>
        <table width="500px">
            <tr>
            <td>
                <font size="2">Kepada Yth :<br> Lorem ipsum<br>Di Tempat</font>
            </td>
            </tr>
        </table>
        <table class="tb">
            <tr>
                <th style="width : 5%;"><b>NO</b></th>
                <th><b>Perihal</b></th>
                <th><b>Banyaknya</b></th>
                <th><b>Keterangan</b></th>
            </tr>
            <tr>
                <td>1</td>
                <td>Laporan Keuangan UPK Kecamatan SINDANG Periode Maret 2022</td>
                <td>1 (Satu) Berkas</td>
                <td>Dikirim dengan Hormat untuk Perlunya.</td>
            </tr>
        </table>
        <table width="500">
                <tr>
                    <td width="300"><br><br><br><br></td>
                    <td class="text" align="center">
                        <br>
                        SINDANG, 31-03-2022
                        <br><br>
                        UPK KECAMATAN SINDANG
                        <br>
                        KETUA
                        <br><br><br><br>
                        <b>( IMA ROHIMA AR, ST )</b>
                    </td>
                </tr>
        </table>
    </center>
</body>
</html>