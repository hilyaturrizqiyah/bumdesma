<!DOCTYPE html>
<html>
<head>
	<title>PROFIL PENGURUS UPK</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<style type="text/css">
        table.table1 {
		float:right;
		border:0;
        }
        table.table1 td {
		border:0;
		width:230px;
        }

        table.table2  th, td {
        border:1px solid black;
        padding:5px;
		font-size:12px;
		text-align: left;
        }

        .grow { width: 100%; }

        table.table3 td{
			border:0;
			text-align:left;"
        }
		p{
			font-size:16px;
		}
        
    </style>

	<center>
		<h4>PROFIL KELEMBAGAAN UPK</h4>
	</center>
	<table class="table3" >
	<tr>
		<td>KECAMATAN </td>
		<td><b>: SINDANG</b></td>
		<td width="800px" style="text-align:right;">Periode s/d Bulan <b>: Maret 2022</b><br></tr>
	</tr>
	<tr>
		<td>KABUPATEN</td>
		<td> : MAJALENGKA</td>
		<tr>
		<td>PROVINSI</td>
		<td> : JAWA BARAT</td>
		</tr>
	</table>
	<br>
    <table class="table2" style="width: 1000px;">
        <tr>
            <td><b>NO</b></td>
            <td><b>NAMA LENGKAP</b></td>
            <td><b>JABATAN</b></td>
            <td><b>ALAMAT</b></td>
            <td><b>HP</b></td>
        </tr>
        <tr>
            <td colspan="5" style="padding-left:35px;"><b>UPK</b></td>
        </tr>
        <tr>
            <td style="width : 3%;">1</td>
            <td style="width :30%">IMA ROHIMA AR, ST</td>
            <td style="width :10%">KETUA</td>
            <td style="width :40%">DESA GARAWASTU</td>
            <td style="width :20%">0821 2755 3355</td>
        </tr>
        <tr>
            <td>1</td>
            <td>IMA ROHIMA AR, ST</td>
            <td>KETUA</td>
            <td>DESA GARAWASTU</td>
            <td>0821 2755 3355</td>
        </tr>
        <tr>
            <td>1</td>
            <td>IMA ROHIMA AR, ST</td>
            <td>KETUA</td>
            <td>DESA GARAWASTU</td>
            <td>0821 2755 3355</td>
        </tr>
        <tr>
            <td colspan="5" style="padding-left:35px;"><b>BKAD</b></td>
        </tr>
        <tr>
            <td>1</td>
            <td>IMA ROHIMA AR, ST</td>
            <td>KETUA</td>
            <td>DESA GARAWASTU</td>
            <td>0821 2755 3355</td>
        </tr>
        <tr>
            <td>1</td>
            <td>IMA ROHIMA AR, ST</td>
            <td>KETUA</td>
            <td>DESA GARAWASTU</td>
            <td>0821 2755 3355</td>
        </tr>
        <tr>
            <td>1</td>
            <td>IMA ROHIMA AR, ST</td>
            <td>KETUA</td>
            <td>DESA GARAWASTU</td>
            <td>0821 2755 3355</td>
        </tr>
    </table>   
</body>
</html>