<!DOCTYPE html>
<html>
<head>
	<title>DAFTAR ISI</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<style type="text/css">
        table.table1{
        width: 100%;
        border-collapse: collapse;
        margin-left: 70px; 
        margin-right: 100px;
        }
        table.table1  th{
        border:1px solid black;
        padding:5px;
        vertical-align : middle;
        font-weight:normal
        }

        table.table1  td {
        border-bottom:0px;
        padding:5px;
        vertical-align : middle;
        font-weight:normal;
        font-size: 12px;
        }
        
    </style>

	<center>
		<h4>Daftar Isi</h4>
	</center>
	    <table class="table1">
            <tr>
                <td colspan="2">Sampul</td>
            </tr>
            <tr>
                <td colspan="2">Surat Pengantar</td>
            </tr>
            <tr>
                <td colspan="2">Kata Pengantar</td>
            </tr>
            <tr>
                <td colspan="2">Profil Pengurus UPK</td>
            </tr>
        </table>
        <table class="table1">
            <tr>
                <td style="width: 3%;"><b>1.</b></td>
                <td><b>LAPORAN KEUANGAN MIKROFINANCE</b></td>
            </tr>
        </table>
        <table class="table1">
            <tr>
                <td style="width: 3%;">1.1</td>
                <td>Neraca Mikrofinance</td>
            </tr>
            <tr>
                <td>1.2</td>
                <td>Rugi Laba Mikrofinance</td>
            </tr>
        </table>
        <table class="table1">
            <tr>
                <td style="width: 3%;"><b>2.</b></td>
                <td><b>LAPORAN PERKEMBANGAN DANA BERGULIR</b></td>
            </tr>
        </table>
        <table class="table1">
            <tr>
                <td style="width: 3%;"><b>2.1.</b></td>
                <td><b>Simpan Pinjaman Kelompok Perempuan (SPKP)</b></td>
            </tr>
            <tr>
                <td>2.1.1</td>
                <td>Perkembangan Pinjaman</td>
            </tr>
            <tr>
                <td>2.1.2</td>
                <td>Kolektibilitas Pinjaman</td>
            </tr>
            <tr>
                <td>2.1.3 </td>
                <td>Daftar Penghapusan Pinjaman</td>
            </tr>
            <tr>
                <td>2.1.4</td>
                <td>Daftar Pinjaman Lain-lain</td>
            </tr>
            <tr>
                <td><b>2.2.</b></td>
                <td><b>Usaha Ekonomi Produktif (UEP)</b></td>
            </tr>
            <tr>
                <td>2.2.1</td>
                <td>Perkembangan Pinjaman</td>
            </tr>
            <tr>
                <td>2.2.2</td>
                <td>Kolektibilitas Pinjaman</td>
            </tr>
            <tr>
                <td>2.2.3</td>
                <td>Daftar Penghapusan Pinjaman</td>
            </tr>
            <tr>
                <td>2.2.4</td>
                <td>Daftar Pinjaman Lain-lain</td>
            </tr>
            <tr>
                <td><b>2.3. </b></td>
                <td><b>Unit Usaha Baru/Pinjaman Individu/Pembiayaan/Lainnya</b></td>
            </tr>
            <tr>
                <td>2.3.1 </td>
                <td>Perkembangan Pinjaman</td>
            </tr>
            <tr>
                <td>2.3.2</td>
                <td>Kolektibilitas Pinjaman</td>
            </tr>
            <tr>
                <td><b>2.4.</b></td>
                <td><b>Data Perkembangan Kelompok SPKP/UEP/Lainnya</b></td>
            </tr>
            <tr>
                <td>2.4.1</td>
                <td>Daftar Kelompok SPKP</td>
            </tr>
            <tr>
                <td>2.4.2 </td>
                <td>Daftar Kelompok UEP</td>
            </tr>
            <tr>
                <td>2.4.3 </td>
                <td>Perkembangan Kelompok l Form84.a</td>
            </tr>
            <tr>
                <td>2.4.4 </td>
                <td>Perkembangan Jenis Usaha/Kegiatan Kelompok SPKP/UEP l Form84.b</td>
            </tr>
            <tr>
                <td>2.4.5 </td>
                <td>Pinjaman Bermasalah l Form84.c</td>
            </tr>
        </table>
        <table class="table1">
            <tr>
                <td colspan="2"><b>LAMPIRAN-LAMPIRAN</b></td>
            </tr>
            <tr>
                <td style="width: 3%;">1.</td>
                <td>Daftar Biaya Dibayar Dimuka</td>
            </tr>
            <tr>
                <td>2.</td>
                <td>Daftar Inventaris UPK</td>
            </tr>
            <tr>
                <td>3.</td>
                <td>Daftar Aktiva Tetap</td>
            </tr>
            <tr>
                <td>4.</td>
                <td>Buku Kas</td>
            </tr>
            <tr>
                <td>5.</td>
                <td>Buku Bank</td>
            </tr>
            <tr>
                <td>6.</td>
                <td>Lampiran Data Pendukung Lainnya</td>
            </tr>
        </table>
</body>
</html>