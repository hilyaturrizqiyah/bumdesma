@extends('layouts.master')

@section('title') {{ $title }} @endsection

@section('css')
    <!-- DataTables -->
    <link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Daftar Pengguna @endslot
        @slot('title') Pengguna @endslot
    @endcomponent

    <form action="<?//Todo: Replace with {} ?>" method="post" class="custom-validation" enctype="multipart/form-data">
        <!--- Header Card --->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body" style="clear: both">
                        <h2 style="float: left">Saldo Kas</h2>
                        <h4 style="float: right">Rp 100.000</h4>
                    </div>
                </div>
            </div>
        </div>

        <!--- Main Card --->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="mb-3 col-8">
                                <label for="pencarian">Pencarian Data</label>
                                <input id="pencarian" name="pencarian" class="form-control">
                            </div>
                            <div class="mb-3 col-4">
                                <button class="mx-1 mt-4 btn btn-primary">Cari</button>
                                <button class="mx-1 mt-4 btn btn-primary">Browse</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-5">
                                <div class="mb-3 row">
                                    <label for="pinjaman-ke" class="col-md-4 col-form-label">Pinjaman Ke ?</label>
                                    <div class="col-md-8">
                                        <input class="w-50 form-control" type="text"  id="pinjaman-ke">
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label for="pinjaman-ke" class="col-md-4 col-form-label">Kode Pinjaman</label>
                                    <div class="col-md-8">
                                        <input class="form-control" type="text"  id="pinjaman-ke">
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label for="pinjaman-ke" class="col-md-4 col-form-label">Kode Kelompok</label>
                                    <div class="col-md-8">
                                        <input class="form-control" type="text"  id="pinjaman-ke">
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label for="pinjaman-ke" class="col-md-4 col-form-label">Nama Kelompok</label>
                                    <div class="col-md-8">
                                        <input class="form-control" type="text"  id="pinjaman-ke">
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label for="pinjaman-ke" class="col-md-4 col-form-label">Desa</label>
                                    <div class="col-md-8">
                                        <input class="form-control" type="text"  id="pinjaman-ke">
                                    </div>
                                </div>
                            </div>
                            <div class="col-7">
                                <div class="mb-3 row">
                                    <label for="example-text-input" class="col-md-3 col-form-label">Suku Bunga</label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" id="example-text-input">
                                    </div>
                                    <label class="col-md-3 col-form-label"> % Per Tahun</label>
                                </div>
                                <div class="mb-3 row">
                                    <label class="col-3 col-form-label">Jangka Waktu</label>
                                    <div class="col-2">
                                        <input type="text" class="form-control">
                                    </div>
                                    <label class="col-2 col-form-label">Bulan</label>
                                    <div class="col-3">
                                        <input type="text" class="form-control">
                                    </div>
                                    <label class="col-2 col-form-label">Bulanan</label>
                                </div>
                                <div class="mb-3 row">
                                    <label for="example-text-input" class="col-md-3 col-form-label">Angsuran Pokok</label>
                                    <div class="col-md-9">
                                        <input class="form-control" type="text"  id="example-text-input">
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label for="example-text-input" class="col-md-3 col-form-label">Angsuran Bunga</label>
                                    <div class="col-md-9">
                                        <input class="form-control" type="text"  id="example-text-input">
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label for="example-text-input" class="col-md-3 col-form-label">Jumlah Angsuran</label>
                                    <div class="col-md-9">
                                        <input class="form-control" type="text" id="example-text-input">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--- Side Card --->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div>
                            <h3>Pembayaran</h3>
                            <div class="mb-3">Mohon Periksa sebelum verifikasi</div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-md-5 col-form-label">Tanggal</label>
                            <div class="col-7">
                                <input type="date" class="form-control">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-md-5 col-form-label">No Bukti</label>
                            <div class="col-7">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-md-5 col-form-label">Keterangan</label>
                            <div class="col-7">
                                <textarea class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-md-5 col-form-label">Nama Petugas</label>
                            <div class="col-7">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-md-5 col-form-label">Nama Penerima</label>
                            <div class="col-7">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-md-5 col-form-label">Jumlah Pinjaman</label>
                            <div class="col-7">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-md-5 col-form-label">Jumlah Bunga</label>
                            <div class="col-7">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-md-5 col-form-label">Total Pinjaman</label>
                            <div class="col-7">
                                <input type="text" class="form-control">
                            </div>
                        </div>

                        <div class="d-flex flex-wrap gap-2">
                            <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                            <button type="button" onclick="window.history.back()" class="btn btn-secondary waves-effect waves-light">Cancel</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>

@endsection
@section('script')
    <!-- Required datatable js -->
    <script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/jszip/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/pdfmake/pdfmake.min.js') }}"></script>
    <!-- Datatable init js -->
    <script src="{{ URL::asset('/assets/js/pages/datatables.init.js') }}"></script>
@endsection
