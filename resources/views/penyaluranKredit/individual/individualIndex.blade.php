@extends('layouts.master')

@section('title') {{ $title }} @endsection

@section('css')
    <!-- DataTables -->
    <link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Daftar Pengguna @endslot
        @slot('title') Pengguna @endslot
    @endcomponent

    <form action="<?//Todo: Replace with {} ?>" method="post" class="custom-validation" enctype="multipart/form-data">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body" style="clear: both">
                        <h2 style="float: left">Saldo Kas</h2>
                        <h4 style="float: right">Rp 100.000</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="mb-3 col-8">
                                <label for="pencarian">Pencarian Data</label>
                                <input id="pencarian" name="pencarian" class="form-control">
                            </div>
                            <div class="mb-3 col-4">
                                <button class="mx-1 mt-4 btn btn-primary">Cari</button>
                                <button class="mx-1 mt-4 btn btn-primary">Browse</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="mb-2 col-12">
                                Data Pemohon
                            </div>
                            <div class="col-6">
                                <div class="mb-3 row">
                                    <label for="pinjaman-ke" class="col-md-6 col-form-label">NIK</label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="number"  id="pinjaman-ke" placeholder="3201012605020011">
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label for="pinjaman-ke" class="col-md-6 col-form-label">No Kepala Keluarga</label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text"  id="pinjaman-ke">
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label for="pinjaman-ke" class="col-md-6 col-form-label">Jenis Kelamin</label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text"  id="pinjaman-ke">
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-3 row">
                                    <label for="example-text-input" class="col-md-4 col-form-label">Desa</label>
                                    <div class="col-md-8">
                                        <input class="form-control" type="text"  id="example-text-input">
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label for="alamat" class="col-md-4 col-form-label">Alamat</label>
                                    <div class="col-md-8">
                                        <textarea class="form-control" rows="3" id="alamat"></textarea>
                                    </div>
                                </div>
                            </div>

                            <!--- Next Detail --->
                            <div class="mb-2 col-12">
                                Detail Pinjaman
                            </div>

                            <div class="col-6">
                                <div class="mb-3 row">
                                    <label for="pinjaman-ke" class="col-md-6 col-form-label">Tanggal</label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="date"  id="pinjaman-ke">
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label for="pinjaman-ke" class="col-md-6 col-form-label">Kode Proposal</label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text"  id="pinjaman-ke">
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label for="pinjaman-ke" class="col-md-6 col-form-label">Pinjaman</label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text"  id="pinjaman-ke">
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label for="pinjaman-ke" class="col-md-6 col-form-label">Jasa</label>
                                    <div class="col-md-3">
                                        <input class="form-control" type="text"  id="pinjaman-ke">
                                    </div>
                                    <div class="col-md-3">
                                        <input class="form-control" type="text"  id="pinjaman-ke">
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label for="pinjaman-ke" class="col-md-6 col-form-label">Jangka Waktu</label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text"  id="pinjaman-ke">
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label for="pinjaman-ke" class="col-md-6 col-form-label">Kategori Kredit</label>
                                    <div class="col-md-3">
                                        <input class="form-control" type="text"  id="pinjaman-ke">
                                    </div>
                                    <div class="col-md-3">
                                        <input class="form-control" type="text"  id="pinjaman-ke">
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label for="pinjaman-ke" class="col-md-6 col-form-label">Keterangan</label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text"  id="pinjaman-ke">
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-3 row">
                                    <label for="example-text-input" class="col-md-4 col-form-label">Item</label>
                                    <div class="col-md-8">
                                        <input class="form-control" type="text"  id="example-text-input">
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label for="alamat" class="col-md-4 col-form-label">Harga Satuan</label>
                                    <div class="col-md-8">
                                        <input class="form-control" type="text"  id="example-text-input">
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label for="alamat" class="col-md-4 col-form-label">Uang Muka</label>
                                    <div class="col-md-8">
                                        <input class="form-control" type="text"  id="example-text-input">
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label for="alamat" class="col-md-4 col-form-label">Agunan/Jaminan</label>
                                    <div class="col-md-8">
                                        <input class="form-control" type="text"  id="example-text-input">
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label for="alamat" class="col-md-4 col-form-label">Spesifikasi Jaminan</label>
                                    <div class="col-md-8">
                                        <textarea class="form-control" rows="3" id="alamat"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--- Side Card --->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div>
                            <h3>Pembayaran</h3>
                            <div class="mb-3">Mohon Periksa sebelum verifikasi</div>
                        </div>

                        <div class="row">
                        <div class="col-6">
                            <div class="mb-3">Rincian Angsuran Pinjaman/Pembiayaan</div>
                            <div class="mb-3 row">
                                <label class="col-md-5 col-form-label">Tanggal</label>
                                <div class="col-7">
                                    <input type="date" class=" form-control">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label class="col-md-5 col-form-label">No Bukti</label>
                                <div class="col-7">
                                    <input type="text" class=" form-control">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label class="col-md-5 col-form-label">Keterangan</label>
                                <div class="col-7">
                                    <textarea class=" form-control" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label class="col-md-5 col-form-label">Nama Petugas</label>
                                <div class="col-7">
                                    <input type="text" class=" form-control">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label class="col-md-5 col-form-label">Nama Penerima</label>
                                <div class="col-7">
                                    <input type="text" class=" form-control">
                                </div>
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="mb-3">Rincian Angsuran Pinjaman/Pembiayaan</div>
                            <div class="mb-3 row">
                                <label class="col-md-5 col-form-label">No</label>
                                <div class="col-7">
                                    <input type="text" class=" form-control">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label class="col-md-5 col-form-label">Kode Pinjaman</label>
                                <div class="col-7">
                                    <input type="text" class=" form-control">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label class="col-md-5 col-form-label">Pokok</label>
                                <div class="col-7">
                                    <input type="text" class=" form-control">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label class="col-md-5 col-form-label">Jasa</label>
                                <div class="col-7">
                                    <input type="text" class=" form-control">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label class="col-md-5 col-form-label">Total</label>
                                <div class="col-7">
                                    <input type="text" class=" form-control">
                                </div>
                            </div>
                        </div>
                        </div>

                        <div class="d-flex flex-wrap gap-2">
                            <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                            <button type="button" onclick="window.history.back()" class="btn btn-secondary waves-effect waves-light">Cancel</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>

@endsection
@section('script')
    <!-- Required datatable js -->
    <script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/jszip/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/pdfmake/pdfmake.min.js') }}"></script>
    <!-- Datatable init js -->
    <script src="{{ URL::asset('/assets/js/pages/datatables.init.js') }}"></script>
@endsection
